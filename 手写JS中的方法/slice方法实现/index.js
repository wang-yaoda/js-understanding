//手写Array中的 slice方法  设置截取的开始和结尾 截取出一个新的数组 
Array.prototype.fakeSlice = function (start, end) {
    //拿到使用fakeSlice的数组
    let arr = this;
    //设置初始值 不写就是0 和结束值 是数组最后一个
    start = start || 0;
    end = end || arr.length;
    //定义返回的空数组
    var newArray= []; 

    for (let i =start; i < end; i++){
       newArray.push(arr[i]);   
    }
    return newArray;
}


 var newArray = Array.prototype.fakeSlice.call(["aaaa", "bbb", "cccc"], 1, 3)
 console.log(newArray)
//保存当前需要收集的响应式函数
let activeReactiveFn = null;

/* 
Depend优化:
 1.depend方法 不在proxy/get 里面使用全局变量
 2.使用set来保存依赖函数,而不是数组[]
*/

class Depend {
    constructor() {
        /* 
    以前用数组封装 所以 只是添加进入数组 使用set 不会再次添加 
    使用set是因为保证 一个obj.name只执行一次
    set保证只执行一次
        */
        this.reactiveFns = new Set();
    }

    depend() {
        if (activeReactiveFn) {
            //set 方法使用add方法
            this.reactiveFns.add(activeReactiveFn);
        }
    }

    notify() {
        this.reactiveFns.forEach(fn => {
            fn()
        })
    }

}
//封装一个响应式的函数
function watchFn(fn) {
    activeReactiveFn = fn;
    fn()
    activeReactiveFn = null;
}

//封装一个获取depend函数

const targetMap = new WeakMap();
function getDepend(target, key) {
    //根据target对象获取map的过程
    let map = targetMap.get(target)
    if (!map) {
        map = new Map()
        targetMap.set(target, map)
    }

    //根据key获取depend对象
    let depend = map.get(key)
    if (!depend) {
        depend = new Depend()
        map.set(key, depend)
    }
    return depend
}


// 对象的响应式
const obj = {
    name: "wyd", // depend对象
    age: 18 // depend对象
}


//监听对象的属性变量: Proxy(vue3)/Object.defineProperty(vue2)
const objProxy = new Proxy(obj, {
    get: function (target, key, receiver) {
        //根据target.key获取对应的depend
        const depend = getDepend(target, key);
        // 给depend对象中添加响应函数
        // depend.addDepend(activeReactiveFn)
        //优化代码 不需要对代码进行添加全局变量 只需要调用一个方法就行
        depend.depend()

        return Reflect.get(target,key,receiver)
    },
    set:function(target,key,newValue,receiver){
        Reflect.set(target,key,newValue,receiver)
        const depend= getDepend(target,key)
        depend.notify()
    }
})

watchFn(()=>{
    console.log(objProxy.name, "-------")
  console.log(objProxy.name, "+++++++")
}) 

objProxy.name = "kobe"
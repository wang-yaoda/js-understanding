//Depend 依赖的意思 收集依赖
class Depend {
    constructor() {
        this.reactiveFns = []
    }
    //添加·到响应式数组里面
    addDepend(reactiveFn) {
        this.reactiveFns.push(reactiveFn)
    }

    //notify  通知
    notify() {
        this.reactiveFns.forEach(fn => {
            fn()
        })
    }

}
  
const depend = new Depend()
//调用子类里面的方法 每一个属性对象一个depend对象

function watchFn(fn) {
    depend.addDepend(fn)
}

// 对象的响应式
const obj = {
    name: "wyd", // depend对象
    age: 18 // depend对象
}

watchFn(function () {
    const newName = obj.name
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(obj.name) // 100行
})

watchFn(function () {
    console.log(obj.name, "demo function -------")
})


obj.name = 'wz'

depend.notify()
  /*
你好啊, 李银河
Hello World
wz
wz demo function -------
*/
/* 当m=200的时候  代码会自动执行 就叫做响应式  
  在开发之中 响应式的
*/
let m =20;
console.log(m);

console.log(m*2); //m*2
console.log(m**2); //m的二次方
console.log('Hello World');


// 对象的响应式
const obj = {
    name: "why",
    age: 18
  }
  
  const newName = obj.name
  console.log("你好啊, 李银河")
  console.log("Hello World")
  console.log(obj.name) // 100行
  
  obj.name = "kobe"
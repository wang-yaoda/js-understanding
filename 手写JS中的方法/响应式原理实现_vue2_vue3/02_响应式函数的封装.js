//封装一个响应式的函数
let reactiveFns = []  //name发生改变所需要执行的函数
//响应式函数里面的内容
function WatchFn(fn){
    reactiveFns.push(fn)
}



//对象的响应式
const obj= {
    name:'wyd',
    age:19
}

//传入匿名函数就行
WatchFn(function() {
    const newName = obj.name
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(obj.name) // 100行
  })

watchFn(function() {
    console.log(obj.name, "demo function -------")
  })

function bar(){
console.log("普通的其他函数")
  console.log("这个函数不需要有任何响应式")
}

//遍历数组中的函数
reactiveFns.forEach(fn=>{
    fn()
})
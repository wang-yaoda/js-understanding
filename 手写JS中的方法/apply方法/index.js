//自己手写fakeApply方法 注意apply第二个参数传入的是数组
Function.prototype.fakeApply = function (thisArg, argArray) {

    //1.获取到要执行的函数
    var fn = this;
    // 2.处理绑定的thisArg  判断是不是undefined和null 但是可以传入0
    thisArg = (thisArg !== null && thisArg !== undefined) ? Object(thisArg) : window
    //3.执行函数
    thisArg.fn = fn

    var result
    // argArray = argArray ? argArray: []
    argArray = argArray || []  //保证不往里面传参数 也是可以运行的 要不第二个参数不传入 undefined ...就报错了

    result = thisArg.fn(...argArray);
    
    delete thisArg.fn

    //4.返回函数

    return result
}

function sum(num1, num2) {
    console.log("sum被调用", this, num1, num2)
    return num1 + num2
  }
  
  function foo(num1, num2) {
    console.log(this);
    return num1 + num2
  }

  // 自己实现的调用
 var result = sum.fakeApply("abc", [20, 30])
 console.log(result)

 var results =foo.fakeApply('ccc',[90,80])
 console.log(results);
 
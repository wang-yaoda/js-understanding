//寄生组合式继承


//传进去一个对象 让 fn的实例等于新传入的对象
function createObject(o){
    function Fn(){}
    Fn.prototype = o;
    var newObj = new Fn();
    return newObj
}


//封装继承函数
//Subtype继承自SuperType
function inheritPrototype(Subtype,SuperType) {
    //Object.create(SuperType.prototype) 创建一个对象 原型是SuperType的原型 
  //赋值给Subtype的原型

  //可以用Object.create 也可以用createObject自己封装的
//   Subtype.prototype =Object.create(SuperType.prototype)
Subtype.prototype =createObject(SuperType.prototype)
  //保证constructor指向
  Object.defineProperty(Subtype.prototype, "constructor", {
    enumerable: false,
    configurable: true,
    writable: true,
    value: Subtype
  })
  }


//增加一个fakeCall方法  call方法里面是参数列表 

Function.prototype.fakeCall = function (thisArg,...args) {
    //可以获取到哪一个函数执行了fakeCall
    //因为调用你的fakeCall就是一个this的隐式绑定 所以可以赋值给一个函数 this就是一个函数
   var fn = this
   // 2.对thisArg转成对象类型(防止它传入的是非对象类型)
   //判断是不是undefined和null 但是可以传入0 
   thisArg = (thisArg !== null && thisArg !== undefined) ? Object(thisArg): window

   //3.调用需要被执行的函数 
   //保证我们对象上面有一个fn函数的属性 隐式绑定
   thisArg.fn = fn;
   //来一个隐式绑定  绑定传进来的对象
   var result = thisArg.fn(...args)
   //因为给你传进来的对象 多一个fn的函数属性 ，所以删除这个属性
   delete thisArg.fn

   //4.讲最终结果返回出去
   return result;

}


function foo(num1,num2,num3) {
    console.log("foo函数被执行", this , num1,num2,num3)
  }

  foo.call('aaa',123)

  foo.fakeCall('bbb',2,3,4)
  

  // 默认进行隐式绑定
// foo.fakeCall({name: "why"})
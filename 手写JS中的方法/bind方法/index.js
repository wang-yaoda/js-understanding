// bind传参数的三种方法
function sum(num1, num2, num3, num4) {
    console.log(this,num1, num2, num3, num4)
  }

// var newSum = sum.bind("aaa", 10, 20, 30, 40)
// newSum()

// var newSum = sum.bind("aaa")
// newSum(10, 20, 30, 40)

// var newSum = sum.bind("aaa", 10)
// newSum(20, 30, 40)

//自己手写fakeBind方法 
Function.prototype.fakeBind = function(thisArg,...argArray){
     //1.获取到真实调用的函数
     var fn = this; this就是一个函数
    //2.对thisArg转成对象类型(防止它传入的是非对象类型)  绑定this
   //判断是不是undefined和null 但是可以传入0 
     thisArg = (thisArg !== null && thisArg !== undefined) ? Object(thisArg): window
    //bind返回一个代理函数
    function proxyFn(...args) {
 // 3.将函数放到thisArg中进行调用
        thisArg.fn=fn;
       // 特殊: 对两个传入的参数进行合并 因为bind传参是可以传入两组函数最后进行合并的 
    var finalArgs = [...argArray, ...args]
    var result = thisArg.fn(...finalArgs)
        delete thisArg.fn;

        //4.返回结果
        return result;

      }
    
      return proxyFn
}

var newSum = sum.fakeBind("abc", 10, 20)
var result = newSum(30, 40)

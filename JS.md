JS高级

js代码可以跑在node环境里

## 1.浏览器工作原理和V8引擎

### 浏览器为什么可以运行JS

1.先下载我们JS代码 但是不是直接下载我们的JS 

2.具体详细的图

![image-20210901214246880](./media/image-20210901214246880.png)

### 浏览器内核

**认识浏览器的内核**   我们经常会说：

不同的浏览器有不同的内核组成 

**Gecko**：早期被Netscape和Mozilla Firefox浏览器浏览器使用； 

**Trident**：微软开发，被IE4~IE11浏览器使用，但是Edge浏览器已经转向Blink； 

**Webkit**：苹果基于KHTML开发、开源的，用于Safari，Google Chrome之前也在使用； 

**Blink**：是Webkit的一个分支，Google开发，目前应用于Google Chrome、Edge、Opera等； 

等等...  事实上，我们经常说的浏览器内核指的是浏览器的排版引擎： 

排版引擎（layout engine），也称为浏览器引擎（browser engine）、页面渲染引擎（rendering engine） 或样版引擎。 

### 浏览器的渲染过程【重绘回流】

1.HTML解析 

先解析HTML 通过HTML parser  将HTML解析成DOM树 【树结构】 如果在这个过程中有对DOM的操作的话 

eg：document.createElement（） 创建元素 JS代码可以操作DOM  

2.CSS解析

解析CSS CSS parser 来解析CSS 解析出CSS规则 

![image-20210901221510542](./media/image-20210901221510542.png)

### JS引擎 

JS引擎就是将JS的代码转换成1010 01 01 11 00 这样的代码    

为什么需要JavaScript引擎呢？

 我们前面说过，高级的编程语言都是需要转成最终的机器指令来执行的； 

事实上我们编写的JavaScript无论你交给浏览器或者Node执行，最后都是需要被CPU执行的； 

但是CPU只认识自己的指令集，实际上是机器语言，才能被CPU所执行； 

**所以我们需要JavaScript引擎帮助我们将JavaScript代码翻译成CPU指令来执行； 比较常见的JavaScript引擎有哪些呢？**

**SpiderMonkey**：第一款JavaScript引擎，由Brendan Eich开发（也就是JavaScript作者）； 

**Chakra**：微软开发，用于IT浏览器； 

**JavaScriptCore**：WebKit中的JavaScript引擎，Apple公司开发； 

**V8**：Google开发的强大JavaScript引擎，也帮助Chrome从众多浏览器中脱颖而出 【市场占有率最高 达到了百分之六七十】

### 浏览器内核和 JS引擎之间的关系 

这里我们先以WebKit为例，WebKit事实上由两部分组成的：

 WebCore：**负责HTML解析、布局、渲染等等相关的工作；**

 JavaScriptCore：**解析、执行JavaScript代码；**

![image-20210901231835714](./media/image-20210901231835714.png)

### V8引擎的原理/架构

**什么是V8引擎?**

**将JS语言转换成CPU可以识别的0101代码**

**V8**是用**C ++**编写的Google**开源高性能JavaScript和WebAssembly引擎**，它用于Chrome和Node.js等【但是也有JS代码】。

 它实现**ECMAScript**和**WebAssembly**，并在Windows 7或更高版本，macOS 10.12+和使用x64，IA-32， ARM或MIPS处理器的Linux系统上运行。 

V8可以独立运行，也可以嵌入到任何C ++应用程序中

**V8引擎的架构图**

**JS代码转换成抽象语法树的官网** ： [AST explorer](https://astexplorer.net/)

转换成语法树的样子

![image-20210902213149443](./media/image-20210902213149443.png)

eg：babel生成语法树之后 如何 TS转换成 JS代码在执行呢

babel

ts => js

ts=>生成语法树 ast => 修改TS语法树 改成新的语法树 new ast => generate code => JS代码

![image-20210902214610156](./media/image-20210902214610156.png)



**为什么通过lgnition转换成字节码 不是直接转成机器码呢**？

 因为 在不同的机器环境，有不同的CPU架构  所能接受的机器指令是不同的 【window / mac /linux 等等】上面 ，所以转换成字节码， 然后V8引擎再将字节码【可以跨平台的，不要求平台】转成对应的汇编指令在转换成对应平台的机器指令

 **Parse模块会将JavaScript代码转换成AST（抽象语法树），这是因为解释器并不直接认识JavaScript代码**；

  **如果函数没有被调用，那么是不会被转换成AST的；** 

 Parse的V8官方文档：https://v8.dev/blog/scanner 

**Ignition是一个解释器，会将AST转换成ByteCode（字节码**） 

 同时会收集TurboFan优化所需要的信息（比如函数参数的类型信息，有了类型才能进行真实的运算）； 

**如果函数只调用一次，Ignition会执行解释执行ByteCode**； 【字节码很像汇编代码 但实际上不是汇编代码】

 Ignition的V8官方文档：https://v8.dev/blog/ignition-interpreter 

**TurboFan是一个编译器，可以将字节码编译为CPU可以直接执行的机器码**； 

 **如果一个函数被多次调用，那么就会被标记为热点函数，那么就会经过TurboFan转换成优化的机器码**，提高代码的执行性能；      但是，机器码实际上也会被还原为ByteCode，**这是因为如果后续执行函数的过程中，类型发生了变化（比如sum函数原来执行的是 number类型，后来执行变成了string类型）**，之前优化的机器码并不能正确的处理运算，【Deoptimization】就会逆向的转换成字节码 ；

 TurboFan的V8官方文档：https://v8.dev/blog/turbofan-jit

![image-20210902222937723](./media/image-20210902222937723.png)

### V8执行细节

 ***那么我们的JavaScript源码是如何被解析（Parse过程）的呢？***

  **Blink**【浏览器内核】将源码交给V8引擎，Stream获取到源码并且进行编码转换； 

 **Scanner**会进行词法分析（lexical analysis），词法分析会将代码转换成tokens【就是一个一个的小对象，里面有key和value，就相当于JSON里面的一个小的字段数据】； 

接下来tokens会被转换成AST树，经过Parser和PreParser： 

 **Parser**就是直接将tokens转成AST树架构； 

 ***PreParser称之为预解析，为什么需要预解析呢？***  

**这是因为并不是所有的JavaScript代码，在一开始时就会被执行。那么对所有的JavaScript代码进行解析，必然会 影响网页的运行效率；** 

**所以V8引擎就实现了Lazy Parsing（延迟解析）的方案，它的作用是将不必要的函数进行预解析【没必要给解析成为树结构】，也就是只解析暂 时需要的内容，而对函数的全量解析是在函数被调用时才会进行；** 

 **比如我们在一个函数outer内部定义了另外一个函数inner，那么inner函数就会进行预解析【因为inner刚开始我们不需要执行，所以预解析看看里面都是啥东西就行了，没必要扣那么细】；** 

 生成AST树后，会被Ignition转成字节码（bytecode），之后的过程就是代码的执行过程（后续会详细分析）。

### TS为什么要比JS代码的编码效率高？

  因为在执行函数的过程中 TS是表示了  代码类型的，中途不允许你进行改变【开始时number类型 后续变成string类型】   在V8引擎中就可以将代码直接转换成字节码 ；不需要当你后续执行函数 改变类型了 重新进行  逆向编码转换成字节码

### 代码在V8引擎中运行的过程

```js
/**
1.代码首先被解析 V8内部【parse阶段】会帮助我们创建一个对象(GlobalObject -> Go) 变成AST语法树
   Global全局的意思  
   Global放一些全局的对象{
                    DATE，
                    math，
                    STring
                    number
                    settimeout
                    console
                    setInterval
                    name：undefined
                   还有一个window属性 指向当前的对象 指向的是自己  window：GlobalObject
                   还有一些你自己写的全局变量 eg：var name= '张三' 
                   name: undefined,  //因为代码没有执行 所以没有赋值 所以只知道有这个变量 不知道值
   }
2.运行代码 
2.1 V8引擎 为了执行代码  V8内部会有一个执行上下文栈（函数调用栈 【栈结构】）
   执行上下文栈（Execution Context Stack，简称ECS/ECStack），它是用于执行代码的调用栈。 
   代码要想执行 就必须放在栈之中 【栈里面一般是函数 但是我们这个是个变量 不是函数 】;
2.2 因为我们是执行的全局代码，为了全局代码能正常的执行，需要创建 全局执行上下文(Global Execution Context/GEC)         (全局代码需要被执行时才会创建)
2.3 执行上下文就是VO吗？
    执行上下文不是VO 当你要执行函数的时候，创建一个执行上下文 执行上下文里面包括了 vo（变量环境）
*/
```



### 初始化全局对象

 **js引擎会在执行代码之前【解析成语法树的时候】，会在堆内存中创建一个全局对象：Global Object（GO）** 

该对象 所有的作用域（scope）都可以访问； 

里面会包含Date、Array、String、Number、setTimeout、setInterval等等； 

其中还有一个window属性指向自己  window ==Global Object

 ![image-20210902224514501](./media/image-20210902224514501.png)

**有关全局对象的伪代码**【不是真正的Global Object】

```js
var name = "why"

console.log(num1)

var num1 = 20
var num2 = 30
var result = num1 + num2

console.log(result)

/**
 * 1.代码被解析, v8引擎内部会帮助我们创建一个对象(GlobalObject -> Go) 变成AST语法树
 * 2.运行代码
 *    2.1. v8为了执行代码, v8引擎内部会有一个执行上下文栈(Execution Context Stack, ECStack)(函数调用栈)
 *         执行上下文栈 === 函数调用栈  【对内存划分结构，堆，栈】  
 *          它是用于执行代码的调用栈。
 *
 *    2.2. 因为我们执行的是全局代码, 为了全局代码能够正常的执行, 需要创建 全局执行上下文(Global Execution Context)(全局代码需要被执行时才会创建)
 */
var globalObject = {
  String: "类",
  Date: "类",
  setTimeout: "函数",
  window: globalObject,
  name: undefined,
  num1: undefined,
  num2: undefined,
  result: undefined
}

// console.log(window.window.window.window)
// 因为window指向的时全局对象的 所以 window里面指向自己 自己里面又有Window
```

### 作用域提升

**作用域提升是在V8引擎解析JS代码生成语法树的时候产生的** 注意是解析代码和函数 而不是执行函数

### 变量环境和记录

*旧的ECMA和新的ECMA标准*

关于全局执行上下文 和函数执行栈之间的问题 【详情看JS-高级why 代码】

**VO:AO ,VO:GO 这些概念都是早期的ECMA里面的概念 ES5之前**

每一个执行上下文都会关联到一个**变量（VO variable Object）对象** 在源代码中的变量和函数声明会被作为属性添加到VO对象之中 ，对于函数代码来说 参数也会被添加到VO之中 【也就是说 函数代码中AO会被添加到VO之中】

【最为官方的新知识】**最新的·ECMA之中 不叫变量对象了 叫做（variableEnvironment）变量环境**

每一个执行上下文都会关联到一个（variableEnvironment）**变量环境**  VE  在执行代码中的变量和函数声明会作为**环境记录**添加到变量环境中 ，对于函数代码来说 参数也会作为环境记录添加到变量环境之中   

【换句话说，保存你这种变量或者函数声明的东西 以前是一个VO（变量对象）对象   现在是一个VE（变量对象）环境 以前只能对象存储，现在可以是MAP存储 】

## 2.内存管理和内存泄漏

###    认识内存管理

 不管什么样的编程语言，在代码的执行过程中都是需要给它分配内存的，不同的是某些编程语言需要我们自己手动 的管理内存，某些编程语言会可以自动帮助我们管理内存： 

 不管以什么样的方式来管理内存，内存的管理都会有如下的生命周期： 

**第一步：分配申请你需要的内存（申请）；** 

**第二步：使用分配的内存（存放一些东西，比如对象等）；** 

**第三步：不需要使用时，对其进行释放；**  不释放会占用CPU内存

 不同的编程语言对于第一步和第三步会有不同的实现： 

手动管理内存：**比如C、C++，包括早期的OC**【IOS里面的语言】，都是需要手动来管理内存的申请和释放的（malloc和free函 数）； 

自动管理内存：比如Java、JavaScript、Python、Swift、Dart等，它们有自动帮助我们管理内存； 会自己释放内存管理

 我们可以知道JavaScript通常情况下是不需要手动来管理的

### JS的内存管理

JavaScript会在定义变量时为我们分配内存。 

 但是内存分配方式是一样的吗？ 

JS对于**基本数据类型内存的分配会在执行时**， 直接在**栈空间**进行分配； 

JS对于**复杂数据类型内存的分配会在堆内存** 中开辟一块空间，并且将这块空间的指针返 回值变量引用

![image-20210905181641128](./media/image-20210905181641128.png)

要是存储一个对象 放在堆内存之中 通过指针引用

### JS的垃圾回收

垃圾回收的英文是Garbage Collection，简称GC；

垃圾回收机制里面的两种方法  引用计数法，标记清除法

### 引用计数法

当一个对象有一个引用指向它时，那么这个对象的引用就+1，当一个对象的引用为0时，这个对象就可以被销 毁掉； 

这个算法有一个很大的弊端就是会产生循环引用

【里面专门有一个 retain count 保存你的指向 清0销毁掉】

```js
var obj = {name: "why"}

var info = {name: "kobe", friend: obj}

var p = {name: "james", friend: obj}

// 引用计数存在一个很大的弊端: 循环引用  两者永远相互指向的话 永远不会释放 造成内存泄漏
var obj1 = {friend: obj2}
var obj2 = {friend: obj1}

```

![image-20210905183248635](./media/image-20210905183248635.png)

### 标记清除法

这个算法是设置一个根对象（root object），垃圾回收器会定期从这个根开始，找所有从根开始有引用到的对象，对于哪些没有引用到的对象，就认为是不可用的对象；【就是不可到达的对象 就给他清除了】 

 这个算法可以很好的解决循环引用的问题

![image-20210905183458886](./media/image-20210905183458886.png)

JS引擎比较广泛的采用的就是标记清除算法，当然类似于V8引擎为了进行更好的优化，它在算法的实现细节上也会结合 一些其他的算法 **【主要采用的标记清除法】**

### 闭包

理解闭包 可以说是某种意义上的重生 大量使用JS但是无法完全彻底闭包

当如果需要彻底理解闭包 希望能掌握前面讲的VO GO AO VE概念

#####  **JS中函数是一等公民** 

函数可以使用的非常灵活，可以作为另一个函数的参数，也可以作为另一个函数的返回值来使用

##### **函数作为参数来使用**

```js
// 案例   通过外部传进函数 进行调用
function calc(num1, num2, calcFn) {
  console.log(calcFn(num1, num2))
}

function add(num1, num2) {
  return num1 + num2
}

function sub(num1, num2) {
  return num1 - num2
}

function mul(num1, num2) {
  return num1 * num2
}

var m = 20
var n = 30

calc(m, n, mul)
```

##### **函数作为返回值的使用**

```js
// js语法允许函数内部再定义函数
// function foo() {
//   function bar() {
//     console.log("bar")
//   }

//   return bar  //返回函数     //return bar() //返回函数执行值
// }

// var fn = foo()
// fn()

function makeAdder(count) {
  function add(num) {
    return count + num      /*  count 都已经使用完了 为什么还不会销毁呢？ 闭包 */
  }

  return add   //返回add函数
}

var add5 = makeAdder(5)   //count = 5  
console.log(add5(6))    //num = 6                    
console.log(add5(10))

var add10 = makeAdder(10)
var add100 = makeAdder(100)
```

##### **什么是高阶函数？**

高阶函数: 把一个函数如果接受**另外一个函数作为参数,或者该函数会返回另外一个函数作为返回值的函数**, 那么这个函数就称之为是一个高阶函数

##### **数组中函数的使用？包含部分高阶函数？**

```js
var nums = [10, 5, 11, 100, 55]
//1.filter: 过滤  返回出一个新的数组 

//[10, 5, 11, 100, 55]
//10 => false => 不会被放到newNums
//5 => false => 不会被放到newNums
//11 => false => 不会被放到newNums
//100 => false => 不会被放到newNums
//55 => false => 不会被放到newNums

/* item 数组中的元素  nums想要过滤的数组  newNums 返回的新的数组 里面会遍历五次 */

nums.filter((item,index,arr)=>boolean)  //返回一个Boolean值 里面传入三个参数 

var newNums = nums.filter(function(item) {
    //内部调用 返回一个boolean类型
  return item % 2 === 0 // 偶数
})
console.log(newNums)
```

```JS
// 2.map: 映射作用 里面传入一个函数
// [10, 5, 11, 100, 55]  

// 遍历你传入数组里面的东西 然后返回一个新数组 对原来的数组进行了个映射 都乘10了
 var newNums2 = nums.map(function(item) {
   return item * 10
 })
 console.log(newNums2)
```

```js
//3.forEach  迭代  遍历 没有返回值的
 nums.forEach(function(item) {
   console.log(item)
 })
```

```JS
// 4.find/findIndex
var nums = [10, 5, 11, 100, 55]
 var item = nums.find(function(item) {
   return item === 11
 })
 console.log(item)

 var friends = [
   {name: "why", age: 18},
   {name: "kobe", age: 40},
   {name: "james", age: 35},
   {name: "curry", age: 30},
 ]

 //find是精准查找 有返回值的 找到当前对象
 var findFriend = friends.find(function(item) {
   return item.name === 'james'
 })
 console.log(findFriend)

 //找到当前name=James的索引值
var friendIndex = friends.findIndex(function(item) {
  return item.name === 'james'
})
// console.log(friendIndex)
```

```JS
// 5.reduce: 累加 最常用的累加器 相比于for循环写的更加的高级
// nums.reduce
var nums = [10, 5, 11, 100, 55]

// 最普通的累加
 var total = 0
 for (var i = 0; i < nums.length; i++) {
   total += nums[i]
 }
 console.log(total)

// prevValue: 0, item: 10
// prevValue: 10, item: 5
// prevValue: 15, item: 11

//preValue 是上一次函数的返回值
var total = nums.reduce(function(prevValue, item) {
  return prevValue + item
}, 0)  //0是第一次初始化的值 第一次调用时候 preValue === 0  不传入的话默认也是0
console.log(total)

```

##### **函数和方法的区别？**

```js
// 函数function: 独立的function, 那么称之为是一个函数*

function* foo() {}

// 方法method: 当我们的一个函数属于某一个对象时, 我们成这个函数是这个对象的方法*

var obj = {

 foo: function*() {}

}

obj.foo()
```

### JS中闭包的定义

 这里先来看一下闭包的定义，分成两个：在计算机科学中和在JavaScript中。

 n在计算机科学中对闭包的定义（维基百科）： 

 闭包（英语：Closure），又称词法闭包【进行词法分析的时候就会确定闭包【确定上层作用域】】（Lexical Closure）或函数闭包（function closures）； 

 是在支持 **头等函数**【头等函数，函数可以作为低公民的语言】 的编程语言中，实现词法绑定的一种技术； 

 闭包在实现上是一个**结构体【不同语言中不同的定义 JS理解就是闭包的实现像一个对象一样 】**，它存储了一个函数和一个关联的环境（相当于一个符号查找表）； 

 **闭包跟函数最大的区别在于，当捕捉闭包的时候，它的 *自由变量* 会在*捕捉* 时被确定，这样即使脱离了捕捉时的上下文，它也能照常运行；** 

闭包的概念出现于60年代，最早实现闭包的程序是 Scheme，那么我们就可以理解为什么JavaScript中有闭包： p

因为JavaScript中有大量的设计是来源于Scheme的； 

 我们再来看一下MDN对JavaScript闭包的解释： 

 一个函数和对其周围状态（lexical environment，词法环境）的引用捆绑在一起（或者说函数被引用包围），这样的组合就是闭包（closure）； 

也就是说**，闭包让你可以在一个内层函数中访问到其外层函数的作用域；** 

 在 JavaScript 中，每当创建一个函数，闭包就会在函数创建的同时被创建出来； 

 **那么我的理解和总结：** 

 一个普通的函数function，如果它可以访问外层作用域【包括上上层的作用域】的自由变量，那么这个函数就是一个闭包； 

 从广义的角度来说：JavaScript中的函数都是闭包； 

 从狭义的角度来说：JavaScript中一个函数，如果访问了外层作用域的变量，那么它是一个闭包

## THIS指向

**大多数情况下this出现在函数里面** 

在全局作用域下 

this指向window（globalObject）

node环境指向{} 

node原因：node要执行的时候 会把他当作一个模块 然后加载->编译->放到一个函数里面->执行函数.call({}) call指向一个大括号

**this指向问题**

但是，开发中很少直接在全局作用于下去使用this，通常都是在函数中使用。 

所有的函数**在被调用时**，【执行的时候，所以才有非常多的绑定规则】都会创建一个执行上下文： 

这个上下文中记录着函数的调用栈、AO对象等； 

this也是其中的一条记录 

**同一个函数的this的不同**

```js
// this指向什么, 跟函数所处的位置是没有关系的
// 跟函数被调用的方式是有关系

function foo() {
  console.log(this)
}

// 1.直接调用这个函数
foo()  //window

// 2.创建一个对象, 对象中的函数指向foo
var obj = {
  name: 'why',
  foo: foo
}

obj.foo() //obj对象

// 3.apply调用
foo.apply("abc")  //字符串

1.函数在调用时，JavaScript会默认给this绑定一个值；
2.this的绑定和定义的位置（编写的位置）没有关系；
3.this的绑定和调用方式以及调用的位置有关系；
4.this是在运行时被绑定的
```

##### **this的绑定规则**

绑定一：默认绑定；  绑定二：隐式绑定；  绑定三：显式绑定；  绑定四：new绑定；

###### **默认绑定**

什么情况下使用默认绑定呢？独立函数调用。

 **独立的函数调用我们可以理解成函数没有被绑定到某个对象上进行*调用*；** 

 我们通过几个案例来看一下，常见的默认绑定

```js
1.案例一:
function foo() {
  console.log(this)  
}

foo()  //window

2.案例二:
function foo1() {
  console.log(this)
}

function foo2() {
  console.log(this)
  foo1()
}

function foo3() {
  console.log(this)
  foo2()
}

foo3() //三个window  三个全是独立函数


3.案例三:
var obj = {
  name: "why",
  foo: function() {
    console.log(this)
  }
}
var bar = obj.foo
bar() // window
/*
调用的时候有没有主题 不是定义时候 也就是说 bar（）调用的是没有主题的 所以是单独调用
*/

4.案例四:
function foo() {
  console.log(this)
}
var obj = {
  name: "why",
  foo: foo
}

var bar = obj.foo
bar() // window

 5.案例五:
function foo() {
  function bar() {
    console.log(this)
  }
  return bar
}

var fn = foo()
fn() // window

var obj = {
  name: "why",
  eating: fn
}

obj.eating() // 隐式绑定 通过对象调用的话 就指向对象了 
```

###### *隐式绑定*

另外一种比较常见的调用方式是通过某个对象进行调用的： 

也就是它的调用位置中，是通过某个对象发起的函数调用

```js
// 隐式绑定: object.fn()
// object对象会被js引擎绑定到fn函数的中this里面

function foo() {
  console.log(this)
}
// 独立函数调用
// foo()

//1.案例一:
var obj = {
  name: "why",
  foo: foo
}

obj.foo() // obj对象

//2.案例二:
var obj = {
  name: "why",
  eating: function() {
    console.log(this.name + "在吃东西")  //why
  },
  running: function() {
    console.log(obj.name + "在跑步")  //why
  }
}

// obj.eating()
// obj.running()

var fn = obj.eating
fn()  // 再吃东西 因为fn调用时独立调用 this指向window window中又有name 


// 3.案例三:
var obj1 = {
  name: "obj1",
  foo: function() {
    console.log(this)
  }
}

var obj2 = {
  name: "obj2",
  bar: obj1.foo
}

obj2.bar()  //obj2
//不管怎么变化 谁调用他 object对象会被js引擎绑定到fn函数的中this里面
```



JS高级

###### 显示绑定

**apply和call**

**this在内存中动态绑定到执行上下文之中**

call和apply可以绑定不同的this 要是直接调用就会指向全局对象

第一个参数是相同的，后面的参数，apply为数组，call为参数列表；

这两个函数的第一个参数都要求是一个对象，这个对象的作用是什么呢？就是给this准备的。

在调用这个函数时，会将this绑定到这个传入的对象上。

```js
// function foo() {
//   console.log("函数被调用了", this)
// }

// 1.foo直接调用和call/apply调用的不同在于this绑定的不同
// foo直接调用指向的是全局对象(window)
// foo()

// var obj = {
//   name: "obj"
// }

// call/apply是可以指定this的绑定对象
// foo.call(obj)
// foo.apply(obj)
// foo.apply("aaaa")


// 2.call和apply有什么区别?
function sum(num1, num2, num3) {
  console.log(num1 + num2 + num3, this)
}

sum.call("call", 20, 30, 40)
sum.apply("apply", [20, 30, 40])

// 3.call和apply在执行函数时,是可以明确的绑定this, 这个绑定规则称之为显示绑定

```

**bind**

```js
function foo() {
  console.log(this)
}

// foo.call("aaa")
// foo.call("aaa")
// foo.call("aaa")
// foo.call("aaa")

// 默认绑定和显示绑定bind冲突: 优先级(显示绑定)
//bind方法会返回另一个函数的

var newFoo = foo.bind("aaa")

newFoo()
// 默认绑定和显示绑定bind冲突: 优先级(显示绑定)
newFoo()
newFoo()
newFoo()
newFoo()
newFoo()

var bar = foo
console.log(bar === foo)
console.log(newFoo === foo)

```

###### new绑定

使用new关键字来调用函数是，会执行如下的操作：
1.创建一个全新的对象；
2.这个新对象会被执行prototype连接；
3.这个新对象会绑定到函数调用的this上（this的绑定在这个步骤完成）；
4.如果函数没有返回其他对象，表达式会返回这个新对象

```js
// 我们通过一个new关键字调用一个函数时(构造器), 这个时候this是在调用这个构造器时创建出来的对象
// this = 创建出来的对象
// 这个绑定过程就是new 绑定

function Person(name, age) {
  this.name = name
  this.age = age
}

var p1 = new Person("why", 18)
console.log(p1.name, p1.age)

var p2 = new Person("kobe", 30)
console.log(p2.name, p2.age)
//上面对象里面的this 指向你的实例对象
```

**关于this的分析**

```js
// 1.setTimeout
// function hySetTimeout(fn, duration) {
//  fn.call("abc")
// }

// hySetTimeout(function() {
//   console.log(this) // window
// }, 3000)

// setTimeout(function() {
//   console.log(this) // window
// }, 2000)

// 2.监听点击
const boxDiv = document.querySelector('.box')
boxDiv.onclick = function() {
  console.log(this) //元素对象 div
}

boxDiv.addEventListener('click', function() {
  console.log(this)
})
boxDiv.addEventListener('click', function() {
  console.log(this)
})
boxDiv.addEventListener('click', function() {
  console.log(this)
})

// 3.数组.forEach/map/filter/find
var names = ["abc", "cba", "nba"]
names.forEach(function(item) {
  console.log(item, this) // this指向window
}, "abc") 
//foreach是可以传入两个参数 第一个参数要遍历的item 第二个参数是this的绑定
names.map(function(item) {
  console.log(item, this)
}, "cba")
//map也是可以传入两个参数  第二个参数指向this
```

##### **关于this优先级问题的比较**

学习了四条规则，接下来开发中我们只需要去查找函数的调用应用了哪条规则即可，但是如果一个函数调用位置应用了多条规则，优先级谁更高呢？

***1.默认规则的优先级最低***
毫无疑问，**默认规则的优先级是最低的**，因为存在其他规则时，就会通过其他规则的方式来绑定this

***2.显示绑定优先级高于隐式绑定***

```js
 var obj = {
   name: "obj",
   foo: function() {
     console.log(this)
   }
 }

 obj.foo() //Obj

// 1.call/apply的显示绑定高于隐式绑定

 obj.foo.apply('abc') //abc
 obj.foo.call('abc') //abc

// 2.bind的优先级高于隐式绑定
 var bar = obj.foo.bind("cba")
 bar() //cba


// 3.更明显的比较
function foo() {
  console.log(this)
}

var obj = {
  name: "obj",
  foo: foo.bind("aaa") //obj里面的函数被bind绑定
}

obj.foo() // aaa
```

***3.new绑定优先级高于隐式绑定***

```js
var obj = {
  name: "obj",
  foo: function() {
    console.log(this)
  }
}

// new的优先级高于隐式绑定
var f = new obj.foo()
//打印出foo函数对象
```

***4.new绑定优先级高于bind***

**new绑定和call、apply是不允许同时使用的，所以不存在谁的优先级更高**

【因为new和call都是主动去调用一个函数，很难放在一起来使用】

**new绑定可以和bind一起使用，new绑定优先级更高**

```JS
// 结论: new关键字不能和apply/call一起来使用

// new的优先级高于bind
function foo() {
  console.log(this)
}

 var bar = foo.bind("aaa")

 var obj = new bar()
 //打印foo函数 所以 new的对象高于显示绑定

// new绑定 > 显示绑定(apply/call/bind) > 隐式绑定(obj.foo()) > 默认绑定(独立函数调用)

```



***5.bind和call优先级哪一个更高***

```js
function foo() {
  console.log(this)
}
foo.bind('abc').call('cba'); //abc
//bind绑定的优先级要高于call
```

##### this规则之外的一些绑定

**1.忽略显示绑定**

```js
function foo() {
  console.log(this)
}

foo.apply("abc") // abc
foo.apply({})  //{}

// apply/call/bind: 当传入null/undefined时, 自动将this绑定成全局对象
foo.apply(null)  //window
foo.apply(undefined) //window

var bar = foo.bind(null)
bar()  //window
```

**2.间接函数引用**

```js
// 争论: 代码规范 ;

var obj1 = {
  name: "obj1",
  foo: function() {
    console.log(this)
  }
}

var obj2 = {
  name: "obj2"
};

// obj2.bar = obj1.foo
// obj2.bar()  //obj2 

/* 如果像下面这样执行 可以调用 将foo函数返回出去 属于独立的函数调用*/

(obj2.bar = obj1.foo)()  //window

```

**3.测试代码**

```js
// 你不知道的JavaScript
function foo(el) {
  console.log(el,this.id)
}

var obj = {
  id:'awesome'
};
/*
如果此处不加上分号 代码解析过程就会是
var obj = {
  id:'awesome'
}nums.forEach(foo,obj) 所以报错 forEach is undefined
*/

var nums = [1,2,3]
nums.forEach(foo,obj)

```

**4.ES6箭头函数【arrow Function】的使用解析**

```js
// 1.编写箭头函数
// 1> (): 参数
// 2> =>: 箭头
// 3> {}: 函数的执行体
var foo = (num1, num2, num3) => {
  console.log(num1, num2, num3)
  var result = num1 + num2 + num3
  console.log(result)
}

function bar(num1, num2, num3) {
}

// 高阶函数在使用时, 也可以传入箭头函数
var nums = [10, 20, 45, 78]
nums.forEach((item, index, arr) => {})

// 箭头函数有一些常见的简写:
// 简写一: 如果参数只有一个, ()可以省略
nums.forEach(item => {
  console.log(item)
})

// 简写二: 如果函数执行体只有一行代码, 那么{}也可以省略
// 强调: 并且它会默认将这行代码的执行结果作为返回值
nums.forEach(item => console.log(item))

var newNums = nums.filter(item => item % 2 === 0)
console.log(newNums)

// filter/map/reduce
var result = nums.filter(item => item % 2 === 0)
                 .map(item => item * 100)
                 .reduce((preValue, item) => preValue + item)
console.log(result)

// 简写三: 如果一个箭头函数, 只有一行代码, 并且返回一个对象, 这个时候如何编写简写
// var bar = () => {
//   return { name: "why", age: 18 }
// }
//返回的时候 加上小括号 相当于返回一个整体对象的形式
var bar = () => ({ name: "why", age: 18 })

```

**5.箭头函数中的this规则**

箭头函数不会绑定this 会绑定到箭头函数外部的作用域上面

```js
// 1.测试箭头函数中this指向
// var name = "why"

// var foo = () => {
//   console.log(this)
// }

// foo()   //window
// var obj = {foo: foo}
// obj.foo()  //window
// foo.call("abc")  //window

// 2.应用场景
var obj = {
  data: [],
  getData: function() {
      /*
      settimeout里面的this指向window的 所以外面要将自己的this存起来 然后
      在里面调用_this来进行指向外面 要不就指向window了 
      
      settimeout箭头函数直接指向外层作用域 所以直接就是指向外层函数了
      */
      
    // 发送网络请求, 将结果放到上面data属性中
      
    // 在箭头函数之前的解决方案
      
    // var _this = this  // 将getData中的this存入
    // setTimeout(function() {
    //  var result = ["abc", "cba", "nba"]
    // _this.data = result
    // }, 2000);
      
      
    // 箭头函数之后
    setTimeout(() => {
      var result = ["abc", "cba", "nba"]
      this.data = result
    }, 2000);
  }
}

obj.getData()
```

##### this的面试题

**一**

```js
var name = "window";  //添加到GO对象中 GO对象中有window
var person = {
  name: "person",
  sayName: function () {
    console.log(this.name);
  }
};

function sayName() {
  var sss = person.sayName;
  sss(); // window: 独立函数调用
  person.sayName(); // person: 隐式调用
  (person.sayName)(); // person: 隐式调用 
    //直接取里面的person调用
  (b = person.sayName)(); // window: 赋值表达式(独立函数调用)
}

sayName();
```

**二**

```js
var name = 'window'
//定义对象的时候 解析对象 属于全局对象 是没有产生作用域的 所以他的作用域还是全局的 
//定义函数的时候 才会解析对象 
var person1 = {
  name: 'person1',
    
  foo1: function () {
    console.log(this.name)
  },
    
  foo2: () => console.log(this.name),
    
  foo3: function () {
    return function () {
      console.log(this.name)
    }
  },
    
  foo4: function () {
    return () => {
      console.log(this.name)
    }
  }
}

var person2 = { name: 'person2' }

 person1.foo1(); // person1(隐式绑定)
 person1.foo1.call(person2); // person2(显示绑定优先级大于隐式绑定)

 person1.foo2(); // window(不绑定作用域,上层作用域是全局)
 person1.foo2.call(person2); // window

 person1.foo3()(); // window(独立函数调用)  //拿到你函数的返回结果 在调用就是window
 person1.foo3.call(person2)(); // window(独立函数调用)
//没有小括号的话 就是person2  有小括号就是独立调用
 person1.foo3().call(person2); // person2(最终调用返回函数式, 使用的是显示绑定)

 person1.foo4()(); // person1 (箭头函数不绑定this, 上层作用域this是person1)
//里面的foo4函数 返回一个函数值
 person1.foo4.call(person2)(); // person2(上层作用域被显示的绑定了一个person2)
 person1.foo4().call(person2); // person1(上层找到person1) 箭头函数 this不绑定作用域 所以不绑定person2
//还是去上层作用域去寻找找到person1
```

**三**

```js
var name = 'window'

function Person (name) {
  this.name = name
    
  this.foo1 = function () {
    console.log(this.name)
  },
      
  this.foo2 = () => console.log(this.name),
      
  this.foo3 = function () {
    return function () {
      console.log(this.name)
    }
  },
      
  this.foo4 = function () {
    return () => {
      console.log(this.name)
    }
  }
    
}

var person1 = new Person('person1')
var person2 = new Person('person2')

person1.foo1() // person1
person1.foo1.call(person2) // person2(显示高于隐式绑定)

//因为找外层作用域找到了Person 然后person1是Person new出来的  

person1.foo2() // person1 (上层作用域中的this是person1)
person1.foo2.call(person2) // person1 (上层作用域中的this是person1)

person1.foo3()() // window(独立函数调用)
person1.foo3.call(person2)() // window
person1.foo3().call(person2) // person2

person1.foo4()() // person1
person1.foo4.call(person2)() // person2  
person1.foo4().call(person2) // person1


var obj = {
  name: "obj",
  foo: function() {

  }
}
```

**四**

```js
var name = 'window'

function Person (name) {
  this.name = name
  this.obj = {
    name: 'obj',
      
    foo1: function () {
      return function () {
        console.log(this.name)
      }
    },
      
    foo2: function () {
      return () => {
        console.log(this.name)
      }
    }
      
  }
}

var person1 = new Person('person1')
var person2 = new Person('person2')

person1.obj.foo1()() // window
person1.obj.foo1.call(person2)() // window
person1.obj.foo1().call(person2) // person2

person1.obj.foo2()() // obj
person1.obj.foo2.call(person2)() // person2
person1.obj.foo2().call(person2) // obj


// 

// 上层作用域的理解
// var obj = {
//   name: "obj",
//   foo: function() {
//     // 上层作用域是全局
//   }
// }

// function Student() {

//foo上级作用域是 Student
//   this.foo = function() {

//   }
// }

```

## 函数的Call，Apply，Bind区别

apply，call，bind使用C++实现的

我们具体用JS实现一下 但是实现是练习函数、this、调用关系，不会过度考虑一些边界情况

### **call的实现**

```js
// apply/call/bind的用法
// js模拟它们的实现? 难度

// 给所有的函数添加一个hycall的方法
Function.prototype.hycall = function(thisArg, ...args) {
  // 在这里可以去执行调用的那个函数(foo)
  // 问题: 得可以获取到是哪一个函数执行了hycall
  // 1.获取需要被执行的函数
  var fn = this

  // 2.对thisArg转成对象类型(防止它传入的是非对象类型)
  thisArg = (thisArg !== null && thisArg !== undefined) ? Object(thisArg): window

  // 3.调用需要被执行的函数
  thisArg.fn = fn
  var result = thisArg.fn(...args)
  delete thisArg.fn

  // 4.将最终的结果返回出去
  return result
}


function foo() {
  console.log("foo函数被执行", this)
}

function sum(num1, num2) {
  console.log("sum函数被执行", this, num1, num2)
  return num1 + num2
}


// 系统的函数的call方法
foo.call(undefined)
var result = sum.call({}, 20, 30)
// console.log("系统调用的结果:", result)

// 自己实现的函数的hycall方法
// 默认进行隐式绑定
// foo.hycall({name: "why"})
foo.hycall(undefined)
var result = sum.hycall("abc", 20, 30)
console.log("hycall的调用:", result)

// var num = {name: "why"}
// console.log(Object(num))

```

### **apply实现**

```js
// 自己实现hyapply
Function.prototype.hyapply = function(thisArg, argArray) {
  // 1.获取到要执行的函数
  var fn = this

  // 2.处理绑定的thisArg
  thisArg = (thisArg !== null && thisArg !== undefined) ? Object(thisArg): window

  // 3.执行函数
  thisArg.fn = fn
  var result
  // if (!argArray) { // argArray是没有值(没有传参数)
  //   result = thisArg.fn()
  // } else { // 有传参数
  //   result = thisArg.fn(...argArray)
  // }

  // argArray = argArray ? argArray: []
  argArray = argArray || []
  result = thisArg.fn(...argArray)

  delete thisArg.fn

  // 4.返回结果
  return result
}

function sum(num1, num2) {
  console.log("sum被调用", this, num1, num2)
  return num1 + num2
}

function foo(num) {
  return num
}

function bar() {
  console.log("bar函数被执行", this)
}

// 系统调用
// var result = sum.apply("abc", 20)
// console.log(result)

// 自己实现的调用
// var result = sum.hyapply("abc", [20, 30])
// console.log(result)

// var result2 = foo.hyapply("abc", [20])
// console.log(result2)

// edge case
bar.hyapply(0)

```

### **bind实现**

```js
Function.prototype.hybind = function(thisArg, ...argArray) {
  // 1.获取到真实需要调用的函数
  var fn = this

  // 2.绑定this
  thisArg = (thisArg !== null && thisArg !== undefined) ? Object(thisArg): window

  function proxyFn(...args) {
    // 3.将函数放到thisArg中进行调用
    thisArg.fn = fn
    // 特殊: 对两个传入的参数进行合并
    var finalArgs = [...argArray, ...args]
    var result = thisArg.fn(...finalArgs)
    delete thisArg.fn

    // 4.返回结果
    return result
  }

  return proxyFn
}

function foo() {
  console.log("foo被执行", this)
  return 20
}

function sum(num1, num2, num3, num4) {
  console.log(num1, num2, num3, num4)
}

// 系统的bind使用
var bar = foo.bind("abc")
bar()

/*bind传值的三种方式*/
// var newSum = sum.bind("aaa", 10, 20, 30, 40)
// newSum()

// var newSum = sum.bind("aaa")
// newSum(10, 20, 30, 40)

// var newSum = sum.bind("aaa", 10)
// newSum(20, 30, 40)


// 使用自己定义的bind
// var bar = foo.hybind("abc")
// var result = bar()
// console.log(result)

var newSum = sum.hybind("abc", 10, 20)
var result = newSum(30, 40)
```

bind函数其实就算是柯里化 先传入一些东西后续在传入一些

### arguments基本使用

###### 1.认识arguments

arguments 是一个 对应于 **传递给函数的参数的 类数组**(array-like)**对象**

常见的对arguments的操作是三个
  1.获取参数的长度   2.根据索引值获取某一个参数  3.callee获取当前arguments所在的函数

```js
// arguments默认放在AO对象之中
/* AO ={
     num1:undefined,
     num2:undefined,
     num3:undefined
}
*/

function foo(num1, num2, num3) {
  // 类数组对象中(长的像是一个数组, 本质上是一个对象): arguments
  // console.log(arguments)   // 10,20,30,40,50 callee

  // 常见的对arguments的操作是三个
  // 1.获取参数的长度
  console.log(arguments.length) //5

  // 2.根据索引值获取某一个参数
  console.log(arguments[2]) //30
  console.log(arguments[3]) //40
  console.log(arguments[4]) //50

  // 3.callee获取当前arguments所在的函数
  console.log(arguments.callee)  //输出foo函数
    
  // arguments.callee() 如果这么操作就会变成递归了
}

foo(10, 20, 30, 40, 50)

```


arguments 是一个 对应于 传递给函数的参数的 类数组(array-like)对象。

array-like意味着它不是一个数组类型，而是一个对象类型：

但是它却拥有数组的一些特性，比如说length，比如可以通过index索引来访问；

但是它却没有数组的一些方法，比如forEach、map等

***arguments是默认生成的 在箭头函数中是没有的***

###### 2.arguments转换成数组

```js
//如果想给 arguments里面所有参数全部*10 应该怎么操作

function foo(num1, num2) {
  // 1.自己遍历
   var newArr = []
   for (var i = 0; i < arguments.length; i++) {
     newArr.push(arguments[i] * 10)
   }
   console.log(newArr)
}
foo(10, 20, 30, 40, 50)
```

```JS
//将arguments转换成数组类型
1.将arguments里面所有元素遍历一遍然后装进一个新的数组
2.Array.prototype.slice将arguments转成array 返回一个新的数组
3.ES6的语法 Array.from传入一个(arguments)   ...展开运算符也是可以的

 function foo(num1, num2) {
  // 2.arguments转成array类型
  // 2.1.自己遍历arguments中所有的元素
   
     //拿到slice方法 从开始长度截到最后长度
 //数组原型上面是有slice的 用call是因为将this指向为我们要指定的 显示绑定
   // array.prototype默认指向调用者
     
  // 2.2.Array.prototype.slice将arguments转成array  也是用了里面的遍历方法
  var newArr2 = Array.prototype.slice.call(arguments)
  console.log(newArr2)
    
     //newArr2 === newArr3 两种方法都是一样的
     
  var newArr3 = [].slice.call(arguments)
  console.log(newArr3)

  // 2.3.ES6的语法
  var newArr4 = Array.from(arguments)
  console.log(newArr4)
  var newArr5 = [...arguments]
  console.log(newArr5)
}

foo(10, 20, 30, 40, 50)

```

###### 3.箭头函数中没有arguments

箭头函数是不绑定arguments的，所以我们在箭头函数中使用arguments会去上层作用域查找：

**全局作用域之中的arguments是不同的 node里面有arguments  浏览器之中没有arguments 报错**

```js
// 1.案例一:
 var foo = () => {
   console.log(arguments) //去全局作用域之中寻找
 }
 foo()

// 2.案例二:
 function foo() {
   var bar = () => {
     console.log(arguments)  //去找foo中的arguments
   }
   return bar
 }
 var fn = foo(123)
 fn()

// 3.案例三: 
// es6 中的扩展运算符 ...args可以替换掉原来之中的arguments
var foo = (num1, num2, ...args) => {
  console.log(args)
}

foo(10, 20, 30, 40, 50)

```

##### 理解JS的纯函数

函数式编程中有一个非常重要的概念叫纯函数，JavaScript符合函数式编程的范式所以也有纯函数的概念；
在react开发中纯函数是被多次提及的；
比如react中组件就被要求像是一个纯函数（为什么是像，因为还有class组件），redux中有一个reducer的概念，也是要求必须是一个纯函数；所以掌握纯函数对于理解很多框架的设计是非常有帮助的；

###### **纯函数的维基百科定义：**

在程序设计中，若一个函数符合以下条件，那么这个函数被称为纯函数：

**此函数在相同的输入值时，需产生相同的输出。**

**函数的输出和输入值以外的其他隐藏信息或状态无关，也和由I/O设备产生的外部输出无关。**

**该函数不能有语义上可观察的函数副作用，诸如“触发事件”，使输出设备输出，或更改输出值以外物件的内容等。**

当然上面的定义会过于的晦涩，所以我简单总结一下：

***确定的输入，一定会产生确定的输出***； 

***函数在执行过程中，不能产生副作用***

###### **什么是副作用？**

那么这里又有一个概念，叫做副作用，什么又是副作用呢？
副作用（side effect）其实本身是医学的一个概念，比如我们经常说吃什么药本来是为了治病，可能会产一些其他的副作用；

在计算机科学中，也引用了副作用的概念，**表示在执行一个函数时，除了返回函数值之外，还对调用函数产生了附加的影响，比如修改了*全局变量*，*修改参数*或者*改变外部的存储*；**

【比如说 在使用函数的时候 你修改了全局变量 或者说 你使用了localstorage 但是使用函数修改了之前的localstorage 这样就是副作用了】

纯函数在执行的过程中就是不能产生这样的副作用： 副作用往往是产生bug的 “温床”

**对于纯函数的认知**

两个纯函数的比较 slice和 splice 对数组进行截取
**slice：slice截取数组时不会对原数组进行任何操作,而是生成一个新的数组； 因为slice是对原来的数组进行遍历重新push进入一个新的数组**
**splice：splice截取数组, 会返回一个新的数组, 也会对原数组进行修改；**

slice就是一个纯函数，不会修改传入的函数

```js
var names = ["abc", "cba", "nba", "dna"]

// slice只要给它传入一个start/end, 那么对于同一个数组来说, 它会给我们返回确定的值
// slice函数本身它是不会修改原来的数组
// slice -> this
// slice函数本身就是一个纯函数

 var newNames1 = names.slice(0, 3) //012
 console.log(newNames1) // abc cba nba 包左不包右
 console.log(names)

// ["abc", "cba", "nba", "dna"]
// splice在执行时, 有修改掉调用的数组对象本身, 修改的这个操作就是产生的副作用
// splice不是一个纯函数

var newNames2 = names.splice(2) //从2的位置开始截取
console.log(newNames2)//["nba", "dna"]
console.log(names)//["abc", "cba"]
```

**对于纯函数的练习**

```JS
// foo函数是否是一个纯函数?
// 1.相同的输入一定产生相同的输出
// 2.在执行的过程中不会产生任何的副作用【没有修改外界的变量和存储】

function foo(num1, num2) {
  return num1 * 2 + num2 * num2
} //纯函数

// bar不是一个纯函数, 因为它修改了外界的变量
var name = "abc" 
function bar() {
  console.log("bar其他的代码执行")
  name = "cba"
} 

bar()

console.log(name)

// baz也不是一个纯函数, 因为我们修改了传入的参数
function baz(info) {
  info.age = 100
}
var obj = {name: "wyd", age: 18}
baz(obj)
console.log(obj) // {name: "wyd", age: 100}


// test是否是一个纯函数? 是一个纯函数
 function test(info) {
   return {
     ...info,
     age: 100
   }
 }
//相同的输入 始终输出相同的输出 obj对象里面的东西没有改变  纯函数
 test(obj)
 test(obj)
 test(obj)
 test(obj)


// React的函数组件 因为react函数里面不允许你修改他传进来的props
// Vue 其实也是一样 不允许修改props 否则影响了他内部的单项数据流 
function HelloWorld(props) {
  props.info = {}
  props.info.name = "why"
}

```

**纯函数的优势**

为什么纯函数在函数式编程中非常重要呢？
因为你可以安心的编写和安心的使用； 你在写的时候保证了函数的纯度，只是单纯实现自己的业务逻辑即可，不需要关心传入的内容是如何获得的或者依赖其他的外部变量是否已经发生了修改； 你在用的时候，你确定你的输入内容不会被任意篡改，并且自己确定的输入，一定会有确定的输出；

#### JS函数柯里化

##### 什么是柯里化？

柯里化也是属于函数式编程里面一个非常重要的概念。
**我们先来看一下维基百科的解释**：
在计算机科学中，柯里化（英语：Currying），又译为卡瑞化或加里化；
是把接收多个参数的函数，变成接受一个单一参数（最初函数的第一个参数）的函数，并且返回接受余下的参数，而且返回结果的新函数的技术；柯里化声称“如果你固定某些参数，你将得到接受余下参数的一个函数”；
维基百科的结束非常的抽象，我们这里做一个总结：
**只传递给函数一部分参数来调用它，让它返回一个函数去处理剩余的参数；这个过程就称之为柯里化；**

```js
//函数柯里化的实例
//只传递给函数一部分参数来调用 让它返回一个函数去处理剩余的参数；

//正常传入三个参数
function add(x, y, z) {
  return x + y + z
}

var result = add(10, 20, 30)
console.log(result)

//上面变成下面的函数的过程就叫柯里化的过程 

//柯里化传入三个参数
function sum1(x) {
  return function(y) {
    return function(z) {
      return x + y + z
    }
  }
}
var result1 = sum1(10)(20)(30)
console.log(result1)

//简化柯里化的过程
var sum2 = x => {
  return y => {
    return z => {
      return x + y + z;
    }
  }
}
//返回出去 并且 只有一条语句 就可以这样返回
var sum2 = x => y => z => {
  return x + y + z
}

console.log(sum2(10)(20)(30))

var sum3 = x => y => z => x + y + z
console.log(sum3(10)(20)(30))


```

##### 为什么要柯里化？

在函数式编程中，我们其实**往往希望一个函数处理的问题尽可能的单一**，**而不是将一大堆的处理过程交给一个 函数来处理； 那么我们是否就可以将每次传入的参数在单一的函数中进行处理，处理完后在下一个函数中再使用处理后的结果；**

设计模式中有一个单一职责原则，（SRP）面向对象中封装一个类的时候，一个类尽可能完成单一的事情；就是单一职责

###### 1.柯里化单一职责的原则

需求：传入的函数需要分别被进行如下处理  第一个参数 + 2   第二个参数 * 2   第三个参数 ** 2

```js
function add(x, y, z) {
  x = x + 2
  y = y * 2
  z = z * z
  return x + y + z
}

console.log(add(10, 20, 30))


function sum(x) {
  x = x + 2

  return function(y) {
    y = y * 2

    return function(z) {
      z = z * z

      return x + y + z
    }
  }
}

console.log(sum(10)(20)(30))

```

###### 2.柯里化的逻辑复用

```js
// function sum(m, n) {
//   return m + n
// }

// 假如在程序中,我们经常需要把5和另外一个数字进行相加
// console.log(sum(5, 10))
// console.log(sum(5, 14))
// console.log(sum(5, 1100))
// console.log(sum(5, 555))

function makeAdder(count) {
  count = count * count

  return function(num) {
    return count + num
  }
}

// var result = makeAdder(5)(10)
var adder5 = makeAdder(5)
adder5(10)
adder5(14)
adder5(1100)
adder5(555)
```

打印日志柯里化

```js
//打印日志
function log(date, type, message) {
  console.log(`[${date.getHours()}:${date.getMinutes()}][${type}]: [${message}]`)
}

// log(new Date(), "DEBUG", "查找到轮播图的bug")
// log(new Date(), "DEBUG", "查询菜单的bug")
// log(new Date(), "DEBUG", "查询数据的bug")

// 柯里化的优化
var log = date => type => message => {
  console.log(`[${date.getHours()}:${date.getMinutes()}][${type}]: [${message}]`)
}

// 如果我现在打印的都是当前时间
var nowLog = log(new Date())
nowLog("DEBUG")("查找到轮播图的bug") //连着两次函数调用 不可以写一个括号里面 两个信息
nowLog("FETURE")("新增了添加用户的功能")

var nowAndDebugLog = log(new Date())("DEBUG")
nowAndDebugLog("查找到轮播图的bug")
nowAndDebugLog("查找到轮播图的bug")
nowAndDebugLog("查找到轮播图的bug")
nowAndDebugLog("查找到轮播图的bug")


var nowAndFetureLog = log(new Date())("FETURE")
nowAndFetureLog("添加新功能~")

```

#### 手写实现函数柯里化

```JS
// 柯里化函数的实现hyCurrying
function hyCurrying(fn) {
  function curried(...args) {
    // 判断当前已经接收的参数的个数, 可以参数本身需要接受的参数是否已经一致了
    // 1.当已经传入的参数 大于等于 需要的参数时, 就执行函数
    if (args.length >= fn.length) {
      // fn(...args)
      // fn.call(this, ...args)
      return fn.apply(this, args)
    } else {
      // 没有达到个数时, 需要返回一个新的函数, 继续来接收的参数
      function curried2(...args2) {
        // 接收到参数后, 需要递归调用curried来检查函数的个数是否达到
        return curried.apply(this, args.concat(args2))
      }
      return curried2
    }
  }
  return curried
}

var curryAdd = hyCurrying(add1)


console.log(curryAdd(10, 20, 30))
console.log(curryAdd(10, 20)(30))
console.log(curryAdd(10)(20)(30))

// function foo(x, y, z, m, n, a, b) {

// }
// console.log(foo.length)


function foo(x, y, z) {
  return x + y + z
}

foo.call({}, 1, 2, 3)

var curryFoo = hyCurrying(foo)
curryFoo.call({}, 1)


```

#### 组合函数

组合（Compose）函数是在JavaScript开发过程中一种对函数的使用技巧、模式：
比如我们现在需要对某一个数据进行函数的调用，执行两个函数fn1和fn2，这两个函数是依次执行的；
那么如果每次我们都需要进行两个函数的调用，操作上就会显得重复；
那么是否可以将这两个函数组合起来，自动依次调用呢？
这个过程就是对函数的组合，我们称之为 组合函数（Compose Function）；

```JS
function double(num) {
  return num * 2
}

function square(num) {
  return num ** 2
}

var count = 10
var result = square(double(count))
console.log(result)

// 实现最简单的组合函数 接受两个函数 一个M 一个N 但是有调用顺序的问题
function composeFn(m, n) {
  return function(count) {
    return n(m(count))
  }
}

var newFn = composeFn(double, square)
console.log(newFn(10))

```

### JS额外知识补充

#### with语句

with语句可以形成自己的作用域的

```js
var message = "Hello World"
// console.log(message)

// with语句: 可以形成自己的作用域
var obj = {name: "why", age: 18, message: "obj message"}

function foo() {
  function bar() {
    with(obj) {   //放一个对象  如果对象obj里面没有自己的message 就去找全局的 message
      console.log(message)
      console.log("------")
    }
  }
  bar()
}

foo()

var info = {name: "kobe"}
with(info) {
  console.log(name)
}

```

 **不建议使用with语句**，因为它可能是混淆错误和兼容性问题的根源 但是需要了解 如果开启严格模式的话，with语句会报错的

#### eval函数的使用

eval是一个特殊的函数，它可以将传入的字符串当做JavaScript代码来运行。 

```js
var jsString = 'var message = "Hello World"; console.log(message);'
//加上分号表示 一段代码的结束；

var message = "Hello World"
console.log(message)

eval(jsString)

```

 不建议在开发中使用eval： eval代码的可读性非常的差（代码的可读性是高质量代码的重要原则）；

 eval是一个字符串，那么有可能在执行的过程中被刻意篡改，那么可能会造成被攻击的风险； 

eval的执行必须经过JS解释器，不能被JS引擎优化

#### 严格模式

##### 什么是严格模式

 在ECMAScript5标准中，JavaScript提出了严格模式的概念（Strict Mode）： 

严格模式很好理解，是一种具有限制性的JavaScript模式，从而使代码隐式的脱离了 ”懒散（sloppy）模式“； 

```js
//js代码是非常灵活的
name ='wyd',
message = 'hello world'
123.name = '123'
```

支持严格模式的浏览器在检测到代码中有严格模式时，会以更加严格的方式对代码进行检测和执行； 

 严格模式对正常的JavaScript语义进行了一些限制：

严格模式通过 **抛出错误 来消除一些原有的 静默（silent）错误**；  就是你的代码是有问题的 但是没有恶劣影响 ，JS引擎不做处理

严格模式让JS引擎在执行代码时可以进行更多的优化（不需要对一些特殊的语法进行处理）； 

严格模式禁用了在ECMAScript未来版本中可能会定义的一些语法

【因为ECMA是有自己的保留字的（class/let/const）es5以前的保留字 后续可以生成关键字 非严格模式是不报错的 严格模式有这些代码就会报错】

##### 如何使用严格模式

可以支持在js文件中开启严格模式；  我**们现在在真实开发的代码 打包【webpack/vite】后会开启严格模式 是针对函数开启的严格模式 use strict**

也支持对某一个函数开启严格模式； 

 严格模式通过在文件或者函数开头使用 use strict 来开启

```js
//js文件开启严格模式

 "use strict"
//message = 'Hello World'  不开启严格模式正常执行 开启后报错
var message = "Hello World"
console.log(message)

// true.foo = 'abc'  //未开启严格模式 会忽略掉  
//开启严格模式后 报错 Cannot create property 'foo' on Boolean 'true'
function foo() {
console.log('message')
}

foo()
```

```js
//函数开启严格模式 表示函数内部开启了严格模式

function foo() {
  "use strict";

  true.foo = "abc"
}

foo()
//开启严格模式后 报错 Cannot create property 'foo' on Boolean 'true'
```

##### 严格模式下限制的不同

1. 无法 意外的创建全局变量  

   ```js
   // 1. 禁止意外创建全局变量
    message = "Hello World"
    console.log(message)
   
    function foo() {
        //也是全局的变量 也是一种语法错误
      age = 20
    }
   
    foo()
    console.log(age)
   ```

2. 严格模式会使引起静默失败(silently fail,注:不报错也没有任何效果)的赋值操作抛出异常 

   ```js
   //静默错误
   true.name = "abc"
   NaN = 123
   var obj = {}
   Object.defineProperty(obj, "name", {
     configurable: false,  //看是否是可以配置的  
     writable: false,  //不可写内容 是可读属性
     value: "why"
   })
   console.log(obj.name)
   // obj.name = "kobe" //不是严格模式下 不会报错
   
   delete obj.name  //上面配置了 configurable后表示了不可以删除name属性
   ```

3.  严格模式下试图删除不可删除的属性 

4. 严格模式不允许函数参数有相同的名称 

   ```js
   // 2.不允许函数有相同的参数名称
    function foo(x, y, x) {
      console.log(x, y, x)
    }
    foo(10, 20, 30)
   ```

5. 不允许0的八进制语法 

   ```js
   // 4.不允许使用原先的八进制格式 0123
    var num = 0o123 // 八进制
    var num2 = 0x123 // 十六进制
    var num3 = 0b100 // 二进制
    console.log(num, num2, num3)
   ```

6. 在严格模式下，不允许使用with 

7. 在严格模式下，eval不再为上层引用变量

   ```js
   var jsString = '"use strict"; var message = "Hello World"; console.log(message);'
   eval(jsString)
   
   console.log(message)
   ```

8.  严格模式下，this绑定不会默认转成对象

```js
"use strict"

// 在严格模式下, 自执行函数(默认绑定)会指向undefined
// 之前编写的代码中, 自执行函数我们是没有使用过this直接去引用window
function foo() {
  console.log(this)  
    //以前如果使用window里面的内容 就是 window.localstorage.setItem
}

var obj = {
  name: "why",
  foo: foo
}

foo()  //window 是不是严格执行下的this指向 如果开启严格模式后 foo函数里面的this指向undefined

obj.foo() //obj
var bar = obj.foo
bar() //undefined


// setTimeout的this
// fn.apply(this = window) setTimeout是一个黑盒子 浏览器内部给我们执行 
// 所以说我们不知道这个具体的this绑定细节   fn.apply(this = window) 里面是apply执行
setTimeout(function() {
  console.log(this)  //严格模式下 this指向依然是一个window
}, 1000);

```

### JS面向对象

#### JS创建对象的方式

```js
// 创建一个对象, 对某一个人进行抽象(描述)

// 1.创建方式一: 通过new Object()创建
var obj = new Object()
obj.name = "why"
obj.age = 18
obj.height = 1.88
obj.running = function() {
  console.log(this.name + "在跑步~")
}

// 2.创建方式二: 字面量形式
var info = {
  name: "kobe",
  age: 40,
  height: 1.98,
  eating: function() {
    console.log(this.name + "在吃东西~")
  }
}
```

#### JS对对象进行操作的方式

```js
var obj = {
  name: "why",
  age: 18
}

// 获取属性
console.log(obj.name)

// 给属性赋值
obj.name = "kobe"
console.log(obj.name)

// 删除属性
 delete obj.name
 console.log(obj)

// 需求: 对属性进行操作时, 进行一些限制
// 限制: 不允许某一个属性被赋值/不允许某个属性被删除/不允许某些属性在遍历时被遍历出来

// 遍历属性
for (var key in obj) {
  console.log(key)
}

```

如果我们想要对一个属性进行比较精准的操作控制，那么我们就可以使用属性描述符。 

通过属性描述符可以精准的添加或修改对象的属性；

属性描述符需要使用 Object.defineProperty 来对属性进行添加或者修改

```js
//Object.defineProperty() 方法会直接在一个对象上定义一个新属性，或者修改一个对象的现有属性，并返回此对象。
Object.defineProperty(obj,prop,descriptor)
//可接收三个参数：
//obj要定义属性的对象；
//prop要定义或修改的属性的名称或 Symbol；
//descriptor要定义或修改的属性描述符；
//返回值：
//被传递给函数的对象
```

#### defineProperty方法

```js
var obj = {
  name: "why",
  age: 18
}

// 属性描述符是一个对象 
//第一个参数是要操作哪一个对象 第二个参数 要对哪一个属性操作，属性描述符
Object.defineProperty(obj, "height", {
  // 很多的配置
  value: 1.88
})

console.log(obj)//里面是有对象的 但是 新添加的是不可以枚举的 现在还是不可以显示的
console.log(obj.height) 

```

##### 数据属性描述符

**[[Configurable]]**：表示属性是否可以通过delete删除属性，是否可以修改它的特性，或者是否可以将它修改为存取属性 描述符； 

当我们直接在一个对象上定义某个属性时，这个属性的[[Configurable]]为true； 

当我们通过属性描述符定义一个属性时，这个属性的[[Configurable]]默认为false； 

 **[[Enumerable]]：**表示属性是否可以通过for-in或者Object.keys()返回该属性； 

 当我们直接在一个对象上定义某个属性时，这个属性的[[Enumerable]]为true； 

 当我们通过属性描述符定义一个属性时，这个属性的[[Enumerable]]默认为false； 

 **[[Writable]]：**表示是否可以修改属性的值； 

 当我们直接在一个对象上定义某个属性时，这个属性的[[Writable]]为true； 

当我们通过属性描述符定义一个属性时，这个属性的[[Writable]]默认为false； 

 **[[value]]**：属性的value值，读取属性时会返回该值，修改属性时，会对其进行修改； 

 默认情况下这个值是undefined

```js
// name和age虽然没有使用属性描述符来定义, 但是它们也是具备对应的特性的

// value: 赋值的value
// configurable: true
// enumerable: true
// writable: true
var obj = {
  name: "why",
  age: 18
}

// 数据属性描述符
// 用了属性描述符, 那么会有默认的特性
Object.defineProperty(obj, "address", {
  // 很多配置
  // value: "北京市", // 默认值undefined
  // 该特殊不可删除/也不可以重新定义属性描述符
  // configurable: false, // 默认值false
  // // 该特殊是配置对应的属性(address)是否是可以枚举
  // enumerable: true, 可以枚举 // 默认值false  不可以枚举
  // // 该特性是属性是否是可以赋值(写入值) 
  // writable: false // 默认值false 不可以修改  改成true object.defineProperty就可以修改了
})

// 测试configurable的作用
// delete obj.name
// console.log(obj.name)
// delete obj.address
// console.log(obj.address)

// Object.defineProperty(obj, "address", {
//   value: "广州市",
//   configurable: true
// })

// 测试enumerable的作用
console.log(obj)
for (var key in obj) {
  console.log(key)
}
console.log(Object.keys(obj))

// 测试Writable的作用
// obj.address = "上海市"
// console.log(obj.address)

```

##### 存取属性描述符

数据数据描述符有如下四个特性： 

 **[[Configurable]]：**表示属性是否可以通过delete删除属性，是否可以修改它的特性，或者是否可以将它修改为存取属性 描述符； 

 和数据属性描述符是一致的； 

当我们直接在一个对象上定义某个属性时，这个属性的[[Configurable]]为true； 

 当我们通过属性描述符定义一个属性时，这个属性的[[Configurable]]默认为false； 

 **[[Enumerable]]：**表示属性是否可以通过for-in或者Object.keys()返回该属性； 

和数据属性描述符是一致的； 

 当我们直接在一个对象上定义某个属性时，这个属性的[[Enumerable]]为true； 

 当我们通过属性描述符定义一个属性时，这个属性的[[Enumerable]]默认为false； 

 **[[get]]：**获取属性时会执行的函数。默认为undefined 

 **[[set]]：**设置属性时会执行的函数。默认为undefined 

```js
var obj = {
  name: "why",
  age: 18,
  _address: "北京市"
}

// 存取属性描述符
// 1.隐藏某一个私有属性被希望直接被外界使用和赋值
// 2.如果我们希望截获某一个属性它访问和设置值的过程时, 也会使用存储属性描述符
Object.defineProperty(obj, "address", {
  enumerable: true,
  configurable: true,
    //获取值
  get: function() {
    foo()
    return this._address
  },
    //修改值
  set: function(value) {
    bar()
    this._address = value
  }
})

console.log(obj.address)

obj.address = "上海市"
console.log(obj.address)

function foo() {
  console.log("获取了一次address的值")
}

function bar() {
  console.log("设置了addres的值")
}

```

##### JS定义多个属性描述符

Object.defineProperties() 方法直接在一个对象上定义 多个 新的属性或修改现有属性，并且返回该对象。

```js
//私有属性的描述
var obj = {
  // 私有属性(js里面是没有严格意义的私有属性)
  //社区里面 一般以下划线开头的属性 或者 方法 就是一个私有方法
  _age: 18,
  _eating: function() {
      console.log('aaaaa')
  },
  //给对象某一个属性定义 get 和 set
  set age(value) {
    this._age = value
  },
  get age() {
    return this._age
  }
}

Object.defineProperties(obj, {
  name: {
    configurable: true,//表示是否可以删除或者修改
    enumerable: true, //表示是否可以枚举
    writable: true,
    value: "why"
  },
  age: {
    configurable: true,
    enumerable: true,
    get: function() {
      return this._age
    },
    set: function(value) {
      this._age = value
    }
  }
})

obj.age = 19
console.log(obj.age)

console.log(obj)

```

#### 对象方法的扩充

获取对象的属性描述符： **getOwnPropertyDescriptor**    **getOwnPropertyDescriptors** 

```js
var obj = {
  // 私有属性(js里面是没有严格意义的私有属性)
  _age: 18,
  _eating: function() {}
}

Object.defineProperties(obj, {
  name: {
    configurable: true,
    enumerable: true,
    writable: true,
    value: "why"
  },
  age: {
    configurable: true,
    enumerable: true,
    get: function() {
      return this._age
    },
    set: function(value) {
      this._age = value
    }
  }
})

// 获取某一个特性属性的属性描述符
console.log(Object.getOwnPropertyDescriptor(obj, "name"))
//{ value: 'why', writable: true, enumerable: true, configurable: true }
console.log(Object.getOwnPropertyDescriptor(obj, "age"))
/*{
  get: [Function: get],
  set: [Function: set],
  enumerable: true,
  configurable: true
}*/

// 获取对象的所有属性描述符
console.log(Object.getOwnPropertyDescriptors(obj))
```

禁止对象扩展新属性：preventExtensions 

给一个对象添加新的属性会失败（在严格模式下会报错）； 

```js
var obj = {
  name: 'why',
  age: 18
}

// 1.禁止对象继续添加新的属性  preventExtensions 阻止对象扩展
Object.preventExtensions(obj)

obj.height = 1.88
obj.address = "广州市"

console.log(obj) //{ name: 'why', age: 18 }
```

 **密封对象，不允许配置和删除属性：seal** 

实际是调用preventExtensions 

并且将现有属性的configurable:false 

```js
// 2.禁止对象配置/删除里面的属性
Object.seal(obj)

delete obj.name
console.log(obj.name) //why 如果没写.seal 那么就给删除了 就是undefined了
```

 **冻结对象，不允许修改现有属性： freeze** 

```js
// 3.让属性不可以修改(writable: false)
Object.freeze(obj)
obj.name = "kobe"
console.log(obj.name) //why 
```

实际上是调用seal  并且将现有属性的writable: false

### 创建对象

##### 1.new Object方式； 

##### 2.字面量创建的方式；

```js
var p1 = {
  name: "张三",
  age: 18,
  height: 1.88,
  address: "广州市",
  eating: function() {
    console.log(this.name + "在吃东西~")
  },
  running: function() {
    console.log(this.name + "在跑步~")
  }
}

var p2 = {
  name: "李四",
  age: 20,
  height: 1.98,
  address: "北京市",
  eating: function() {
    console.log(this.name + "在吃东西~")
  },
  running: function() {
    console.log(this.name + "在跑步~")
  }
}

var p3 = {
  name: "王五",
  age: 30,
  height: 1.78,
  address: "上海市",
  eating: function() {
    console.log(this.name + "在吃东西~")
  },
  running: function() {
    console.log(this.name + "在跑步~")
  }
}
```

可以看到除了名字 年龄 身高 地址 改变了 其他基本没有改变 所以这种方法如果创建大量的对象是非常麻烦的；

##### 3.工厂模式

工厂模式其实是一种常见的设计模式； 

通常我们会有一个工厂方法，通过该工厂方法我们可以产生想要的对象；

```js
// 工厂模式: 工厂函数
function createPerson(name, age, height, address) {
  var p = {}
  p.name = name;
  p.age = age;
  p.height = height;
  p.address = address;

  p.eating = function() {
    console.log(this.name + "在吃东西~")
  }

  p.running = function() {
    console.log(this.name + "在跑步~")
  }

  return p
}

var p1 = createPerson("张三", 18, 1.88, "广州市")
var p2 = createPerson("李四", 20, 1.98, "上海市")
var p3 = createPerson("王五", 30, 1.78, "北京市")

// 工厂模式的缺点(获取不到对象最真实的类型) 只能看到Object类型
console.log(p1, p2, p3)
```

工厂方法创建对象有一个比较大的问题：我们在打印对象时，对象的类型都是Object类型 但是从某些角度来说，这些对象应该有一个他们共同的类型；

##### 4.构造函数

我们先理解什么是构造函数？ 

```js
function foo() {
  console.log("foo~, 函数体代码")
}

// foo就是一个普通的函数
// foo()

// 换一种方式来调用foo函数: 通过new关键字去调用一个函数, 那么这个函数就是一个构造函数了
var f1 = new foo() //后面写小括号的原因是要传递参数
console.log(f1)


// 当我们通过new去调用一个函数时, 和普通的调用到底有什么区别?


```

构造函数也称之为构造器（constructor），通常是我们在创建对象时会调用的函数； 

在其他面向的编程语言里面，构造函数是存在于类中的一个方法，称之为构造方法； 但是JavaScript中的构造函数有点不太一样； 

 JavaScript中的构造函数是怎么样的？ 

构造函数也是一个普通的函数，从表现形式来说，和千千万万个普通的函数没有任何区别； 

**那么如果这么一个普通的函数被使用new操作符来调用了，那么这个函数就称之为是一个构造函数；** 

###  *那么被new调用有什么特殊的呢？* 

如果一个函数被使用new操作符调用了，那么它会执行如下操作： 

1. 在内存中创建一个新的对象（空对象）； 
2. 这个对象内部的[[prototype]]【--proto--】属性会被赋值为该构造函数的prototype属性；
3. 构造函数内部的this，会指向创建出来的新对象；
4. 执行函数的内部代码（函数体代码）；   
5. 如果构造函数没有返回非空对象，则返回创建出来的新对象；

```JS
// 规范: 构造函数的首字母一般是大写
function Person(name, age, height, address) {
  this.name = name
  this.age = age
  this.height = height
  this.address = address

  this.eating = function() {
    console.log(this.name + "在吃东西~")
  }

  this.running = function() {
    console.log(this.name + "在跑步")
  }
}
//不需要手动返回 因为执行后 也会自动返回

var p1 = new Person("张三", 18, 1.88, "广州市")
var p2 = new Person("李四", 20, 1.98, "北京市")

console.log(p1)
console.log(p2)
p1.eating()
p2.eating()

```

构造函数缺点：它在于我们需要为每个对象的函数去创建一个函数对象实例；当属性值是函数的时候，会重复调用函数，也会造成浪费

### 原型和原型链

 JavaScript当中每个对象都有一个特殊的内置属性 [[prototype]]，这个特殊的对象可以指向另外一个对象。  那么这个对象有什么用呢？ 

当我们通过引用对象的属性key来获取一个value时，它会触发 [[Get]]的操作； 这个操作会首先检查该属性是否有对应的属性，如果有的话就使用它； 

如果对象中没有改属性，那么会访问对象[[prototype]]内置属性指向的对象上的属性； 

那么如果通过字面量直接创建一个对象，这个对象也会有这样的属性吗？如果有，应该如何获取这个属性呢？ 答案是有的，只要是对象都会有这样的一个内置属性； 

获取的方式有两种

方式一：通过对象的 __proto__ 属性可以获取到（但是这个是早期浏览器自己添加的，存在一定的兼容性问 题）； 方式二：通过 Object.getPrototypeOf 方法可以获取到

##### 对象的原型

```JS
// 我们每个对象中都有一个 [[prototype]], 这个属性可以称之为对象的原型(隐式原型)
//对象的原型 一般被称之为隐式原型
var obj = { name: "why" } // [[prototype]]
var info = {} // [[prototype]]

// 1.解释原型的概念和看一下原型
// 早期的ECMA是没有规范如何去查看 [[prototype]]

// 给对象中提供了一个属性, 可以让我们查看一下这个原型对象(浏览器提供)
// __proto__

// console.log(obj.__proto__) //默认是一个空的{}
// console.log(info.__proto__) //默认是一个空的 {}

// var obj = {name: "why", __proto__: {} }

// // ES5之后提供的Object.getPrototypeOf
// console.log(Object.getPrototypeOf(obj))


// 2.原型有什么用呢?
// 当我们从一个对象中获取某一个属性时, 它会触发 [[get]] 操作
// 1. 在当前对象中去查找对应的属性, 如果找到就直接使用
// 2. 如果没有找到, 那么会沿着它的原型去查找 [[prototype]]
// obj.age = 18
obj.__proto__.age = 18

console.log(obj.age)

```

##### 函数的原型

```js
function foo() {
}

// 函数也是一个对象
// console.log(foo.__proto__) // 函数作为对象来说, 它也是有[[prototype]] 隐式原型

// 函数它因为是一个函数, 所以它还会多出来一个显示原型属性: prototype
console.log(foo.prototype)

var f1 = new foo()
var f2 = new foo()

console.log(f1.__proto__ === foo.prototype) //true
console.log(f2.__proto__ === foo.prototype) //true

```

##### constructor属性

```js
function foo() {

}

// 1.constructor属性
// foo.prototype这个对象中有一个constructor属性 可枚举型是看不见的 他是创建对象的时候 JS引擎自动添加的 指向构造函数本身 

// console.log(foo.prototype)
// console.log(Object.getOwnPropertyDescriptors(foo.prototype))

// console.log(foo.prototype.constructor) // [Function: foo]



// 2.我们也可以添加自己的属性
// foo.prototype.name = "why"
// foo.prototype.age = 18
// foo.prototype.height = 18
// foo.prototype.eating = function() {
// }

var f1 = new foo()
console.log(f1.name, f1.age)


// 3.直接修改整个prototype对象 当添加属性过多的时候 就会添加一个新的对象prototype
foo.prototype = {
  // constructor: foo,
  name: "why",
  age: 18,
  height: 1.88
}

var f1 = new foo()

console.log(f1.name, f1.age, f1.height)

// 真实开发中我们可以通过Object.defineProperty方式添加constructor
Object.defineProperty(foo.prototype, "constructor", {
  enumerable: false, //可枚举型
  configurable: true, //是否可以删除属性
  writable: true, //是否可以修改
  value: foo //内容
})
```

##### 创建对象的方法-原型和构造函数

```js
function Person(name, age, height, address) {
  this.name = name
  this.age = age
  this.height = height
  this.address = address
}

Person.prototype.eating = function() {
  console.log(this.name + "在吃东西~")
}

Person.prototype.running = function() {
  console.log(this.name + "在跑步~")
}

var p1 = new Person("why", 18, 1.88, "北京市")
var p2 = new Person("kobe", 20, 1.98, "洛杉矶市")

p1.eating()
p2.eating()
```

### JS中的类和对象

其实ES6中的类就是构造函数中的语法糖

#### 面向对象的三大特性

封装，继承，多态

封装：我们前面将属性和方法封装到一个类中，可以称之为封装的过程； 

继承：继承是面向对象中非常重要的，不仅仅可以减少重复代码的数量，也是多态前提（纯面向对象中）； 

多态：不同的对象在执行时表现出不同的形态

##### 继承

继承可以帮助我们将重复的代码和逻辑抽取到父类中，子类只需要直接继承过来使用即可

利用原型链的机制来实现继承

##### 原型链

```js
var obj = {
  name: "why",
  age: 18
}

// [[get]]操作
// 1.在当前的对象中查找属性
// 2.如果没有找到, 这个时候会去原型链(__proto__)对象上查找


//因为__proto__对应的也是一个对象 所以他也是有__proto__的
obj.__proto__ = {  
}

// 原型链
obj.__proto__.__proto__ = {
  
}

obj.__proto__.__proto__.__proto__ = {
  address: "上海市"
}

console.log(obj.address)


```

##### 顶层原型是什么

```js
var obj = { name: "why" }

// console.log(obj.address)

// 到底是找到哪一层对象之后停止继续查找了呢?
// 字面对象obj的原型是 [Object: null prototype] {}
// [Object: null prototype] {} 就是顶层的原型

console.log(obj.__proto__) // [Object: null prototype] {}

// obj.__proto__ => [Object: null prototype] {}

console.log(obj.__proto__.__proto__)  //null

```

##### 顶层原型来自哪里

```js
// var obj1 = {} // 创建了一个对象 
// var obj2 = new Object() // 创建了一个对象

// function Person() {
// }
// var p = new Person()

var obj = {
  name: "why",
  age: 18
}

var obj2 = {
  // address: "北京市"
}
obj.__proto__ = obj2

// Object.prototype
// console.log(obj.__proto__)  //[Object: null prototype] {}
// console.log(Object.prototype)  //[Object: null prototype] {}
// console.log(obj.__proto__ === Object.prototype)   //true
 
console.log(Object.prototype)
console.log(Object.prototype.constructor)
console.log(Object.prototype.__proto__)

console.log(Object.getOwnPropertyDescriptors(Object.prototype))

```

[Object: null prototype] {} 原型有什么特殊吗？

特殊一：该对象有原型属性，但是它的原型属性已经指向的是null，也就是已经是顶层原型了； 

特殊二：该对象上有很多默认的属性和方法

#### 关于继承  详细看代码 mypractice

###### 1.原型链的继承方案

将子类的原型等于父类的实例对象

```js
// 父类: 公共属性和方法
function Person() {
  this.name = "why"
  this.friends = [];
}

Person.prototype.eating = function() {
  console.log(this.name + " eating~")
}

// 子类: 特有属性和方法
function Student() {
  this.sno = 111
}
/* 子类的原型等于父类的实例对象 必须写在子类原型赋值之前  否则就给原来的实例重新赋值了 */
var p = new Person()
Student.prototype = p

Student.prototype.studying = function() {
  console.log(this.name + " studying~")
}

 
// name/sno
var stu = new Student()

// console.log(stu.name)
// stu.eating()

// stu.studying()
```

弊端

```js
// 原型链实现继承的弊端:
// 1.第一个弊端: 打印stu对象, 继承的属性是看不到的
// console.log(stu.name)

// 2.第二个弊端: 创建出来两个stu的对象如果修改引用值的话 就全部修改了
var stu1 = new Student()
var stu2 = new Student()

// 直接修改对象上的属性, 是给本对象添加了一个新属性
stu1.name = "kobe"
console.log(stu2.name)

// 获取引用, 修改引用中的值, 会相互影响 改了 stu1的东西 stu2也会改变
// 如果 stu1.friends = ['aaa'] 这样写的话 是重新赋值了 //set操作
stu1.friends.push("kobe") //get操作 顺着原型链去找
console.log(stu1.friends)
console.log(stu2.friends)

// 3.第三个弊端: 在前面实现类的过程中都没有传递参数
var stu3 = new Student("lilei", 112)
```

###### 2.借用构造函数继承

为了解决原型链继承中存在的问题，开发人员提供了一种新的技术: constructor stealing(有很多名称: 借用构造函 数或者称之为经典继承或者称之为伪造对象)： 

steal是偷窃、剽窃的意思，但是这里可以翻译成借用； 

借用继承的做法非常简单：在子类型构造函数的内部调用父类型构造函数. 

因为函数可以在任意的时刻被调用； 

因此通过apply()和call()方法也可以在新创建的对象上执行构造函数

```js
// 父类: 公共属性和方法
function Person(name, age, friends) {
  // this = stu
  this.name = name
  this.age = age
  this.friends = friends
}

Person.prototype.eating = function() {
  console.log(this.name + " eating~")
}

// 子类: 特有属性和方法
function Student(name, age, friends, sno) {
  //通过call的方式调用上面的Person 并且给new Student创建出来的this传进去
  Person.call(this, name, age, friends)
  //this是student里面的对象  
  this.sno = 111
}
        
var p = new Person() //保证函数继承过来的东西
Student.prototype = p

Student.prototype.studying = function() {
  console.log(this.name + " studying~")
}


// name/sno
var stu = new Student("why", 18, ["kobe"], 111)

// console.log(stu.name)
// stu.eating()

// stu.studying()


// 原型链实现继承已经解决的弊端
// 1.第一个弊端: 打印stu对象, 继承的属性是看不到的
console.log(stu)

// 2.第二个弊端: 创建出来两个stu的对象
var stu1 = new Student("why", 18, ["lilei"], 111)
var stu2 = new Student("kobe", 30, ["james"], 112)

// // 直接修改对象上的属性, 是给本对象添加了一个新属性
// stu1.name = "kobe"
// console.log(stu2.name)

// // 获取引用, 修改引用中的值, 会相互影响
stu1.friends.push("lucy")

console.log(stu1.friends)
console.log(stu2.friends)

// // 3.第三个弊端: 在前面实现类的过程中都没有传递参数
// var stu3 = new Student("lilei", 112)
```

弊端

```JS
 强调: 借用构造函数也是有弊端:
 1.第一个弊端: Person函数至少被调用了两次  //new一次  Person.call 被调用一次
 2.第二个弊端: stu的原型对象上会多出一些属性, 但是这些属性是没有存在的必要
 多出来的属性？？？？ 意思就是说，在你执行var p = new Person() 将person中的东西 放在p上面了 
  所以多出来三个undefined属性
```

子类原型的对象为什么不能等于父类原型的对象？

```js
// 父类: 公共属性和方法
function Person(name, age, friends) {
  // this = stu
  this.name = name
  this.age = age
  this.friends = friends
}

Person.prototype.eating = function() {
  console.log(this.name + " eating~")
}

// 子类: 特有属性和方法
function Student(name, age, friends, sno) {
  Person.call(this, name, age, friends)
  // this.name = name
  // this.age = age
  // this.friends = friends
  this.sno = 111
}

// 直接将父类的原型赋值给子类, 作为子类的原型
Student.prototype = Person.prototype

//下面这个操作就给加入到person的原型里面了 明明是给子类原型加的但是加到父类原型里面了
Student.prototype.studying = function() {
  console.log(this.name + " studying~")
}


// name/sno
var stu = new Student("why", 18, ["kobe"], 111)
console.log(stu)
stu.eating()
```

理想状态下的继承

![image-20210929100542434](./media/理想状态下的继承.png)

###### 3.原型式继承实现    

  道格拉斯提出 新创建一个对象并且用原来某一个对象作为其原型的方法

```js
//道格拉斯提出原型式继承
//这种继承仅仅局限于对象 
var obj ={
    name:'wyd',
    age:19
}

//var info = {};
//想要info的原型指向 obj 才能实现继承的理想化

//方法一
function createObject1(o) {
    //想要实现 o为新创建出来某一个对象的原型的
    var newObj={};
    //将newObj的原型设置为传进来的o对象
  Object.setPrototypeOf(newObj,o)
    return newObj
}

// 方法二 道格拉斯实现方法
function createObject2(o) {
   //定义一个函数Fn
    function Fn() {}
    //让Fn的原型等于 传进来的对象
    Fn.prototype = o;
    //newObj.__proto__ = Fn.prototype
    var newObj = new Fn();
   return newObj
}

//方法三 将OBJ作为新创建出来对象的原型
var nice = Object.create(obj);
console.log('nice',nice);//nice {}
console.log('nice.__proto__',nice.__proto__); //nice.__proto__ { name: 'wyd', age: 19 }


var info =createObject1(obj)
console.log(info);//{}
console.log(info.__proto__);//{ name: 'wyd', age: 19 }



var info =createObject2(obj)
console.log(info);//{}
console.log(info.__proto__);//{ name: 'wyd', age: 19 }

```

###### 4.寄生式继承

```js
//这种继承也局限于对象  寄生shi继承+工厂函数
var Personobj ={
   running:function(){
       console.log('running');
   }
}

var Student = Object.create(Personobj); 
//如果以这样的方法进行操作的话 创建一百个学生 
//那就要给一百个学生创建各种的方法和属性
/* 
 var Student1 = Object.create(Personobj); 
 Student1.name='wyd',
 Student1.age=20,
 Student1.studying = function(){} 


 var Student2 = Object.create(Personobj); 
 Student2.name='wyd',
 Student2.age=20,
 Student2.studying = function(){} 
*/

//借用工厂函数的思想 将Personobj的原型赋值给stu对象
function createStudent(name) {
    var stu = Object.create(Personobj) //原型式继承+工厂函数
    stu.name = name 
    stu.studying = function () {
        console.log('studying~');
    }
    return stu
}

var stuObj = createStudent("wyd")
var stuObj1 = createStudent("kobe")
var stuObj2 = createStudent("james")
```

###### 5.寄生组合式继承

```js
//父类
function Person(name, age, friends) {
    this.name = name
    this.age = age
    this.friends = friends
  }
  
  Person.prototype.running = function() {
    console.log("running~")
  }
  
  Person.prototype.eating = function() {
    console.log("eating~")
  }


   //学生
  function Student(name,age,friends,sno,score) {
      Person.call(this,name,age,friends)
      this.sno=sno;
      this.score=score;
  }

  //Object.create(Person.prototype) 创建一个对象 原型是Person的原型 
  //赋值给Student的原型
  Student.prototype =Object.create(Person.prototype)
  //保证constructor指向
  Object.defineProperty(Student.prototype, "constructor", {
    enumerable: false,
    configurable: true,
    writable: true,
    value: Student
  })
 

  Student.prototype.studying = function() {
    console.log("studying~")
  }


var stu = new Student("wyd", 18, ["kobe"], 111, 100)
console.log(stu) 
stu.studying()
stu.running()
stu.eating()
/* 
Person {
  name: 'wyd',
  age: 18,
  friends: [ 'kobe' ],
  sno: 111,
  score: 100
} 
studying~
running~
eating~

为什么是Person类型呢？

因为他是去找我们的当前对象的constructor.name
console.log(stu.constructor.name)
  Student.prototype =Object.create(Person.prototype) 
  因为Student的原型改变了 没有自己的constructor   但是输出的时候还是回去找 就去原型链上找
  constructor 指向Person
*/


/* 
//通过Object.defineProperty来添加constructor属性
 Object.defineProperty(Student.prototype, "constructor", {
    enumerable: false,
    configurable: true,
    writable: true,
    value: Student
  })
*/

 /*  
  可以指向Student了 保证constructor指向
  Student {
  name: 'wyd',        
  age: 18,
  friends: [ 'kobe' ],
  sno: 111,
  score: 100
}
studying~
running~
eating~
  */
```

###### 6.简化寄生组合式继承

```js

function createObject(o) {
    function Fn() {}
    Fn.prototype = o ;
   return  new Fn();
}


//封装继承函数
function inheritPrototype(Subtype,SuperType) {
    //Object.create(SuperType.prototype) 创建一个对象 原型是SuperType的原型 
  //赋值给Subtype的原型

  //可以用Object.create 也可以用createObject自己封装的
//   Subtype.prototype =Object.create(SuperType.prototype)
Subtype.prototype =createObject(SuperType.prototype)
  //保证constructor指向
  Object.defineProperty(Subtype.prototype, "constructor", {
    enumerable: false,
    configurable: true,
    writable: true,
    value: Subtype
  })
  }







//父类
function Person(name, age, friends) {
    this.name = name
    this.age = age
    this.friends = friends
  }
  
  Person.prototype.running = function() {
    console.log("running~")
  }
  
  Person.prototype.eating = function() {
    console.log("eating~")
  }


   //学生
  function Student(name,age,friends,sno,score) {
      Person.call(this,name,age,friends)
      this.sno=sno;
      this.score=score;
  }

  inheritPrototype(Student,Person)
 

  Student.prototype.studying = function() {
    console.log("studying~")
  }


var stu = new Student("wyd", 18, ["kobe"], 111, 100)
console.log(stu) 
stu.studying()
stu.running()
stu.eating()

 /*  
  可以指向Student了 保证constructor指向
  Student {
  name: 'wyd',        
  age: 18,
  friends: [ 'kobe' ],
  sno: 111,
  score: 100
}
studying~
running~
eating~
  */
```

#### JS原型内容的补充

###### 1.hasOwnProperty

对象是否有某一个属于自己的属性（不是在原型上的属性）

```js
var obj = {
    name: "why",
    age: 18
  }

  //可以接受更多的参数 
  //将obj的作为info的原型使用
  var info = Object.create(obj, {
      //传入对应的描述符
    address: {
      value: "北京市",
      enumerable: true
    }
  })

  console.log(info); //{ address: '北京市' }

  console.log(info.__proto__); //{ name: 'why', age: 18 }

  //判断属性是否在对象上面 还是在原型上面
  console.log(info.hasOwnProperty("address")) //true
 console.log(info.hasOwnProperty("name"))  //false

```

###### 2.instanceof

用于检测构造函数的pototype，是否出现在某个实例对象的原型链上

A instanceof B 判断 B是否出现在A的原型链上

B只能是构造函数  console.log(stu instanceof Student) Student是构造函数

```js
//用于检测构造函数的pototype，是否出现在某个实例对象的原型链上
function createObject(o) {
    function Fn() {}
    Fn.prototype = o
    return new Fn()
  }
  
  function inheritPrototype(SubType, SuperType) {
    SubType.prototype = createObject(SuperType.prototype)
    Object.defineProperty(SubType.prototype, "constructor", {
      enumerable: false,
      configurable: true,
      writable: true,
      value: SubType
    })
  }
   
  function Person() {
  
  }
  
  function Student() {
  
  }
  //前面的继承后面的
  inheritPrototype(Student, Person) 

  var stu = new Student();

  // A instanceof B 判断 B是否出现在A的原型链上
  console.log(stu instanceof Student);//true
  console.log(stu instanceof Person);//true
  console.log(stu instanceof Object);//true
```

###### 3.isPrototypeOf

用于检测某个对象，是否出现在某个实例对象的原型链上

```js
function Person() {

}

var p = new Person()

//instanceof 第二个参数只能传入构造函数
console.log(p instanceof Person) //true
//表示p对象是否出现在Person.prototype上面
console.log(Person.prototype.isPrototypeOf(p))

// 
var obj = {
  name: "why",
  age: 18
}

var info = Object.create(obj)

 //console.log(info instanceof obj) //报错 因为第二个参数必须是构造函数
 //两个都是对象
console.log(obj.isPrototypeOf(info))

```

###### 4.in/for in 操作符

判断某个属性是否在某个对象或者对象的原型上

```js
var obj = {
    name: "why",
    age: 18
  }

  //可以接受更多的参数 
  //将obj的作为info的原型使用
  var info = Object.create(obj, {
      //传入对应的描述符
    address: {
      value: "北京市",
      enumerable: true
    }
  })
  
//in 操作符  不管在当前对象还是原型中返回的都是true
//只要在属性中 就可以返回true
 console.log('address' in info); //true
 console.log('name' in info); //true

 // for in
for (var key in info) {
  console.log(key)
} 

/* 
address
name
age
*/

```

###### 对象 函数 原型 之间的关系

```js
var obj = {
    name: "why"
  }
  
  console.log(obj.__proto__)


  //函数本身也是一个对象
  
  // 对象里面是有一个__proto__对象: 隐式原型对象
  
  // Foo是一个函数, 那么它会有一个显示原型对象: Foo.prototype
  // Foo.prototype来自哪里?
  // 答案: 一旦创建了一个函数, 
  //JS内部会帮你创建对象 Foo.prototype = { constructor: Foo }
  
  // Foo是一个对象, 那么它会有一个隐式原型对象: Foo.__proto__
  // Foo.__proto__来自哪里?
  // 答案: new Function()  Foo.__proto__ = Function.prototype
  // Function.prototype = { constructor: Function }
  
  // var Foo = new Function()
  function Foo() {
  
  }
  
  console.log(Foo.prototype === Foo.__proto__)//false
  console.log(Foo.prototype.constructor)//[Function: Foo]
  console.log(Foo.__proto__.constructor)//[Function: Function]
  
  
  var foo1 = new Foo()
  var obj1 = new Object()
  
  console.log(Object.getOwnPropertyDescriptors(Function.__proto__))
 
```

**原型图**![image-20210929154802689](./media/原型图.png)

### ES6 class类

我们会发现，按照前面的构造函数形式创建 类，不仅仅和编写普通的函数过于相似，而且代码并不容易理解。

在ES6（ECMAScript2015）新的标准中使用了class关键字来直接定义类； 

但是类本质上依然是前面所讲的构造函数、原型链的语法糖而已； 

所以学好了前面的构造函数、原型链更有利于我们理解类的概念和继承关系； 

那么，如何使用class来定义一个类呢？ 

可以使用两种方式来声明类：类声明和类表达式

###### 类的声明

```js
//类的声明
class Person {

}

//类的表达式
var Animal = class {

}

//研究一下类的特点
console.log(Person.prototype); //Person {}
console.log(Person.prototype.__proto__);//{}

console.log(Person.prototype.constructor)//[Function: Person]

//typeof 返回的东西本来就是固定的 只能返回那几个
console.log(typeof Person); //function


var p = new Person()
console.log(p.__proto__ === Person.prototype) //true
```

###### 类的构造方法

  1.在内存中创建一个对象 moni = {}
  2.将类的原型prototype赋值给创建出来的对象 moni.__proto__ = Person.prototype
  3.将对象赋值给函数的this: new绑定 this = moni
  4.执行函数体中的代码
  5.自动返回创建出来的对象

```js
class Person{
    //类的构造方法
    //注意 一个类只能有一个构造函数
 
    constructor(name,age){
     this.name=name;
     this.age=age;
    }
}

var p1 = new Person("why", 18)//Person { name: 'why', age: 18 } 
var p2 = new Person("kobe", 30)//Person { name: 'kobe', age: 30 }
console.log(p1, p2)
```

###### 类中的方法定义

```js
var names = ["abc", "cba", "nba"]

class Person {
    //constructor 设置属性的构造器
    constructor(name, age) {
        this.name = name;
        this.age = age;
        this._address = '北京市'
    }
    //类里面的实例方法 
    //一般通过创建出来的对象来进行访问
    // var p = new Person("wyd", 18)
    // p.eating()
    eating() {
        console.log(this.name + 'eating');
    }

    running() {
        console.log(this.name + 'running');
    }

    //类的访问器方法
    get address() {
        // console.log("拦截访问操作")
        return this._address
    }

    set address(value) {
        // console.log("拦截设置操作")
        this._address = value
    }


    //类的静态方法 /类方法 通过类名直接访问的方法
    // Person.createPerson()
    static randomPerson() {
        var nameIndex =Math.floor(Math.random()*names.length)
        var name = names[nameIndex]
        var age = Math.floor(Math.random()*100)
         return new Person(name,age)
    }
}

//实例方法测试
var p = new Person("wyd", 18)
p.eating() //wyd eating
p.running() //wyd running

//访问器方法测试
console.log(p.address); //北京市
//类的拦截器设置
p.address = '广州市'
console.log(p.address); //广州市

//静态方法（类方法）测试
for (let i = 0; i < 20; i++) {
    //直接用类名类调用里面的方法就可以了
    console.log(Person.randomPerson());
}
```

###### 类中继承的实现 extends

```js
class Person {
    constructor(name, age) {
        this.name = name
        this.age = age
    }

    running() {
        console.log(this.name + " running~")
    }

    eating() {
        console.log(this.name + " eating~")
    }

    personMethod() {
        console.log("处理逻辑1")
        console.log("处理逻辑2")
        console.log("处理逻辑3")
    }

    static PersonstaticMethod() {
        console.log("PersonStaticMethod")
    }
}

//student子类 继承Person 父类
class Student extends Person {
    // JS引擎在解析子类的时候就有要求, 如果我们有实现继承
    // 那么子类的构造方法中, 在使用this之前/使用return之前 必须用super调用父类的构造方法
    constructor(name, age, sno) {
        super(name, age)//调用父类的方法
        this.sno = sno
    }
    //子类对父类方法running的重写
    running() {
        console.log('student ' +  this.name + ' studying');
    }
    //重写personMethod方法 复用父类的同时 添加自己的方法
    personMethod(){
        super.personMethod();
        console.log('处理逻辑4');
        console.log('处理逻辑5');
        console.log('处理逻辑6');
    }
    //重写静态方法
    static PersonstaticMethod(){
        super.PersonstaticMethod();
        console.log('重写static方法');
    }
}

var stu1 = new Student('wyd', 19, 18100140406);
console.log(stu1);

stu1.eating();//wyd eating~

// stu1.running();//wyd running~ 
//继承来的方法 是在父类的原型之中 如果子类有自己的方法 就先使用自己的方法
stu1.running(); //student wyd studying

stu1.personMethod();
//静态方法只能通过类调用
Student.PersonstaticMethod();
```

###### 创建类继承内置类

```js
class Person {

}

class Student extends Person {

}
//继承自Object
// class foo extends Object{

// }

class foo {

}

//js开发中还是很少操作这样的方法的 
// 自己创建的类 继承 内置数组类 可以调用里面的方法
class wydArray extends Array {
    firstItem() {
        return this[0]
    }

    lastItem() {
        return this[this.length - 1]
    }
}

var arr = new Array(1, 2, 3, 4)
console.log(arr);

var wydarr = new wydArray(2,3,4,5,6,7)

console.log(wydarr.firstItem()); //2
console.log(wydarr.lastItem()); //7
```

###### JS实现混入效果

JavaScript的类只支持单继承：也就是只能有一个父类 

那么在开发中我们我们需要在一个类中添加更多相似的功能时，应该如何来做呢？ 

这个时候我们可以使用混入（mixin）；

```js
//混入 mixin 
/* 
js之中其实没有混入 只是用JS来实现某些效果
*/

class Person {

}

class Runner {
    running() {

    }
}

class Eater {
    eating() {

    }
}
//给传入的类 做一个混入的效果
function mixinRunner(BaseClass) {
    class NewClass extends BaseClass {
        running() {
            console.log('running');
        }
    }
    return NewClass
}

function mixinEater(BaseClass) {
    return class extends BaseClass {
      eating() {
        console.log("eating~")
      }
    }
  }

//JS中类只能有一个父类 ：单继承
class Student extends Person {

}

var NewStudent = mixinEater(mixinRunner(Student))

var ns = new NewStudent()
ns.running() //running
ns.eating() //eating
```

### JS中的多态

多态（英语：polymorphism）指为不同数据类型的实体提供统一的接口，或使用一 个单一的符号来表示多个不同的类型。 

非常的抽象，个人的总结：不同的数据类型进行同一个操作，表现出不同的行为，就是多态的体现。

*传统的面向对象多态是有三个前提:*

1> 必须有继承(是多态的前提)

2> 必须有重写(子类重写父类的方法)

3> 必须有父类引用指向子类对象

###### **传统面向对象中的多态：TS代码**

```ts
// 传统的面向对象多态是有三个前提:
// 1> 必须有继承(是多态的前提)
// 2> 必须有重写(子类重写父类的方法)
// 3> 必须有父类引用指向子类对象

// Shape形状
class Shape {
    //获取面积
  getArea() {}
}

// Rectangle 矩形
class Rectangle extends Shape {
  getArea() {
    return 100
  }
}

class Circle extends Shape {
  getArea() {
    return 200
  }
}

var r = new Rectangle()
var c = new Circle()

// 多态: 当对不同的数据类型执行同一个操作时, 如果表现出来的行为(形态)不一样, 那么就是多态的体现.

//要求类型必须是shape类型
function calcArea(shape: Shape) {
  console.log(shape.getArea())
}
//r 和 c 继承自 shape类型
calcArea(r)
calcArea(c)

export {}
```

###### JS多态的体现

```js
// 多态: 当对不同的数据类型执行同一个操作时,
// 如果表现出来的行为(形态)不一样, 那么就是多态的体现.
function calcArea(foo) {
  console.log(foo.getArea())
}

var obj1 = {
  name: "why",
  getArea: function() {
    return 1000
  }
}

class Person {
  getArea() {
    return 100
  }
}

var p = new Person()
//虽然没有继承 但是也是符合维基百科多态的状态
calcArea(obj1)
calcArea(p)


// 也是多态的体现
function sum(m, n) {
  return m + n
}
//不同数据类型 获取的行为和状态不一样 下面这种方法 也是多态的体现
sum(20, 30)
sum("abc", "cba")

```

### ES6

什么是字面量增强

 字面量的增强主要包括下面几部分： 

属性的简写：Property Shorthand 

方法的简写：Method Shorthand 

计算属性名：Computed Property Names

###### 字面量增强的写法

```js
var name = "why"
var age = 18

var obj = {
  // 1.property shorthand(属性的简写)
  /* 
  name:name,
  age:age,
  //ES6里面两个可以写成一个
  */
  name,
  age,

  // 2.method shorthand(方法的简写)
  foo: function() {
    console.log(this)
  },
  //ES6写法
  bar() {
    console.log(this)
  },
  baz: () => {
    console.log(this) 
  },

  // 3.computed property name(计算属性名)
  //以前写key obj[name + 123] = 'hahaha'
  [name + 123]: 'hehehehe'
}

obj.baz()//window
obj.bar() //obj
obj.foo() //obj

// obj[name + 123] = "hahaha"
console.log(obj)

```

###### 解构赋值Destructuring

数组解构

```js
var names = ["abc", "cba", "nba"]
// var item1 = names[0]
// var item2 = names[1]
// var item3 = names[2]

// 对数组的解构: [] 也是使用中括号
var [item1, item2, item3] = names
console.log(item1, item2, item3) //abc cba nba

// 解构后面的元素
var [, , itemz] = names
console.log(itemz)//nba

// 解构出一个元素,后面的元素放到一个新数组中
var [itemx, ...newNames] = names
console.log(itemx, newNames) //abc [ 'cba', 'nba' ]

// 解构的默认值
var [itema, itemb, itemc, itemd = "aaa"] = names
console.log(itemd) //aaa

```

对象解构

```js
var obj = {
  name: "why",
  age: 18,
  height: 1.88
}

// 对象的解构: {}
var { name, age, height } = obj
console.log(name, age, height) //why 18 1.88

var { age } = obj //按照key的进行匹配
console.log(age) //18

//修改解构出来的名字 name:newName
var { name: newName } = obj
console.log(newName) //why

//修改结构出来的地址值  赋予默认值
var { address: newAddress = "广州市" } = obj
console.log(newAddress) //广州市


function foo(info) {
  console.log(info.name, info.age)
}

foo(obj)

//直接结构 不需要拿到之后在.name .age进行调用了
function bar({name, age}) {
  console.log(name, age)
}

bar(obj)
```

###### let/const的基本使用

在ES5中我们声明变量都是使用的var关键字，从ES6开始新增了两个关键字可以声明变量：let、const 

let、const在其他编程语言中都是有的，所以也并不是新鲜的关键字； 

但是let、const确确实实给JavaScript带来一些不一样的东西； 

 let关键字： 

从直观的角度来说，let和var是没有太大的区别的，都是用于声明一个变量 

 const关键字： 

const关键字是constant的单词的缩写，表示常量、衡量的意思； 

**它表示保存的数据一旦被赋值，就不能被修改；** 

但是如果赋值的是引用类型，那么可以通过引用找到对应的对象，修改对象的内容； 

 **注意：另外let、const不允许重复声明变量；**

```js
// var foo = "foo"
// let bar = "bar"

 const constant(常量/衡量)
 const name = "abc"
// name = "cba"   //const定完值之后 值不允许修改

// 注意事项一: const本质上是传递的值不可以修改
// 但是如果传递的是一个引用类型(内存地址), 可以通过引用找到对应的对象, 去修改对象内部的属性, 这个是可以的
 
const obj = {
  foo: "foo"
}

// obj = {}  //这样修改会报错
obj.foo = "aaa"
console.log(obj.foo) //aaa

// 注意事项二: 通过let/const定义的变量名是不可以重复定义

 var foo = "abc"
 var foo = "cba"  //可以
 
 
 let foo = "abc"
 // 报错  SyntaxError: Identifier 'foo' has already been declared
 let foo = "cba"

// console.log(foo)

```

###### let/const的作用域提升

let、const和var的另一个重要区别是作用域提升： 

我们知道var声明的变量是会进行作用域提升的； 

但是如果我们使用let声明的变量，在声明之前访问会报错；

```js

console.log(foo) //可以访问到 undefined
 var foo = "foo"

// Reference(引用)Error: Cannot access 'foo' before initialization(初始化)
// let/const他们是没有作用域提升
// foo被创建出来了, 但是不能被访问
// 作用域提升: 能提前被访问
console.log(foo)
let foo = "foo"

```

那么是不是意味着foo变量只有在代码执行阶段才会创建的呢？ 

事实上并不是这样的，我们可以看一下ECMA262对let和const的描述； 

这些变量会被创建在包含他们的词法环境被实例化时，但是是不可以访问它们的，直到词法绑定被求值；

![image-20211008135325936](./media/ECMA对letconst描述.png)

###### let/const有没有作用域提升呢?

 **从上面我们可以看出，在执行上下文的词法环境创建出来的时候，变量事实上已经被创建了，只是这个变量是不能被访问的。** 

那么变量已经有了，但是不能被访问，是不是一种作用域的提升呢？ 

事实上维基百科并没有对作用域提升有严格的概念解释，那么我们自己从字面量上理解； 

**作用域提升：在声明变量的作用域中，如果这个变量可以在声明之前被访问，那么我们可以称之为作用域提升；** 

在这里，它虽然被创建出来了，但是不能被访问，我认为不能称之为作用域提升； 

 所以我的观点是**let、const没有进行作用域提升，但是会在执行上下文创建阶段被创建出来**

###### let/const/window 之间的关系

我们知道，在全局通过var来声明一个变量，事实上会在window上添加一个属性： 

**但是let、const是不会给window上添加任何属性的。** 

那么我们可能会想这个变量是保存在哪里呢？

回顾一下最新的ECMA标准中对执行上下文的描述

上面的描述是老版的描述 下面的是新版的描述 新版  变量环境叫做VE了

![image-20211008140646870](./media/执行上下文描述.png)

 新老版本不同的原因是 老版本var直接给了Go全局对象 而新版本 有了let/const 不能直接赋值给新的对象了

let/const定义的变量现在已经不开始存在window里面了  和var定义的变量 存储 不在同一个地方了

比如v8中其实是通过VariableMap的一个hashmap来实现它们的存储的。 保存的东西variables_：VariableMap【hashMap类型的】

那么window对象呢？而window对象是早期的GO对象，在最新的实现中其实是浏览器添加的全局对象，并且 一直保持了window和var之间值的相等性；但是不是同一个对象了【换句话说就是window给包含V8的浏览器实现了】

###### ES5作用域的理解

```js

// 声明对象的字面量
// var obj = {
//   name: "why"
// }

// ES5中没有块级作用域
// 块代码(block code)

 {
   // 声明一个变量
   var foo = "foo"
 }

// console.log(foo)

// 在ES5中只有两个东西会形成作用域
// 1.全局作用域
// 2.函数作用域

// function foo() {
//   var bar = "bar"
// }

// console.log(bar)

//三个作用域 GO fooAO demoAO 
function foo() {
  function demo() {  
  }
}
```

###### ES6块级作用域

```js
// ES6的代码块级作用域
// 对let/const/function/class声明的类型是有效
{
  let foo = "why"
  function demo() {
    console.log("demo function")
  }
  class Person {}
}

// console.log(foo) // foo is not defined

// 不同的浏览器有不同实现的(大部分浏览器为了兼容以前的代码, 让function是没有块级作用域)
// demo()  // demo function

var p = new Person() // Person is not defined
```

但是我们会发现函数拥有块级作用域，但是外面依然是可以访问的： 

**这是因为引擎【浏览器】会对函数的声明进行特殊的处理【兼容以前的代码】，允许像var那样进行提升；  如果是一个只支持ES6的浏览器 demo是无法找到的**

###### 块级作用域

if语句  switch语句 for循环语句

```js
{

}

// if语句的代码就是块级作用域
 if (true) {
   var foo = "foo" //可以访问
   let bar = "bar" //不可以访问
 }

 console.log(foo) //foo
 console.log(bar) //bar

// switch语句的代码也是块级作用域
 var color = "red"

 switch (color) {
   case "red":
    var foo = "foo"
    let bar = "bar"
 }

 console.log(foo)  //foo
 console.log(bar)  //bar

// for语句的代码也是块级作用域
for (var i = 0; i < 10; i++) {
   console.log("Hello World" + i)
 }

 console.log(i) //可以访问到

for (let i = 0; i < 10; i++) {
     console.log("Hello World" + i)
}

console.log(i) //不可以访问到 i not defined
```

###### 块级作用域的应用场景

四个按钮依次点击

```js
const btns = document.getElementsByTagName('button')

for (var i = 0; i < btns.length; i++) {

//【闭包】 如果不加上立即执行函数 那么直接去上一层寻找作用域 for var 定义没有作用域 所以直接去全局寻找作用域了 显示 第4个按钮被点击四次
  (function(n) {
    btns[i].onclick = function() {
      console.log("第" + n + "个按钮被点击")
    }
  })(i)
  //将i传入进去 第一次是0 第二次是1 上层作用域里面的 n 被访问到
}
// console.log(i)


for (let i = 0; i < btns.length; i++) {
  btns[i].onclick = function() {
    console.log("第" + i + "个按钮被点击")
  }
}
// console.log(i)
```

###### 块级作用域的补充

```js
const names = ["abc", "cba", "nba"]

// 不可以使用const   报错
  for (let i = 0; i < names.length; i++) {
   console.log(names[i])
  }

/* 
因为for循环有自己的块级作用域 每一次 i++ 就会变成let i+1
*/
/*
{
  let i = 0
  console.log(names[i])
}

{
  let i = 1
  console.log(names[i])
}

{
  let i = 2
  console.log(names[i])
} */

// for...of: ES6新增的遍历数组(可迭代对象) 可以用const 因为没有 ++ 操作 是直接赋值的操作
 for (const item of names) {
   console.log(item)  //abc cba nba
 }

// {
//   const item = "abc"
//   console.log(item)
// }

// {
//   const item = "cba"
//   console.log(item)
// }

// console.log(item)

```

###### let_const暂时性死区

在ES6中，我们还有一个概念称之为暂时性死区： 

它表达的意思是在一个代码中，使用let、const声明的变量，在声明之前，变量都是不可以访问的； 

我们将这种现象称之为 temporal dead zone（暂时性死区，TDZ）；

```js
var foo = "foo"

 if (true) {
   console.log(foo)   //可以打印foo
 }


 if (true) {
   console.log(foo)   //Cannot access 'foo' before initialization（初始化）

 let foo = 'foo'
 }
```

###### let_const_var 的选择

 那么在开发中，我们到底应该选择使用哪一种方式来定义我们的变量呢？ 

 对于var的使用： 

 我们需要明白一个事实，**var所表现出来的特殊性：比如作用域提升、window全局对象、没有块级作用域等都是一些 历史遗留问题**； 

 其实是JavaScript在设计之初的一种语言缺陷； 

 当然目前市场上也在利用这种缺陷出一系列的面试题，来考察大家对JavaScript语言本身以及底层的   理解； 

 但是在实际工作中，我们可以使用最新的规范来编写，也就是不再使用var来定义变量了； 

对于let、const：

对于let和const来说，是目前开发中推荐使用的； 

 我们会优先推荐使用const，这样可以保证数据的安全性不会被随意的篡改； 

 只有当我们明确知道一个变量后续会需要被重新赋值时，这个时候再使用let； 

 这种在很多其他语言里面也都是一种约定俗成的规范，尽量我们也遵守这种规范；

#####  模板字符串

```js
// ES6之前拼接字符串和其他标识符
const name = "why"
const age = 18
const height = 1.88

// console.log("my name is " + name + ", age is " + age + ", height is " + height)

// ES6提供模板字符串 ``
const message = `my name is ${name}, age is ${age}, height is ${height}`
console.log(message)

//可以在解析字符串里进行运算
const info = `age double is ${age * 2}`
console.log(info) //36

function doubleAge() {
  return age * 2
}

 //也可以执行函数
const info2 = `double age is ${doubleAge()}`
console.log(info2)

```

###### 标签模板字符串

特别地点 特别使用  React中使用

果我们使用标签模板字符串，并且在调用的时候插入其他的变量：  模板字符串被拆分了； 

第一个元素是数组，是被模块字符串拆分的字符串组合； 

 后面的元素是一个个模块字符串传入的内容

```js
// 第一个参数依然是模块字符串中整个字符串, 只是被切成多块,放到了一个数组中
// 第二个参数是模块字符串中, 第一个 ${}
function foo(m, n, x) {
  console.log(m, n, x, '---------')
}

// foo("Hello", "World")

// 另外调用函数的方式: 标签模块字符串
 //foo``  //[ '' ] undefined undefined ---------

// foo`Hello World`
const name = "why"
const age = 18

// ['Hello', 'Wo', 'rld']
foo`Hello${name}Wo${age}rld`  //[ 'Hello', 'Wo', 'rld' ] why 18 ---------
 /* 
 将原来的模板字符串切成三份 组成一个数组 占用第一个m
 第二个是name 
 都三个是age
 */
```

##### 函数的默认参数

```js
// ES5以及之前给参数默认值
/**
 * 缺点:
 *  1.写起来很麻烦, 并且代码的阅读性是比较差
 *  2.这种写法是有bug
 */
// function foo(m, n) {
//   m = m || "aaa"
//   n = n || "bbb"

//   console.log(m, n)
// }

// 1.ES6可以给函数参数提供默认值
function foo(m = "aaa", n = "bbb") {
  console.log(m, n)
}

// foo()
foo(0, "")

// 2.对象参数和默认值以及解构 直接对原来的代码进行解构
function printInfo({name, age} = {name: "why", age: 18}) {
  console.log(name, age)
}

printInfo({name: "kobe", age: 40})

// 另外一种写法
//相当于给一个空对象作为默认值 然后解构出来给的默认值
function printInfo1({name = "why", age = 18} = {}) {
  console.log(name, age)
}

printInfo1()

// 3.有默认值的形参最好放到最后
function bar(x, y, z = 30) {
  console.log(x, y, z)
}

// bar(10, 20)
bar(undefined, 10, 20)

// 4.有默认值的函数的length属性
// 另外默认值会改变函数的length的个数，默认值以及后面的参数都不计算在length之内了。
function baz(x, y, z, m, n = 30) {
  console.log(x, y, z, m, n)
}

console.log(baz.length) //4 有默认值的参数 不算是length之中 并且后面所有的参数都不算length
```

##### 函数的剩余参数

 ES6中引用了rest parameter，可以将不定数量的参数放入到一个数组中： 

**如果最后一个参数是 ... 为前缀的，那么它会将剩余的参数放到该参数中，并且作为一个数组；** 

```js
function foo(m,n,...args) {
    console.log(m,n);
    console.log(args);
}
```

######  **那么剩余参数和arguments有什么区别呢？**

1.剩余参数只包含那些没有对应形参的实参，而 arguments 对象包含了传给函数的所有实参； 

2.arguments对象不是一个真正的数组，而rest参数是一个真正的数组，可以进行数组的所有操作； 

3.arguments是早期的ECMAScript中为了方便去获取所有的参数提供的一个数据结构，而rest参数是ES6中提供 并且希望以此来替代arguments的； 

**剩余参数必须放到最后一个位置，否则会报错**

##### ES6箭头函数的补充

箭头函数是没有显式原型的，所以不能作为构造函数，使用new来创建对象；

```js

//普通函数具备的特性
/* function foo() {

}
console.log(foo.prototype)、
const f = new foo()

f.__proto__ = foo.prototype 
*/

// 箭头函数不能通过new去调用
var bar = () => {   //bar is not a constructor
  console.log(this, arguments)
}

console.log(bar.prototype) //undefined

// bar is not a constructor
const b = new bar()


```

##### 展开语法

 展开语法(Spread syntax)： 

可以在函数调用/数组构造时，将数组表达式或者string在语法层面展开； 

还可以在构造字面量对象时, 将对象表达式按key-value的方式展开； 

 **展开语法的场景：** 

在函数调用时使用； 

在数组构造时使用； 

在构建对象字面量时，也可以使用展开运算符，这个是在ES2018（ES9）中添加的新特性； 

```js
const names = ["abc", "cba", "nba"]
const name = "why"
const info = {name: "why", age: 18}

// 1.函数调用时
function foo(x, y, z) {
  console.log(x, y, z)
}

// foo.apply(null, names) //也可以将names里面的值赋值给x，y，z
foo(...names) //abc cba nba
foo(...name)  // w h y

// 2.构造数组时
const newNames = [...names, ...name]
console.log(newNames) //[ 'abc', 'cba', 'nba', 'w', 'h', 'y' ]

// 3.构建对象字面量时ES2018(ES9)
const obj = { ...info, address: "广州市"} //{ name: 'why', age: 18, address: '广州市' }
console.log(obj)

const obj1 = {...info,address:"北京市",...names}
console.log(obj1);
/* 
{
  '0': 'abc',
  '1': 'cba',
  '2': 'nba',
  name: 'why',
  age: 18,
  address: '广州市'
}
*/
```

###### 展开运算符_浅拷贝

 **注意：展开运算符其实是一种浅拷贝；** Object.assgin（）

```js

//展开运算符其实是一个浅拷贝
const info = {
  name: "why",
  friend: { name: "kobe" }
}

const obj = { ...info, name: "coderwhy" }
 //console.log(obj) //{ name: 'coderwhy', friend: { name: 'kobe' } }

 //改了obj.friend.name改变了 连着info也改了 所以是浅拷贝
 obj.friend.name = "james"
console.log(info.friend.name) //james
```

浅拷贝：就是将对象浅层对象拷贝出来 不能将对象套着的对象引用值套出来

深拷贝：就是将对象深层拷贝 如果对象里面在套着一层对象也拷贝出来

##### ES6表示数值的方法

```js
const num1 = 100 // 十进制

// b -> binary
const num2 = 0b100 // 二进制
// o -> octonary
const num3 = 0o100 // 八进制
// x -> hexadecimal
const num4 = 0x100 // 十六进制

console.log(num1, num2, num3, num4) //100 4 64 256

// 大的数值的连接符(ES2021 ES12) 方便阅读而已
const num = 10_000_000_000_000_000 
console.log(num) //10000000000000000
```

##### Symbol

Symbol是什么呢？Symbol是ES6中新增的一个基本数据类型，翻译为符号。 

 那么为什么需要Symbol呢？ 

**1.在ES6之前，对象的属性名都是字符串形式，那么很容易造成属性名的冲突；** 

```js
// 1.ES6之前, 对象的属性名(key)

 var obj = {
   name: "why",
   friend: { name: "kobe" },
   age: 18
 }

 /* 为了防止后面的名字覆盖前面的 就写个newName */
  obj['newName'] = "james"
 console.log(obj)
```

 比如原来有一个对象，我们希望在其中添加一个新的属性和值，但是我们在不确定它原来内部有什么内容的情况下， 很容易造成冲突，从而覆盖掉它内部的某个属性； 

比如我们前面在讲apply、call、bind实现时，我们有给其中添加一个fn属性，那么如果它内部原来已经有了fn属性了 呢？  比如开发中我们使用混入，那么混入中出现了同名的属性，必然有一个会被覆盖掉； 

**2.Symbol就是为了解决上面的问题，用来生成一个独一无二的值。** 

```js
// 2.ES6中Symbol的基本使用
const s1 = Symbol()
const s2 = Symbol()

console.log(s1 === s2)  //false 生成的都是独一无二的值
```

 Symbol值是通过Symbol函数来生成的，生成后可以作为属性名； 

也就是在ES6中，对象的属性名可以使用字符串，也可以使用Symbol值； 

Symbol即使多次创建值，它们也是不同的：Symbol函数执行后每次创建出来的值都是独一无二的； 

 **3.我们也可以在创建Symbol值的时候传入一个描述description：这个是（ES10）新增的特性；**

```js
// ES2019(ES10)中, Symbol还有一个描述(description)
const s3 = Symbol("aaa")
console.log(s3.description) //aaa
```

**4.Symbol作为key**

```js
// 4.1.在定义对象字面量时使用
const obj = {
  [s1]: "abc",
  [s2]: "cba"
}

// 4.2.新增属性
obj[s3] = "nba"

// 4.3.Object.defineProperty方式 新增/定义
const s4 = Symbol()

Object.defineProperty(obj, s4, {
  enumerable: true,
  configurable: true,
  writable: true,
  value: "mba"
})

console.log(obj[s1], obj[s2], obj[s3], obj[s4])  //abc cba nba mba
// 注意: 不能通过 .语法获取
// console.log(obj.s1)

```

**5.使用Symbol作为key的属性名,在遍历 /   Object.keys等中是获取不到这些Symbol值  需要Object.getOwnPropertySymbols来获取所有Symbol的key**

我们可以使用Symbol.for方法来做到这一点；

并且我们可以通过Symbol.keyFor方法来获取对应的key；

```js
console.log(Object.keys(obj)) //[]
console.log(Object.getOwnPropertyNames(obj)) //[]
console.log(Object.getOwnPropertySymbols(obj)) //[ Symbol(), Symbol(), Symbol(aaa), Symbol() ]
const sKeys = Object.getOwnPropertySymbols(obj)
for (const sKey of sKeys) {
  console.log(obj[sKey])  //遍历 abc cba nba mba
}

// 5.Symbol.for(key)/Symbol.keyFor(symbol)  创建两个symbol是一样的
const sa = Symbol.for("aaa")
const sb = Symbol.for("aaa")
console.log(sa === sb) //这种情况下创建的symbol值是一样的 true


const key = Symbol.keyFor(sa)  //解除被symbol包括的key
console.log(key) //aaa
const sc = Symbol.for(key)
console.log(sa === sc) //true

```

#### Set

在ES6之前，我们存储数据的结构主要有两种：数组、对象

在ES6中新增了另外两种数据结构：Set、Map，以及它们的另外形式WeakSet、WeakMap。 

 **Set是一个新增的数据结构，可以用来保存数据，类似于数组，但是和数组的区别是元素不能重复**。 

创建Set我们需要通过Set构造函数（暂时没有字面量创建的方式）： 

我们可以发现Set中存放的元素是不会重复的，那么Set有一个非常常用的功能就是给数组去重。

**Set常见的属性：** 

**size：返回Set中元素的个数；** 

 **Set常用的方法：** 

**add(value)：添加某个元素，返回Set对象本身；** 

**delete(value)：从set中删除和这个值相等的元素，返回boolean类型；** 

**has(value)：判断set中是否存在某个元素，返回boolean类型；** 

**clear()：清空set中所有的元素，没有返回值；** 

**forEach(callback, [, thisArg])：通过forEach遍历set；** 

 **另外Set是支持for of的遍历的。**

```js
// 10, 20, 40, 333
// 1.创建Set结构
const set = new Set()
set.add(10)
set.add(20)
set.add(40)
set.add(333)

// console.log(set); //Set { 10, 20, 40, 333 }

//2.添加对象的时候 需要特别注意
set.add({});
set.add({});
console.log(set); //Set { 10, 20, 40, 333, {}, {} } 两个对象 不同的地址 

const obj = {};
set.add(obj);
set.add(obj);
console.log(set);  //Set { 10, 20, 40, 333, {}, {}, {} } 虽然加了两遍 但是 也只是加了一个对象 
//因为obj指向同一个地址值

//3.对数组进行去重(去除重复元素)
const arr = [33, 10, 26, 30, 33, 26]
const arrSet = new Set(arr)
 const newArr = Array.from(arrSet)
 //const newArr = [...arrSet]
console.log(newArr); //[ 33, 10, 26, 30 ]

//4.size属性
console.log(arrSet.size); //4

//5.delete 删除
arrSet.delete(33)
console.log(arrSet); //Set { 10, 26, 30 }

//6.has
console.log(arrSet.has(30));  //true
console.log(arrSet.has(250)); //false

//7.clear
//arrSet.clear()
console.log(arrSet); //Set {}

//8.对set进行遍历
arrSet.forEach(item => {
    console.log(item)  //10 26 30
  })
  
  for (const item of arrSet) {
    console.log(item)  //10 26 30
  }
```

#### WeakSet

那么和Set有什么区别呢？ 

 区别一：WeakSet中只能存放对象类型，不能存放基本数据类型； 

 区别二：WeakSet对元素的引用是弱引用，如果没有其他引用对某个对象进行引用，那么GC可以对该对象进行回收；

```js
const weakSet = new WeakSet()

// 1.区别一: 只能存放对象类型
// TypeError: Invalid value used in weak set
// weakSet.add(10)

// 强引用和弱引用的概念(看图)

// 2.区别二: 对对象是一个弱引用
let obj = { 
  name: "why"
}

// weakSet.add(obj)

const set = new Set()
// 建立的是强引用
set.add(obj)

// 建立的是弱引用
weakSet.add(obj)
```

**WeakSet常见的方法**： 

 **add(value)：添加某个元素，返回WeakSet对象本身；** 

 **delete(value)：从WeakSet中删除和这个值相等的元素，返回boolean类型；** 

 **has(value)：判断WeakSet中是否存在某个元素，返回boolean类型；**

**注意：WeakSet不能遍历**  【所以i只能往里面添加 不能取出 开发中很少使用】

**因为WeakSet只是对对象的弱引用，如果我们遍历获取到其中的元素，那么有可能造成对象不能正常的销毁。** 

**所以存储到WeakSet中的对象是没办法获取的；**

```js
//weakSet的应用场景

/* 
根据weakSet的弱引用来判断 实例对象不是构造出来的不让执行running
*/

/* 
因为是弱引用 所以 当以后p=null的时候 就给删除了  
要是set强引用的话 如果想要销毁对象的话 必须执行p=null 然偶personSet.delete(p)比较麻烦
*/
const personSet = new WeakSet()

class Person {
    constructor(){
        personSet.add(this)
    }

    running(){
        if (!personSet.has(this)) {
            throw new Error("不能通过非构造方法创建出来的对象调用running方法")
          }
          console.log("running~", this)
    }
}

let p = new Person()
p.running();

//p = null 
p.running.call({name: "why"}) //不能通过非构造方法创建出来的对象调用running方法
```

#### Map

**另外一个新增的数据结构是Map，用于存储映射关系**。 

但是我们可能会想，在之前我们可以使用对象来存储映射关系，他们有什么区别呢？ 

**事实上我们对象存储映射关系只能用字符串（ES6新增了Symbol）作为属性名（key）；** 

```js
// 1.JavaScript中对象中是不能使用对象来作为key的
const obj1 = { name: "why" }
const obj2 = { name: "kobe" }

const info = {
  [obj1]: "aaa",
  [obj2]: "bbb"
}

console.log(info) //{ '[object Object]': 'bbb' } 
/* 因为 JavaScript会把key准换成字符串的格式 来进行存储的 
    你将obj1 当成key的话 就会转成 '[object Object]' 这个字符串 
    第二次命名的时候 就会变成重新给key赋值  === bbb
*/
```

**某些情况下我们可能希望通过其他类型作为key，比如对象，这个时候会自动将对象转成字符串来作为key**； 

**那么我们就可以使用Map：**

```js
// 2.Map就是允许我们对象类型来作为key的
// 构造方法的使用
const map = new Map()
map.set(obj1, "aaa")
map.set(obj2, "bbb")
map.set(1, "ccc")
console.log(map) //Map { { name: 'why' } => 'aaa', { name: 'kobe' } => 'bbb', 1 => 'ccc' }
```



**Map常见的属性：** 

**size：返回Map中元素的个数；** 

 **Map常见的方法：** 

**set(key, value)：在Map中添加key、value，并且返回整个Map对象；** 

**get(key)：根据key获取Map中的value；** 

**has(key)：判断是否包括某一个key，返回Boolean类型；** 

**delete(key)：根据key删除一个键值对，返回Boolean类型；** 

**clear()：清空所有的元素；** 

**forEach(callback, [, thisArg])：通过forEach遍历Map；** 

**Map也可以通过for of进行遍历。**



#### WeakMap

**那么和Map有什么区别呢？** 

 **区别一：WeakMap的key只能使用对象，不接受其他的类型作为key；** 

 **区别二：WeakMap的key对 对象想的引用是弱引用，如果没有其他引用引用这个对象，那么GC可以回收该对象；**



 **WeakMap常见的方法有四个：**

 **set(key, value)：在Map中添加key、value，并且返回整个Map对象；** 

 **get(key)：根据key获取Map中的value；** 

 **has(key)：判断是否包括某一个key，返回Boolean类型；** 

 **delete(key)：根据key删除一个键值对，返回Boolean类型；**

```js

const obj = {name: "obj1"}
// 1.WeakMap和Map的区别二:  一个是弱引用 一个是强引用
const map = new Map()
map.set(obj, "aaa")

const weakMap = new WeakMap()
weakMap.set(obj, "aaa")

// 2.区别一: 不能使用基本数据类型
// weakMap.set(1, "ccc")

// 3.常见方法
// get方法
console.log(weakMap.get(obj)) //aaa

// has方法
console.log(weakMap.has(obj)) //true

// delete方法
console.log(weakMap.delete(obj))  //true 返回一个Bolean值

console.log(weakMap)  //WeakMap { <items unknown> }
/* 
打印这个WeakMap { <items unknown> } 原因就是weakMap 本身不能
遍历 因为打印出来需要遍历一下 然后在拼接 显示在页面之上
*/

```

**weakmap在响应式原理中的使用**

```js
// 应用场景(vue3响应式原理)
const obj1 = {
    name: "why",
    age: 18
  }
  
  function obj1NameFn1() {
    console.log("obj1NameFn1被执行")
  }
  
  function obj1NameFn2() {
    console.log("obj1NameFn2被执行")
  }
  
  function obj1AgeFn1() {
    console.log("obj1AgeFn1")
  }
  
  function obj1AgeFn2() {
    console.log("obj1AgeFn2")
  }
  
  const obj2 = {
    name: "kobe",
    height: 1.88,
    address: "广州市"
  }
  
  function obj2NameFn1() {
    console.log("obj1NameFn1被执行")
  }
  
  function obj2NameFn2() {
    console.log("obj1NameFn2被执行")
  }

  /* 
  响应式原理中的数据保存过程
  */
  
  // 1.创建WeakMap 
  /* 
  因为用的是weakmap 所以 以后在销毁 obj对象的时候 obj1=null
  是弱引用的  不会占用对象
  */
  const weakMap = new WeakMap();
  

  // 2.收集依赖结构
  // 2.1.对obj1收集的数据结构
 const mapObj1= new Map()
 mapObj1.set('name',[obj1NameFn1,obj1NameFn2])
 mapObj1.set('age',[obj1AgeFn1,obj1AgeFn2])
 weakMap.set('obj',mapObj1)
  
  // 2.2.对obj2收集的数据结构
  const mapObj2 = new Map()
  mapObj2.set('name',[obj2NameFn1,obj2NameFn2])
  weakMap.set(obj2,mapObj2)

  // 3.如果obj1.name发生了改变
  // Proxy/Object.defineProperty
  obj1.name = "james"
  const targetMap = weakMap.get(obj1) //拿到obj对应的 map
  const fns = targetMap.get("name") //拿到里面函数
  fns.forEach(item => item())
  
```

### **ES7**（ES2016）

#### **Array Includes**

在ES7之前，如果我们想判断一个数组中是否包含某个元素，需要通过 indexOf 获取结果，并且判断是否为 -1。 

 在ES7中，我们可以通过includes来判断一个数组中是否包含一个指定的元素，根据情况，如果包含则返回 true， 否则返回false。

```js
const names = ["abc", "cba", "nba", "mba", NaN]

if (names.indexOf('abc')!== -1) {  //没查询到返回-1
    console.log('包含abc元素'); //包含abc元素
}
 
//ES6 2015年
//ES7 2016年

/* 
可以传入第二个参数 判断从第几个位置开始包含这个元素
*/
if (names.includes('cba',2)) {
    console.log("包含cba元素") //包含cba元素
}

/* 
用includes可以判断是否包含NAN
*/

if (names.indexOf(NaN) !== -1) {
    console.log('indexOf',"包含NaN")
}
  
  if (names.includes(NaN)) {
    console.log('includes',"包含NaN")
}
  
```

#### 指数乘方运算符

 在ES7之前，计算数字的乘方需要通过 Math.pow 方法来完成。 

 在ES7中，增加了 ** 运算符，可以对数字来计算乘方。

```js
const result1 = Math.pow(3, 3)
//ES7
const result2 = 3 ** 3;
console.log(result1, result2);//27 27
```

### ES8 （ES2017）

#### Object.values

之前我们可以通过Object.keys 获取一个对象所有的key，在ES8中提供了Object.values 来获取所有的value值：

```js
const obj ={
    name:'wyd',
    age:'19'
}
console.log(Object.keys(obj)); //[ 'name', 'age' ]
console.log(Object.values(obj));  //[ 'wyd', '19' ]


// 用的非常少
console.log(Object.values(["abc", "cba", "nba"]))  //[ 'abc', 'cba', 'nba' ]
console.log(Object.values("abc")) //[ 'a', 'b', 'c' ]
```

#### Object.entries

通过Object.entries 可以获取到一个数组，数组中会存放可枚举属性的键值对数组

#### String Padding

某些字符串我们需要对其进行前后的填充，来实现某种格式化效果，ES8中增加了padStart 和padEnd 方法，分别是对字符串的首尾进行填充的。

pad单词英文意思（填充 动词含义）

```js
 //padStart 和 padEnd
 const message ='Hello World'


 const newMessage1 =message.padStart(15,'*').padEnd(20,'////') 
 //填充长度至15的长度   填充长度至20的长度 在后面 如果padEnd 长度小于前面的长度
 //就按照前面长度来


 const newMessage2 =message.padStart(15,'*').padEnd(5,'////') 

 console.log(newMessage1);  //****Hello World/////
 console.log(newMessage2); //****Hello World


//关于Padd的实用信息 手机号的隐藏   \
//也可以用正则判断前面的信息 进行replace的替换
const cardNumber = "13684644489"
const lastFourCard = cardNumber.slice(-4) //拿到后四位数字
const finalCard = lastFourCard.padStart(cardNumber.length, "*")//填充到电话号长度
console.log(finalCard) //*******4489 
```

#### Trailing Commas(结尾逗号)

在ES8中，我们允许在函数定义和调用时多加一个逗号：  **知道就行了**  这个增加来源于其他语言编码中的习惯 列如 以前的java 但是我个人是不加上这个逗号的 

```js
function foo(m, n,) {
console.log(m,n);
}

foo(20, 30,) //20 30

```

### ES10(ES2019)

#### flat flatMap

*flat扁平的 扁平的意思* 降维            

flat() 方法会按照一个可指定的深度递归遍历数组，并将所有元素与遍历到的子数组中的元素合并为一个新数组返回。       

**flat**

```js
//1.flat的使用
const nums = [10,20,[2,9],[[30,40],[10,45]],78,[66,88]]
const newNums = nums.flat()
console.log(newNums); //[ 10, 20, 2, 9, [ 30, 40 ], [ 10, 45 ], 78, 66, 88 ]

const newNums2 =nums.flat(2)
console.log(newNums2);
/* 
[
  10, 20,  2,  9, 30,
  40, 10, 45, 78, 66,
  88
]
*/
```

**flatMap**

**flatMap() 方法首先使用映射函数映射每个元素，然后将结果压缩成一个新数组。**
**注意一：flatMap是先进行map操作，再做flat的操作；**
**注意二：flatMap中的flat相当于深度为1；**

```js

// 2.flatMap的使用
const nums2 = [10, 20, 30]
const newNums3 = nums2.flatMap(item => {
    return item * 2 //对里面行进映射
}) //可以绑定第二个参数 在回调之后 可以绑定一个this 
console.log(newNums3); //[ 20, 40, 60 ]

const newNums4 = nums2.map(item => {
  return item * 2
})
console.log(newNums4); //[ 20, 40, 60 ]


//3.flatMap的应用场景
const messages = ["Hello World", "hello lyh", "my name is coderwhy"]

const words = messages.flatMap(item=>{  //flatMap 已经进行降维了
    return item.split(" ")
})

console.log(words);

/* 
[
  'Hello', 'World',
  'hello', 'lyh',
  'my',    'name',
  'is',    'coderwhy'
]
*/

//如果使用Map的话

const words1 = messages.map(item=>{  //flatMap 已经进行降维了
    return item.split(" ")
})

console.log(words1);

/* 、
还是需要降维解决操作的
[
  [ 'Hello', 'World' ],
  [ 'hello', 'lyh' ],
  [ 'my', 'name', 'is', 'coderwhy' ]
]
*/
```

#### Object fromEntries

我们可以通过Object.entries 将一个对象转换成entries，那么如果我们有一个entries了，如何将其转换
成对象呢？
ES10提供了Object.formEntries来完成转换：

```js
const obj = {
    name: "why",
    age: 18,
    height: 1.88
}


const entries = Object.entries(obj)
console.log(entries);
//[ [ 'name', 'why' ], [ 'age', 18 ], [ 'height', 1.88 ] ] entries格式
/* 
这种entries格式 如何转换成object格式
*/


const newObj = {}

/* 
弊端 如果用for in 拿到的是索引值 for of 才拿到的每一个元素 entry[0]
*/
for (const entry of entries) {
    newObj[entry[0]] = entry[1]
}
console.log(newObj); //{ name: 'why', age: 18, height: 1.88 }

//console.log({...obj}); //{ name: 'why', age: 18, height: 1.88 }


//1.ES10中新增了Object.fromEntries方法

const newObj1 = Object.fromEntries(entries)
console.log(newObj1); //{ name: 'why', age: 18, height: 1.88 }


// 2.Object.fromEntries的应用场景
const queryString = 'name=why&age=18&height=1.88'
//解析URL路径 URLSearchParams 创建这个类 传入query这个字符串 返回一个对象
const queryParams = new URLSearchParams(queryString)
console.log(queryParams);
//URLSearchParams { 'name' => 'why', 'age' => '18', 'height' => '1.88' }

// for (const param of queryParams) {
//     console.log(param)
//     /* 
//   [ 'name', 'why' ]
//   [ 'age', '18' ]
//   [ 'height', '1.88' ]
//     */
// }

const paramObj = Object.fromEntries(queryParams) 
//将queryString转换成对象
console.log(paramObj)  //{ name: 'why', age: '18', height: '1.88' }
```

#### trimStart和trimEnd的使用

去除一个字符串首尾的空格，我们可以通过trim方法，如果单独去除前面或者后面呢？
ES10中给我们提供了trimStart和trimEnd；

```js
const message = "    Hello World    "

console.log(message.trim()) //Hello World
console.log(message.trimStart())//Hello World    
console.log(message.trimEnd()) //    Hello World
```

### ES11（ES2020）

#### BigInt（最大整数）

在早期的JavaScript中，我们不能正确的表示过大的数字：
大于MAX_SAFE_INTEGER的数值，表示的可能是不正确的。

```js
// ES11之前 max_safe_integer
const maxInt = Number.MAX_SAFE_INTEGER
console.log(maxInt) // 9007199254740991
console.log(maxInt + 1)
console.log(maxInt + 2)
```

那么ES11中，引入了新的数据类型BigInt，用于表示大的整数：
BitInt的表示方法是在数值的后面加上n

#### **Nullish Coalescing Operator**（空值合并运算符）

 ES11，Nullish Coalescing Operator增加了空值合并操作符：


```js
// ES11: 空值合并运算 ??

const foo = undefined
//const bar = foo || "default value" //以前的做法  default value
/* 这种处理加上 ?? 代表了给上一个默认值  可以明确判断是否是一个 undefined 
或者是null的时候  不在受0和空字符串的影响 明确判断 default value*/

 const bar = foo ?? "defualt value"  //现在的写法

console.log(bar)

// ts 是 js 的超级

```

#### Optional Chaining（可选链 很重要）

可选链也是ES11中新增一个特性，

主要作用是让我们的代码在进行null和undefined判断时更加清晰和简洁：

```js
// 可选链也是ES11中新增一个特性，
// 主要作用是让我们的代码在进行null和undefined判断时更加清晰和简洁：
const info = {
    name: "why",
    // friend: {
    //   girlFriend: {
    //     name: "hmm"
    //   }
    // }
  }

  //如果上面的代码块没有了 下面的代码就会报错 
  //当ES11出来之后 就会添加一个可选链 如果上面代码消失了 也是可以正常运行的

  console.log(info.friend.girlFriend.name) //girFriend undefined

  // ES11提供了可选链(Optional Chainling)
console.log(info.friend?.girlFriend?.name) //如果真的是undefined的话 此行代码后面的代码就不执行了

console.log('其他的代码逻辑')   //逻辑正常执行


//ES11之前的操作

 if (info && info.friend && info.friend.girlFriend) {
   console.log(info.friend.girlFriend.name)
 }
//判断里面有没有值 如果有值 在进行下一步操作
```

#### Global This

在之前我们希望获取JavaScript环境的全局对象，不同的环境获取的方式是不一样的
比如在浏览器中可以通过this、window来获取；
比如在Node中我们需要通过global来获取；
那么在ES11中对获取全局对象进行了统一的规范：globalThis

```js
// 获取某一个环境下的全局对象(Global Object)

// 在浏览器下
// console.log(window)
// console.log(this)

// 在node下
// console.log(global)

// ES11  浏览器和node全能拿到 Global object
console.log(globalThis)
```

#### for-in（标准化）

在ES11之前，虽然很多浏览器支持for...in来遍历对象类型，但是并没有被ECMA标准化。
在ES11中，对其进行了标准化，for...in是用于遍历对象的key的：

```js
// for...in 标准化: ECMA
const obj = {
  name: "why",
  age: 18
}

for (const item in obj) {
  console.log(item)
}
```



### ES12（2021）

#### FinalizationRegistry（） 

**FinalizationRegistry（） 提供一个类**

FinalizationRegistry对象可以让你在对象被垃圾回收时请求一个回调。

FinalizationRegistry提供了这样的一种方法，当一个在注册表中注册的对象被回收时，请求在某个时间点上调用一个清理回调。（清理回调有时被称为finalizer ）

你可以通过调用register方法，注册任何你想要清理回调的对象，传入该对象和所含的值

```js
/* 
FinalizationRegistry
FinalizationRegistry对象可以让你在对象被垃圾回收时请求一个回调。
FinalizationRegistry提供了这样的一种方法：
当一个在注册表中注册的对象被回收时，请求在某个时间点上调
用一个清理回调。（清理回调有时被称为finalizer ）;
你可以通过调用register方法，注册任何你想要清理回调的对象，传入该对象和所含的值
*/

//创建一个类 创建的时候 传入一个回调函数
const finalRegistry = new FinalizationRegistry((value)=>{
    console.log("注册在finalRegistry的对象, 某一个被销毁", value)
})

let obj = { name: "why" }
 let info = { age: 18 }

finalRegistry.register(obj, "obj")   //obj是传入的回调函数
//这个类 携带一个 register方法
//注册在finalRegistry的对象, 某一个被销毁 obj

 finalRegistry.register(info, "value") //注册在finalRegistry的对象, 某一个被销毁 value

obj = null 
info = null
```

#### WeakRefs

如果我们默认将一个对象赋值给另外一个引用，那么这个引用是一个强引用： 如果我们希望是一个弱引用的话，可以使用WeakRef；

```js
//ES12 WeakRef类
//WeakRef。prototype。deref
// > 如果原来的对象没有销毁，那么可以获取到原对象
// > 如果原对象已经销毁 ,那么获取到的是  undefined

const finalRegistry = new FinalizationRegistry((value)=>{
  console.log("注册在finalRegistry的对象, 某一个被销毁", value);
})

let obj = {name : 'wyd'}
let info = new WeakRef(obj) //传入对象 并且创建出来的是弱引用

finalRegistry.register(obj,'obj')

obj = null  

// console.log(info.name);// undefined 不能通过info来拿数值 因为WeakRef不让直接拿值


setTimeout(() => {
    console.log(info.deref()); // 可以拿到info这个Ref对象
    console.log(info.deref()?.name) //加上可选链 就不会报错了 
    console.log(info.deref() && info.deref().name)// 防止报错 加上一个判断
}, 10000);
```

#### logical assignment operators

逻辑赋值运算

```js
//1.||= 逻辑或赋值运算

/*  let message = "hello world"
 message = message || "default value"  //老式写法
 message ||= "default value"  //新版本写法
 console.log(message) // default value
 */

//2.&&= 逻辑与赋值运算

// &&
const obj = {
  name: "why",
  foo: function() {
    console.log("foo函数被调用")
    
  }
}
//obj.foo是否存在 如果存在在执行函数
obj.foo && obj.foo() //foo函数被调用

//&&=
let info = {
  name: "wyd"
}

// 1.判断info
// 2.有值的情况下, 取出info.name 且赋值
// info = info && info.name
info &&= info.name
console.log(info) //wyd

//3.??=逻辑空赋值运算

let message = 0
message ??= "default value"
console.log(message) // 0
```

#### replaceAll

将所有的字符替换成输出字符

```js
 const name= 'wwwwyyyydddd'
 console.log(name.replaceAll('w','a')); //aaaayyyydddd
```

### Proxy_Reflect

#### **监听对象的操作一**

我们先来看一个需求：有一个对象，我们希望监听这个对象中的属性被设置或获取的过程
通过我们前面所学的知识，能不能做到这一点呢？
其实是可以的，我们可以通过之前的属性描述符中的存储属性描述符来做到；

下边这段代码就利用了前面讲过的 Object.defineProperty 的存储属性描述符来对属性的操作进行监听。

```js
const obj= {
    name:'wyd',
    age:19
}

/* 
Object.defineProperty(obj,'name',{
    get(){
        console.log('监听到obj对象的name属性被访问了');
    },
    set(){
        console.log('监听到obj对象的name属性被设置值');
    }
})


obj.name ='wz' //监听到obj对象的name属性被设置值
console.log(obj.name);  //监听到obj对象的name属性被访问了 undefined
 */
Object.keys(obj).forEach(key=>{
    let value=obj[key]
    //console.log(key); //name age
     Object.defineProperty(obj,key,{
        get(){
            console.log(`监听到obj对象的${key}属性被访问了`);
            return value
        },
        set(newValue){
            console.log(`监听到obj对象的${key}属性被设置值`);
            value=newValue
        }
     })
})


obj.name ='kobe';  //监听到obj对象的name属性被设置值
obj.age=20;   //监听到obj对象的age属性被设置值

console.log(obj.name);  //kobe
console.log(obj.age);   //20

```

但是这样做有什么缺点呢？
首先，Object.defineProperty设计的初衷，不是为了去监听截止一个对象中所有的属性的。
我们在定义某些属性的时候，初衷其实是定义普通的属性，但是后面我们强行将它变成了数据属性描述符。
其次，如果我们想监听更加丰富的操作，比如新增属性、删除属性，那么Object.defineProperty是无能为力的。
所以我们要知道，存储数据描述符设计的初衷并不是为了去监听一个完整的对象。

#### **监听对象的操作2**

在ES6中，新增了一个Proxy类，这个类从名字就可以看出来，是用于帮助我们创建一个代理的：
也就是说，如果我们希望监听一个对象的相关操作，那么我们可以先创建一个代理对象（Proxy对象）；之后对该对象的所有操作，都通过代理对象来完成，代理对象可以监听我们想要对原对象进行哪些操作；
我们可以将上面的案例用Proxy来实现一次：

```js
const obj = {
    name: "why",
    age: 18
  }

  const objProxy = new Proxy(obj,{
      //获取值时的捕获器下
      //target值得就是上面的代理对象obj
      get(target,key){
        console.log(`监听到对象的${key}属性被访问了`, target);
        return target[key]
      },

      //设置值时的捕获器
      set(target,key,newValue){
        console.log(`监听到对象的${key}属性被设置值`, target)
        target[key] = newValue
      }
  })

//   console.log(objProxy.name); //why
  console.log(objProxy.age);  //18

/* 将设置的值 设置在原来对象的里面 */
objProxy.name = "kobe"
objProxy.age = 30

console.log(objProxy.name); //kobe
console.log(objProxy.age);  //30


console.log(obj.name) //kobe
console.log(obj.age) //obj
```

首先，我们需要new Proxy对象，并且传入需要侦听的对象以及一个处理对象，可以称之为handler；
const p = new Proxy(target, handler)
其次，我们之后的操作都是直接对Proxy的操作，而不是原有的对象，因为我们需要在handler里面进行侦听；

如果我们想要侦听某些具体的操作，那么就可以在handler中添加对应的捕捉器（Trap）：
set和get分别对应的是函数类型；
**set函数有四个参数：**
target：目标对象（侦听的对象）；
property：将被设置的属性key；
value：新属性值；
receiver：调用的代理对象；
**get函数有三个参数：**
target：目标对象（侦听的对象）；
property：被获取的属性key；

receiver：调用的代理对象；

#### Proxy的捕获器

**13个捕获器分别是做什么的呢？**

handler.getPrototypeOf()
Object.getPrototypeOf 方法的捕捉器。

handler.setPrototypeOf()
Object.setPrototypeOf 方法的捕捉器。

handler.isExtensible()
Object.isExtensible 方法的捕捉器。

handler.preventExtensions()
Object.preventExtensions 方法的捕捉器。 //阻止对对象扩展

handler.getOwnPropertyDescriptor()
Object.getOwnPropertyDescriptor 方法的捕捉器。

handler.defineProperty()
Object.defineProperty 方法的捕捉器。

handler.ownKeys()  
Object.getOwnPropertyNames 方法和Object.getOwnPropertySymbols 方法的捕捉器。

handler.has()

in 操作符的捕捉器。

handler.get()
属性读取操作的捕捉器。

handler.set()
属性设置操作的捕捉器。

handler.deleteProperty()
delete 操作符的捕捉器。

**handler.apply()**
**函数调用操作的捕捉器。**

**handler.construct()**
**new 操作符的捕捉器。**

**后两个捕获器用于函数对象的**

#### Proxy对函数对象的监听

```js
function foo() {
    
}
//代理对象
const fooProxy = new Proxy(foo,{
   apply(target,thisArg,argArray){
    console.log("对foo函数进行了apply调用")
    return target.apply(thisArg,argArray)
   },
   construct(target,argArray,newTarget){
       console.log('对foo函数进行了调用');
       return new target(...argArray)
   }
})

fooProxy.apply({},['abc','cba']) //对foo函数进行了apply调用
 new fooProxy('abc','cba')

```

#### Reflect的作用

Reflect也是ES6新增的一个API，它是一个对象，字面的意思是反射。
**那么这个Reflect有什么用呢？**

它主要提供了很多操作JavaScript对象的方法，有点像Object中操作对象的方法；
比如Reflect.getPrototypeOf(target)类似于 Object.getPrototypeOf()；
比如Reflect.defineProperty(target, propertyKey, attributes)类似于Object.defineProperty() ；

**如果我们有Object可以做这些操作，那么为什么还需要有Reflect这样的新增对象呢？**

这是因为在早期的ECMA规范中没有考虑到这种对对象本身的操作如何设计会更加规范，所以将这些API放到Object上面；
但是Object作为一个构造函数，这些操作实际上放到它身上并不合适；
另外还包含一些类似于 in、delete操作符，让JS看起来是会有一些奇怪的；
所以在ES6中新增了Reflect，让我们这些操作都集中到了Reflect对象上；
**那么Object和Reflect对象之间的API关系，可以参考MDN文档：**
https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Reflect/Comparing_Reflect_and_Object_methods

#### Reflect常见方法

Reflect中有哪些常见的方法呢？它和Proxy是一一对应的，也是13个：

Reflect.getPrototypeOf(target)
类似于Object.getPrototypeOf()。

Reflect.setPrototypeOf(target, prototype)
设置对象原型的函数. 返回一个Boolean，如果更新成功，则返回true。

Reflect.isExtensible(target)
类似于Object.isExtensible()

Reflect.preventExtensions(target)
类似于Object.preventExtensions()。返回一个Boolean。

Reflect.getOwnPropertyDescriptor(target, propertyKey)
类似于Object.getOwnPropertyDescriptor()。如果对象中存在该属性，则返回对应的属性描述符, 否则返回undefined.

Reflect.defineProperty(target, propertyKey, attributes)
和Object.defineProperty() 类似。如果设置成功就会返回true

Reflect.ownKeys(target)
返回一个包含所有自身属性（不包含继承属性）的数组。(类似于Object.keys(), 但不会受enumerable影响).

Reflect.has(target, propertyKey)
判断一个对象是否存在某个属性，和 in 运算符 的功能完全相同。

Reflect.get(target, propertyKey[, receiver])
获取对象身上某个属性的值，类似于 target[name]。

Reflect.set(target, propertyKey, value[, receiver])
将值分配给属性的函数。返回一个Boolean，如果更新成功，则返回true。

Reflect.deleteProperty(target, propertyKey)
作为函数的delete操作符，相当于执行 delete target[name]。

Reflect.apply(target, thisArgument, argumentsList)
对一个函数进行调用操作，同时可以传入一个数组作为调用参数。和Function.prototype.apply() 功能类似。

Reflect.construct(target, argumentsList[, newTarget])
对构造函数进行 new 操作，相当于执行 new target(...args)。

```js
/* 将Object上面的对象 放到Reflect上面 让object更加规范 */

const obj = {
    name: "why",
    age: 18
  }

 const objProxy = new Proxy(obj,{
     get(target,key,receiver){
       console.log('get-------');
       //返回原来的对象映射
       return Reflect.get(target,key)
     },
     set(target, key, newValue, receiver){
        console.log('set---------');
        //这种写法设置值的时候 你不知道是否成功 比如对象冻结的时候 就需要检测
        //target[key] = newValue
 
        // reflect 设置值的话 返回一个boolean类型值 
        const result = Reflect.set(target,key,newValue)
        if (result) {
             
        }
     }
 }) 

  objProxy.name= 'kobe'  //set--------

 console.log(objProxy.name);  
 /* 
 get-------
 why
 */
```

#### receiver参数的作用

我们发现在使用getter、setter的时候有一个receiver的参数，它的作用是什么呢？
如果我们的源对象（obj）有setter、getter的访问器属性，那么可以通过receiver来改变里面的this；

```js
const obj = {
    _name:'wyd',
    get name(){
        //this指向obj
        console.log('get方法操作');
     return this._name
    },
    set name(newValue){
        console.log('set方法操作');
       this._name = newValue
    }
}

/* get和set里面的receiver就是代理上面的对象 关于this指向新的代理对象的问题 */

const objProxy = new Proxy(obj,{
    //get和set里面才有receiver
    get(target,key,receiver){
      console.log(target,key,receiver);
      return Reflect.get(target,key,receiver)
    },
    set(target,key,newValue,receiver){
    console.log('set方法被访问------',key,receiver);
    Reflect.set(target,key,newValue,receiver)
    }
})

/* obj.name='kobe' //set方法操作
console.log(obj.name); // get方法操作 kobe */

console.log(objProxy.name);
objProxy.name='gai'
```

#### Reflect中construct的作用

```js
function Student(name, age) {
    this.name = name;
    this.age = age;
}

function Teacher() {

}


const stu = new Student('wyd', '19')
console.log(stu); //Student { name: 'wyd', age: '19' }
console.log(stu.__proto__ === Student.prototype);  //true

//执行Student函数中的内容，但是创建出来的是Teacher对象

// 三个参数 target执行函数 额外的参数放在数组里面  第三个是类型
const teacher = Reflect.construct(Student, ['wyd', 19], Teacher)

console.log(teacher);  //Teacher { name: 'wyd', age: 19 } 
//调用Student里面的知识 然后 指向是teacher类
console.log(teacher.__proto__ === Teacher.prototype); //true
```

## 响应式原理

### 什么是响应式？

我们先来看一下响应式意味着什么？我们来看一段代码：
m有一个初始化的值，有一段代码使用了这个值；
那么在m有一个新的值时，这段代码可以自动重新执行；

```js
let m =20；
console.log(m)
console.log(m*2)

m=40
```

上面的这样一种可以自动响应数据变量的代码机制，我们就称之为是响应式的。
那么我们再来看一下对象的响应式：

![image-20211019120157019](./media/对象响应式原理.png)

### 响应式函数的封装

```js
//封装一个响应式的函数
let reactiveFns = []  //name发生改变所需要执行的函数
//响应式函数里面的内容
function WatchFn(fn){
    reactiveFns.push(fn )
}

//对象的响应式
const obj= {
    name:'wyd',
    age:19
}

//传入匿名函数就行
WatchFn(function() {
    const newName = obj.name
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(obj.name) // 100行
  })

watchFn(function() {
    console.log(obj.name, "demo function -------")
  })

function bar(){
console.log("普通的其他函数")
  console.log("这个函数不需要有任何响应式")
}

//遍历数组中的函数
reactiveFns.forEach(fn=>{
    fn()
})
```

### 依赖收集类的封装

```js
//Depend 依赖的意思 收集依赖
class Depend {
    constructor() {
        this.reactiveFns = []
    }
    //添加·到响应式数组里面
    addDepend(reactiveFn) {
        this.reactiveFns.push(reactiveFn)
    }

    //notify  通知
    notify() {
        this.reactiveFns.forEach(fn => {
            fn()
        })
    }
}

const depend = new Depend()
//调用子类里面的方法 每一个属性对象一个depend对象

function watchFn(fn) {
    depend.addDepend(fn)
}

// 对象的响应式
const obj = {
    name: "wyd", // depend对象
    age: 18 // depend对象
}

watchFn(function () {
    const newName = obj.name
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(obj.name) // 100行
})

watchFn(function () {
    console.log(obj.name, "demo function -------")
})

obj.name = 'wz'

depend.notify()
  /*
你好啊, 李银河
Hello World
wz
wz demo function -------
*/
```

### 自动监听对象的方法

```js
class Depend {
    constructor() {
        this.reactiveFns = []
    }
    addDepend(reactiveFn) {
        this.reactiveFns.push(reactiveFn)
    }
    notify() {
        this.reactiveFns.forEach(fn => {
            fn()
        })
    }
}


const depend = new Depend()

function watchFn(fn) {
    depend.addDepend(fn)
}


//对象的响应式

const obj = {
    name: 'wyd',
    age: 19,
}


// 监听对象的属性变量: Proxy(vue3)/Object.defineProperty(vue2)
const objProxy = new Proxy(obj, {
    get: function(target, key, receiver) {
      return Reflect.get(target, key, receiver)
    },
    set: function(target, key, newValue, receiver) {
      Reflect.set(target, key, newValue, receiver)
      depend.notify()
    }
  })
  


watchFn(function () {
    const newName = objProxy.name
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(objProxy.name) // 100行
})

watchFn(function () {
    console.log(objProxy.name, "demo function -------")
})

watchFn(function () {
    console.log(objProxy.age, "age 发生变化是需要执行的----1")
})

watchFn(function () {
    console.log(objProxy.age, "age 发生变化是需要执行的----2")
})

objProxy.name = "kobe"
objProxy.name = "james"
objProxy.name = "curry"

objProxy.age = 100
//   console.log(1);
```

### 收集依赖的管理

```js
class Depend {
    constructor() {
        this.reactiveFns = []
    }

    addDepend(reactiveFn) {
        this.reactiveFns.push(reactiveFn)
    }

    notify() {
        this.reactiveFns.forEach(fn => {
            fn()
        })
    }
}

// 封装一个响应式函数
const depend = new Depend()
function watchFn(fn) {
    depend.addDepend(fn)
}

//封装一个获取depend的函数
const targetMap = new WeakMap()

function getDepend(target, key) {
    //根据target对象获取map的过程
    let map = targetMap.get(target)
    // 如果第一次没有值 就创建一个map
    if (!map) {
        map = new Map()
        targetMap.set(target, map)
    }

    //根据key获取depend对象
    let depend = map.get(key)
    if (!depend) {
        depend = new Depend()
        map.set(key, depend)
      }

      return depend
}

// 对象的响应式
const obj = {
    name: "why", // depend对象
    age: 18 // depend对象
  }

// 监听对象的属性变量: Proxy(vue3)/Object.defineProperty(vue2)
const objProxy = new Proxy(obj, {
    get: function(target, key, receiver) {
      return Reflect.get(target, key, receiver)
    },
    set: function(target, key, newValue, receiver) {
      Reflect.set(target, key, newValue, receiver)
      const depend = getDepend(target,key)
      depend.notify()
    }
  })

  watchFn(function() {
    const newName = objProxy.name
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(objProxy.name) // 100行
  })
  
  watchFn(function() {
    console.log(objProxy.name, "demo function -------")
  })
  
  watchFn(function() {
    console.log(objProxy.age, "age 发生变化是需要执行的----1")
  })
  
  watchFn(function() {
    console.log(objProxy.age, "age 发生变化是需要执行的----2")
  })
  
  objProxy.name = "kobe"
  objProxy.name = "james"
  objProxy.name = "curry"
  
  objProxy.age = 100
```

### 正确的依赖的管理

```js
class Depend {
    constructor() {
        this.reactiveFns = []
    }
    addDepend(reactiveFn) {
        this.reactiveFns.push(reactiveFn)
    }
    notify() {
        console.log(this.reactiveFns);
        this.reactiveFns.forEach(fn => {
            fn();
        })
    } 

}

// 封装一个响应式的函数
let activeReactiveFn = null
function watchFn(fn) {
    activeReactiveFn = fn
    fn()
    activeReactiveFn = null
}


//封装一个获取depend函数
const targetMap = new WeakMap();
function getDepend(target, key) {
    //根据 target对象获取map
    let map = targetMap.get(target)
    if (!map) {
        map = new Map()
        targetMap.set(target, map)
    }

    //根据key获取depend对象
    let depend = map.get(key)
    if (!depend) {
        depend = new Depend()
        map.set(key, depend)
    }
    return depend
}

// 对象的响应式
const obj = {
    name: "why", // depend对象
    age: 18 // depend对象
}

//监听对象的属性变量 Proxy(vue3)/object.defineProperty(vue2)
const objProxy = new Proxy(obj, {
    get: function (target, key, receiver) {
        //根据target.key获取对象之中的depend
        const depend = getDepend(target, key)
        // 给depend对象中添加响应函数 activeReactiveFn是一个全局的变量
        depend.addDepend(activeReactiveFn)

        return Reflect.get(target, key, receiver)
    },
    set: function (target, key, newValue, receiver) {
     Reflect.set(target, key, newValue, receiver)
     const depend =getDepend(target, key)
     depend.notify()
    }
})
watchFn(function() {
    console.log("-----第一个name函数开始------")
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(objProxy.name) // 100行
    console.log("-----第一个name函数结束------")
  })
  
  watchFn(function() {
    console.log(objProxy.name, "demo function -------")
  })

  objProxy.name='kobe'
```

### 对Depend类的重构

```js
//保存当前需要收集的响应式函数
let activeReactiveFn = null;

/* 
Depend优化:
 1.depend方法 不在proxy/get 里面使用全局变量
 2.使用set来保存依赖函数,而不是数组[]
*/

class Depend {
    constructor() {
        /* 
    以前用数组封装 所以 只是添加进入数组 使用set 不会再次添加 
    使用set是因为保证 一个obj.name只执行一次
    set保证只执行一次
        */
        this.reactiveFns = new Set();
    }

    depend() {
        if (activeReactiveFn) {
            //set 方法使用add方法
            this.reactiveFns.add(activeReactiveFn);
        }
    }

    notify() {
        this.reactiveFns.forEach(fn => {
            fn()
        })
    }

}
//封装一个响应式的函数
function watchFn(fn) {
    activeReactiveFn = fn;
    fn()
    activeReactiveFn = null;
}

//封装一个获取depend函数

const targetMap = new WeakMap();
function getDepend(target, key) {
    //根据target对象获取map的过程
    let map = targetMap.get(target)
    if (!map) {
        map = new Map()
        targetMap.set(target, map)
    }

    //根据key获取depend对象
    let depend = map.get(key)
    if (!depend) {
        depend = new Depend()
        map.set(key, depend)
    }
    return depend
}


// 对象的响应式
const obj = {
    name: "wyd", // depend对象
    age: 18 // depend对象
}


//监听对象的属性变量: Proxy(vue3)/Object.defineProperty(vue2)
const objProxy = new Proxy(obj, {
    get: function (target, key, receiver) {
        //根据target.key获取对应的depend
        const depend = getDepend(target, key);
        // 给depend对象中添加响应函数
        // depend.addDepend(activeReactiveFn)
        //优化代码 不需要对代码进行添加全局变量 只需要调用一个方法就行
        depend.depend()

        return Reflect.get(target,key,receiver)
    },
    set:function(target,key,newValue,receiver){
        Reflect.set(target,key,newValue,receiver)
        const depend= getDepend(target,key)
        depend.notify()
    }
})

watchFn(()=>{
    console.log(objProxy.name, "-------")
  console.log(objProxy.name, "+++++++")
}) 

objProxy.name = "kobe"
```

### vue3响应式原理的实现

```js
//保存当前需要收集的响应式函数
let activeReactiveFn = null;

/**
 * Depend优化:
 *  1> depend方法
 *  2> 使用Set来保存依赖函数, 而不是数组[]
 */

 class Depend {
    constructor() {
      this.reactiveFns = new Set()
    }
  
    // addDepend(reactiveFn) {
    //   this.reactiveFns.add(reactiveFn)
    // }
  
    depend() {
      if (activeReactiveFn) {
        this.reactiveFns.add(activeReactiveFn)
      }
    }
  
    notify() {
      this.reactiveFns.forEach(fn => {
        fn()
      })
    }
  }
  

  //封装一个响应的函数

  function watchFn(fn){
      activeReactiveFn = fn;
      fn();
      activeReactiveFn= null;
  }

  //封装一个获取depend的函数
  const targetMap = new WeakMap();
 function getDepend(target,key){
     //根据target对象获取map的过程
     let map = targetMap.get(target)
     if (!map) {
         map = new Map()
         targetMap.set(target,map);
     }

     //根据key获取depend对象
     let depend = map.get(key)
     if (!depend) {
          depend= new Depend();
          map.set(key,depend);
     }
     return depend;
 }

 //保证自动化响应式的函数

 function reactive(obj){
     return new Proxy(obj,{
       get:function(target,key,receiver){
           //根据target，key获取对象的depend
           const depend =getDepend(target,key)
           //给depend 对象中添加响应函数
           // depend.addDepend(activeReactiveFn)
           depend.depend();
           return Reflect.get(target,key,receiver)
       },
       set:function(target,key,newValue,receiver){
           Reflect.set(target,key,newValue,receiver);
           const depend =getDepend(target,key);
           depend.notify()
       }
     })
 }

 // 监听对象的属性变量: Proxy(vue3)/Object.defineProperty(vue2)
const objProxy = reactive({
    name: "why", // depend对象
    age: 18 // depend对象
  })
  
  const infoProxy = reactive({
    address: "广州市",
    height: 1.88
  })

  watchFn(()=>{
      console.log(infoProxy.address);
  })

  infoProxy.address = "北京市"

  const foo = reactive({
    name: "foo"
  })
  
  watchFn(() => {
    console.log(foo.name)
  })
  
  foo.name = "bar"
   
```

### vue2响应式原理的实现

```js
//保存当前需要收集的响应式函数
let activeReactiveFn = null;

/**
 * Depend优化:
 *  1> depend方法
 *  2> 使用Set来保存依赖函数, 而不是数组[]
 */

class Depend{
    constructor(){
        this.reactiveFns= new Set();
    }

    depend(){
        if (activeReactiveFn) {
            this.reactiveFns.add(activeReactiveFn);
        }
    }

    notify(){
        this.reactiveFns.forEach(fn=>{
            fn();
        })
    }
}

//封装一个响应式函数
function watchFn(fn){
    activeReactiveFn = fn;
    fn();
    activeReactiveFn = null;
}

//封装一个获取depend函数
const targetMap = new WeakMap();
function getDepend(target,key){
    //根据target对象获取map的全过程
    let map = targetMap.get(target);
    if (!map) {
        map= new Map();
        targetMap.set(target,map);
    }
  //根据key来获取依赖depend对象
  let depend = map.get(key)
   if (!depend) {
        depend = new Depend();
        map.set(key,depend);
   }
   return depend;
  
}


function reactive(obj){
    //ES6之前 使用Object.defineProperty
    Object.keys(obj).forEach(key=>{
         let value = obj[key];
         Object.defineProperty(obj,key,{
             get(){
                 const depend = getDepend(obj,key);
                 depend.depend();
                 return value;
             },
             set(newValue){
                 value = newValue;
                 const depend = getDepend(obj,key);
                 depend.notify();
             }
         })
    })
    return obj;
}


// 监听对象的属性变量: Proxy(vue3)/Object.defineProperty(vue2)
const objProxy = reactive({
    name: "why", // depend对象
    age: 18 // depend对象
  })
  
  const infoProxy = reactive({
    address: "广州市",
    height: 1.88
  })
  
  watchFn(() => {
    console.log(infoProxy.address)
  })
  
  infoProxy.address = "北京市"
  
  const foo = reactive({
    name: "foo"
  })
  
  watchFn(() => {
    console.log(foo.name)
  })
  
  foo.name = "bar"
  foo.name = "hhh"
```

## Promise

那么这里我从一个实际的例子来作为切入点：
我们调用一个函数，这个函数中发送网络请求（我们可以用定时器来模拟）；

如果发送网络请求成功了，那么告知调用者发送成功，并且将相关数据返回过去；

如果发送网络请求失败了，那么告知调用者发送失败，并且告知错误信息；

### 异步任务的处理

```js

/* 这种回调函数有很多的弊端
   1.如果是我们自己封装的requestData，那么我们在封装的时候
   必须要设计好自己的callback名称，并且使用好callback 
   2.如果我们使用别人封装的requestData 或者一些第三方的库，
   那么我们必须要去看别人的源码或者文档，才知道这个函数怎么获取到结果【沟通成本过高】
*/

// 在实际开发中 请求函数 和实际发送请求函数分开
//request.js
function  requestData(url,successCallback,failtureCallback) {
    //模拟网络请求
    setTimeout(() => {
        //拿到请求的结果
        //url传入的是wangyaoda 请求成功
        if (url=== 'wangyaoda'){
            //成功
            let successMessage =  '请求成功';
            // return successMessage  
            successCallback(successMessage);  
        }else{
            //失败
            let errorMessage ='请求失败,检查您的网络'
            // return errorMessage
            failtureCallback(errorMessage)
        }
    }, 3000);
}
//main.js

const result = requestData('wangyaoda',(res)=>{
    console.log(res); //请求成功
},(err)=>{
    console.log(err);
})


// const result =requestData('wangyaoda')
// console.log(result);//undefined 
/* 因为上面的操作时异步的 直接return的话 无法拿到requestData返回的值 
   拿到的是 settimeout里面的值 所以返回时undefined 这个时候 就需要
   用 回调函数来拿到当前值
*/


//更规范/更好的方案 Promise承诺(规范好了所有的代码 编写逻辑)
/* 
承诺的意思就是告诉你 返回的结果是成功还是失败 如果成功就是怎样 失败就是怎样的 
*/
function requestData2() {
    return "承诺"
  }
  
  const chengnuo = requestData2()
```

### 什么是Promise呢

在上面的解决方案中，我们确确实实可以解决请求函数得到结果之后，获取到对应的回调，但是它存在两个主要的
问题：
第一，我们需要自己来设计回调函数、回调函数的名称、回调函数的使用等；

第二，对于不同的人、不同的框架设计出来的方案是不同的，那么我们必须耐心去看别人的源码或者文档，以
便可以理解它这个函数到底怎么用；

我们来看一下Promise的API是怎么样的：
Promise是一个类，可以翻译成 承诺、许诺 、期约；
当我们需要给予调用者一个承诺：待会儿我会给你回调数据时，就可以创建一个Promise的对象；
在通过new创建Promise对象时，我们需要传入一个回调函数，我们称之为executor
这个回调函数会被立即执行，并且给传入另外两个回调函数resolve、reject；
**当我们调用resolve回调函数时，会执行Promise对象的then方法传入的回调函数；**
**当我们调用reject回调函数时，会执行Promise对象的catch方法传入的回调函数；**

```js

/* 其实Promise就是一种机制 就是表述了 正确的回调和错误的回调 建立了一些正确的的承诺 */
function foo() {
    //相当于你new Promise的时候 就传入两个回调函数 一个是resolve 一个是reject
    return new Promise((resolve,reject)=>{
        resolve('success message')
        // reject('error message')
    })
}

//main.js 
const fooPromise = foo()

// then方法传入的回调函数两个回调函数:
// > 第一个回调函数, 会在Promise执行resolve函数时, 被回调
// > 第二个回调函数, 会在Promise执行reject函数时, 被回调
fooPromise.then((res)=>{
   console.log(res);
},(err)=>{
    console.log(err);
})

//catch 方法传入回调函数 会在Promise执行reject函数的时候 被回调
fooPromise.catch(()=>{

})



//传入的这个函数 被称之为executor 并且立即执行
/* 
在成功的时候 调用 resolve函数  回调函数
在失败的时候 调用reject函数 回调函数
*/
const promise  = new Promise((resolve,reject)=>{
// console.log("promise传入的函数被执行了")
    resolve()
//   reject()
})
```

### 异步请求Promise

```js
//将第一个列子用Promise来实现

//request.js
function requestData(url) {
     return new Promise((resolve,reject)=>{
         //模拟网络请求
         /* 里面正常写请求 当错误的时候 执行错误的回调 正确的 正确的回调 */
         setTimeout(() => {
             //拿到请求的结果
             //url传入的是 wangyaoda 请求成功
             if (url === 'wangyaoda') {
                 //成功 
                 let success = '这次请求成功'
                 resolve(success)
             }else{
                 let errmessage ="这次请求失败,url错误"
                 reject(errmessage)
             }
         }, 3000);
     })
}


//main.js
 const promise =  requestData('wangzhang1')
// promise.then((res)=>{
//     //res就是上面 resolve回调函数
//     console.log('请求成功',res);
// }).catch((err)=>{
//    console.log('请求失败',err);
// })

//这次请求错误 因为node中没有 对这种非链式调用进行处理
/* 
promise.then((res)=>{
    console.log('请求成功',res);
})
promise.catch((err)=>{
    console.log('请求失败',err);
})
*/


//也可以在then中传入两次回调
promise.then((res)=>{
   console.log('请求成功',res);
},(err)=>{
console.log('请求失败',err); //请求失败 这次请求失败,url错误
})

```

### Promise的三种状态

```js
const promise = new Promise((resolve,reject)=>{

})

promise.then(res=>{

},err=>{

})

/* 上面的等价与下面的 */
//状态一旦确定下来 状态时不可以改变的 但是后面的代码还是可以继续执行的
new Promise ((resolve,reject)=>{
    //pending状态 当前处于待定状态
 console.log('---------');
resolve() //处于fulfilled状态(已敲定/兑现状态)  执行完resolve之后 后面的就不会在执行了
reject()  //处于rejected状态(已拒绝状态)
console.log('+++++++++');
}).then(res=>{
   console.log('res:',res);
},err=>{
   console.log('err',err);
})

```

### Promise的resolve参数

```js
/* 
关于resolve
1.普通的值 或者对象 pending -> fulfilled
2.传入一个 promise
那么当前的promise的状态会由传入的promise来决定  相当于状态的移交
3.传入一个对象 如果对像之中实现了then方法 （并且这个对象是实现thenable接口）
那么也会执行该 then方法 并且由该then方法决定后续的状态
*/

//1，传入Promise的特殊情况 
new Promise((resolve,reject)=>{
   resolve()  //pending -> fulfilled
}).then(res=>{
   console.log('res',res);
},err=>{
   console.log('err',err);
})

//2.传入一个新的 promise那么当前的promise的状态会由传入的promise来决定  相当于状态的移交
const newPromise = new Promise((resolve,reject)=>{
    resolve('aaaaaa')
    // reject('error message')
})

new Promise((resolve,reject)=>{
    resolve(newPromise)  //pending -> fulfilled
 }).then(res=>{
    console.log('res',res);
 },err=>{
    console.log('err',err);
 })

 //3.传入一个对象 如果对象之中实现then方法 （并且这个对象中实现thenable接口）
 //那么也会执行该 then方法 并且由该then方法决定后续的状态
 
  new Promise((resolve,reject)=>{
      const obj={
          then:function (resolve,reject) {
            //   resolve('resolve message');
               reject('reject message');
          }
      }
      resolve(obj)
  }).then(res=>{
      console.log('res',res);
  },err=>{
      console.log('err',err);
  })

//【thenable:在其他强类型语言中 thenable指的就是 interface确定的方法名称和方法类型】
// eatable/runable
//在强类型语言中 相当于实现了 eat 和 run 的接口 类型检测 就叫做 eatable 和 runable
const obj = {
    eat: function() {
  
    },
    run: function() {
  
    }
  }
  
```

### Promise的then方法

then方法是Promise对象上的一个方法：它其实是放在Promise的原型上的 Promise.prototype.then

**then方法接受两个参数：**

fulfilled的回调函数：当状态变成fulfilled时会回调的函数；

reject的回调函数：当状态变成reject时会回调的函数；

```js
//查看Promise的哪些对象方法
// console.log(Object.getOwnPropertyDescriptors(Promise.prototype));


const promise = new Promise((resolve,reject)=>{
    resolve('hahaha')
})

//1.同一个promise可以被多次调用then方法
// 当我们的resolve被调用时 所有的then方法 传入的回调函数都会被调用、

 promise.then(res => {
  console.log("res1:", res)
})

promise.then(res => {
  console.log("res2:", res)
})

promise.then(res => {
  console.log("res3:", res)
})
 




//2.then方法传入的回调函数 可以有返回值的 
//then方法本身也是由返回值的 他的返回值是Promsie

//1>如果我们返回的是一个普通值 那么这个普通的值 (数值/字符串/普通对象/undefined)
//会被作为一个新的 Promise的resolve值
//如果链式调用没写 return 就是返回一个undefined

 promise.then((res)=>{
  return 'aaaaaa' //将aaaaa作为一个promise resolve返回值  .then
}).then((res)=>{  //then对应返回的新的Promise值
   console.log('res',res); //aaaaaa
   return 'bbbbbb'
}).then((res)=>{
    console.log('res2',res); //bbbbbb
})
 

//2> 如果我们返回的是一个Promise 

 promise.then(res=>{
    return new Promise ((resolve,reject)=>{ //由返回Promise决定状态的返回值
        setTimeout(() => {
              resolve('promise一次返回')
           // reject(22222)
        }, 3000);
    })
}).then((res)=>{
    console.log('res',res);  //res 1111
    return new Promise((resolve,reject)=>{
           setTimeout(() => {
                resolve('promise二次返回')
           }, 2000);
    })
},(err)=>{
  console.log('err',err);  //err 22222
}).then((res)=>{
    console.log('res',res);
})
 



//3> 如果·返回的是一个对象 并且对象实现了 thenable 

promise.then((res)=>{
   return{
    then: function(resolve, reject) {
        // resolve(222222)
        reject(111111)
      } 
   }
}).then((res)=>{
    console.log('res',res);
},(err)=>{
    console.log('err',err);
})
```

### Promise的catch方法

 catch方法也是Promise对象上的一个方法：它也是放在Promise的原型上的 Promise.prototype.catch 

 一个Promise的catch方法是可以被多次调用的： 

每次调用我们都可以传入对应的reject回调； 

当Promise的状态变成reject的时候，这些回调函数都会被执行；

```js
// const promise = new Promise((resolve,reject)=>{
//     //resolve('wangyaoda')
//      reject('reject status')
// // throw  Error('rejected status')

// })

//传入第一个是undefined 就表示了 我们不执行resolve后面的回调

//1>当excutor抛出异常的时候 也是会调用错误(拒绝)捕获的回调函数的
/* promise.then(undefined,err=>{
   console.log('err',err);
   console.log('---------------------');
})
 */

//2> 通过catch方法来传入错误(拒绝)的捕获的回调函数 一种新的写法
//promise a+规范 这种写法不算是符合a+规范 正常写在then回调里面 跟这个catch回调是一样的


/* promise.catch((err)=>{
     console.log('err',err);  //err reject status
})
 */

// promise.then(res=>{
//     console.log('res',res);
//     //如果第一个promise回调时正确的· 第二个promise回调是错误的 那么
//     //回调catach捕获的就是错误的第二个Promise回调 抛出异常也是一样

//    /*  return new Promise((resolve,reject)=>{
//          reject('第二个promise的错误回调')
//     }) */
//     throw new Error('error message')
// }).catch(err=>{
//   console.log('err',err);  //err 第二个promise的错误回调
// }) 



//3>拒绝捕获的问题（前面的课程）
// promise.then(res=>{

// },err=>{
//     console.log('err错误',err); //err错误 reject status
// })

/* 
const promise2 = new Promise((resolve,reject)=>{
//reject('1111111')
resolve('000000')
})

promise2.then(res=>{
    return new Promise((resolve,reject)=>{
    
        setTimeout(() => {
            resolve('第一次回调')
        }, 2000);
    })
}).then(res=>{
    // throw Error ('then error message')
    console.log('res',res);
    return new Promise((resolve,reject)=>{
      
        setTimeout(() => {
            resolve('第二次回调')
        }, 2000);
      
    })
}).then((res)=>{
  console.log('res',res);
}).catch(err=>{
    console.log('err',err);
}) 
*/


//4.catch方法的返回值 catch方法 也是返回一个promise的
const promise = new Promise((resolve,reject)=>{
    reject('111111')
})

promise.then(res=>{
    console.log('res',res);
}).catch((err)=>{
console.log('err1',err);
// throw new Error('错误信息')
return 'catch return value' //返回一个普通类型 返回一个被promise resolve包裹的普通类型

}).then((res)=>{
  console.log('res result',res);
  return new Promise((resolve,reject)=>{
   reject('222222')
  })
}).catch(err=>{
    console.log('err2 result' , err);
})

/* 
err1 111111
res result catch return value
err2 result 222222
*/
```

### Promise对象的finally方法

```js
/* 

finally是在ES9（ES2018）中新增的一个特性：
表示无论Promise对象无论变成fulfilled还是reject状态，最终都会被执行的代码。
finally方法是不接收参数的，因为无论前面是fulfilled状态，还是reject状态，它都会执行。
*/

const promise = new Promise((resolve,reject)=>{
   resolve('resolve message')
 //  reject('reject message')
})

promise.then(res=>{
    console.log('res1',res);
}).catch(err=>{
    console.log('err',err);
}).finally(()=>{ //java语言里面有该语法
    console.log('finally code execute');
})
```

### Promise对象的类方法 resolve

```js
//转成Promise对象
/* 
function foo() {
    const obj = {name:'wangyaoda'}
    return new Promise((resolve)=>{
        resolve(obj)
    })
}

foo().then(res=>{
    console.log('res',res);
})
*/

//类方法Promise.resolve
//1.普通的值
const promise = Promise.resolve({name:'wyd'})
//相当于
// const promise2 = new Promise((resolve,reject)=>{
//     resolve({name:'wyd'})
// })
promise.then(res=>{
    console.log('res',res); //res { name: 'wyd' }
})


//2.传入Promise
const promise2 =  Promise.resolve(new Promise((resolve,reject)=>{
    resolve('11111')
}))

promise2.then(res=>{
    console.log('res',res); //res 1111
})

//3.传入thenable对象 
const obj={
    then:function (resolve,reject) {
         resolve('resolve message');
         //reject('reject message');
    }
}
const promise3 = Promise.resolve(obj)

promise3.then(res=>{
   console.log('res',res);  //res resolve message
},err=>{
    console.log('err',err);
})
```

### Promise对象的类方法 reject

```js
// //转成 Promise对象
// const promise = Promise.reject('reject message')
// //相当于
// const promise2 = new Promise((resolve,reject)=>{
//     reject('reject message')
// })



//注意 reject 无论传入什么值都是一样的  全部都是回调到 err里面的
 const promise = Promise.reject(new Promise((resolve,reject)=>{
       resolve('1111')
    //reject('000')
 }))

/*  promise.then(res=>{
     console.log('res',res);
 },err=>{
     console.log('err',err); //Promise { '1111' }
 }) */

 //等价于
 promise.then(res=>{
     console.log('res',res);
 }).catch(err=>{
     console.log('err',err);
 })
```

### Promise对象的类方法 all

```js
//创建多个Promise
const p1 = new Promise((resolve,reject)=>{
    setTimeout(() => {
        resolve('11111')
    }, 1000);
})

const p2= new Promise((resolve,reject)=>{
    setTimeout(() => {
        // resolve('222222')
        reject('2222222')
    }, 2000);
})


const p3 = new Promise((resolve,reject)=>{
    setTimeout(() => {
        resolve('333333')
    }, 3000);
})


//传入多个promise 等到所有的promise都变成了 fulfilled的时候 在拿到结果
//意外 在拿到所有的结果之前  有一个promise变成 reject 那么整个promise就变成了 reject
//promise.all 返回也是一个promise

Promise.all([p1,p2,p3,'wangyaoda']).then(res=>{
    console.log(res);  //三秒之后[ '11111', '222222', '333333', 'wangyaoda' ]
}).catch(err=>{
    console.log('err',err);  //错误  err 2222222 
})

/* 
问题 如果想要所有都显示呢 就算是 promise里面有 reject 也不阻止resolve的显示
调用promise.allSettled方法
*/
```

### Promise类方法 allSettled

```js
// 创建多个Promise ES11新增方法
const p1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(11111)
    }, 1000);
})

const p2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        reject(22222)
    }, 2000);
})

const p3 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(33333)
    }, 3000);
})


//allSettled
Promise.allSettled([p1, p2, p3, 'aaaa']).then(res => {
    console.log(res);
}, err => {
    console.log(err);
})

/*
[
{ status: 'fulfilled', value: 11111 },
{ status: 'rejected', reason: 22222 },
{ status: 'fulfilled', value: 33333 },
{ status: 'fulfilled', value: 'aaaa' }
]
*/
```

### Promise类方法 race

```js
const p1 = new Promise((resolve,reject)=>{
    setTimeout(() => {
        resolve(11111)
    }, 1000);
})

const p2 = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(22222)
    }, 2000);
  })
  
  const p3 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(33333)
    }, 3000);
  })

  /* 
  race:竞技/竞赛
  只要有一个promise变成fulfilled状态 那么就结束 也要等所有Promise运行完
  如果在resolve之前有一个reject 那么就直接是 reject状态 
  */

  Promise.race([p1,p2,p3]).then(res=>{
    console.log('res',res);
  }).catch(err=>{
   console.log('err',err);
  })

 
  /* 
  res 11111
   如果至少等到必须有一个人resolve的话 就需要用any
  */
```

### Promise类方法 any

```js
//es12 （2021）新增特性
const p1 = new Promise((resolve,reject)=>{
    setTimeout(() => {
        reject(11111)
    }, 1000);
})

const p2 = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(22222)
    }, 2000);
  })
  
  const p3 = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(33333)
    }, 3000);
  })
 
  //any放啊
  /* 
  没有提示原因 这个 是新增特性 node版本过低不支持
  */
  Promise.any([p1,p2,p3]).then(res=>{
      console.log('res',res);
  }).catch(err=>{
      console.log('err',err);
  })

  /* 
  res 11111  
  必须等到有一个resolve的时候 才会返回结果
  */

  /* 
  如果三个都是错误的
  返回【这个是自己内部封装的错误】 err AggregateError: All promises were rejected
  */
```

## 迭代器和生成器

### 什么是迭代器Iterator

迭代器（iterator），是确使用户可在容器对象（container，例如链表或数组）上遍访的对象，使用该接口无需关心对象的内部实现细节。

其行为像数据库中的光标，迭代器最早出现在1974年设计的CLU编程语言中；在各种编程语言的实现中，迭代器的实现方式各不相同，但是基本都有迭代器，比如Java、Python等；从迭代器的定义我们可以看出来，迭代器是帮助我们对某个数据结构进行遍历的对象。

**在JavaScript中，迭代器也是一个具体的对象，这个对象需要符合迭代器协议（iterator protocol）**：迭代器协议定义了产生一系列值（无论是有限还是无限个）的标准方式；那么在js中这个标准就是一个特定的next方法；
next方法有如下的要求：
**无参数或者一个参数的函数，返回一个应当拥有以下两个属性的对象：**
**done（boolean）**

**如果迭代器可以产生序列中的下一个值，则为false。（这等价于没有指定done 这个属性。）如果迭代器已将序列迭代完毕，则为true。这种情况下，value 是可选的，如果它依然存在，即为迭代结束之后的默认返回值**。

**value  迭代器返回的任何JavaScript 值。done 为true 时可省略。**

```js
//编写一个迭代器
const iterator = {
    next() {
        return { done: true, value: 234 }
    }
}

//编写一个数组 
const names = ['wangyaoda', 'wangzhang', 'liyinhe'] //数组就是一个迭代器

let index = 0
const namesInterator = {
    next() {
        if (index < names.length) {
            return { down: false, value: names[index++] }
        } else {
            return { done: true, value: undefined }
        }
    }
}

/* 
{ down: false, value: 'wangyaoda' }
{ down: false, value: 'wangzhang'}
{ down: false, value: 'liyinhe' }
{ done: true, value: undefined }
{ done: true, value: undefined }
{ done: true, value: undefined }
{ done: true, value: undefined }
{ done: true, value: undefined }
{ done: true, value: undefined }
*/

console.log(namesInterator.next());
console.log(namesInterator.next()); 
console.log(namesInterator.next());
console.log(namesInterator.next());
console.log(namesInterator.next());
console.log(namesInterator.next()); 
console.log(namesInterator.next());
console.log(namesInterator.next());
console.log(namesInterator.next());
```

### 生成迭代器的函数

```js
//全局创建一个迭代器函数
function createArrayIterator(arr) {
    let index = 0;
    //将next方法返回出去
    return{

         next () {
           if (index<arr.length) {
                return { done:false, value:arr[index++]}
           }else{
               return { done:true,value:undefined}
           }
       }
    }
}

const names=['wangyaoda','wangzhang','liyinhe']

const nums=[1,2,3,4,5,6,7,87]


const namesInterator= createArrayIterator(nums)
console.log(namesInterator.next())
console.log(namesInterator.next())
console.log(namesInterator.next())
console.log(namesInterator.next())
console.log(namesInterator.next())
console.log(namesInterator.next())
console.log(namesInterator.next())
console.log(namesInterator.next())
console.log(namesInterator.next())

const namesIterator = createArrayIterator(names)
console.log(namesIterator.next())
console.log(namesIterator.next())
console.log(namesIterator.next())


//创建一个无限的迭代器
 function createNumberIterator() {
     let index = 0 
     return{
         next(){
             return{done:false,value:index++}
         }
     }
 }
//无限迭代器 一直done：false
 const numberInterator = createNumberIterator()
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
console.log(numberInterator.next())
```

### 认识可迭代对象

什么又是可迭代对象呢？
**它和迭代器是不同的概念；**
**当一个对象实现了iterable protocol协议时，它就是一个可迭代对象；**
**这个对象的要求是必须实现@@iterator 方法，在代码中我们使用Symbol.iterator 访问该属性；**
当我们要问一个问题，我们转成这样的一个东西有什么好处呢？
当一个对象变成一个可迭代对象的时候，进行某些迭代操作，比如for...of 操作时，其实就会调用它的@@iterator [Symbol.iterator] 方法；

【换句话说 迭代器就是里面实现 next函数 并且返回值是一个对象，里面包含 done 和 value的 】 

【而可迭代的对象是值里面有[Symbol.iterator]函数的时候，并且将 next函数返回出来  在我们进行迭代操作的时候 会进行调用此函数的操作】

```js
//创建一个迭代器对象来访问数组
const iterableObj = {
    names:['wangyaoda','wangzhang','linyinhe','wangxiaobo'],
    //可迭代对象要求实现的函数
    [Symbol.iterator](){
        let index = 0
        return{
            //将函数写成箭头函数 指向外层的this.name 否则在调用的时候 interator.next()则指向iterator 
      //里面没有this 所以需要箭头函数
            next:()=>{
               if (index<this.names.length) {
                    return { done:false,value:this.names[index++]}
               }else{
                   return { done:true ,value:undefined}
               }
            }
        }
    }
}
//iterableObj 对象是一个可迭代对象 每次调用都生成一个迭代器
 
console.log(iterableObj[Symbol.iterator]);

//第一次调用iterableObj[Symbol.iterator]函数
const iterator = iterableObj[Symbol.iterator]()
console.log(iterator.next())
console.log(iterator.next())
console.log(iterator.next())
console.log(iterator.next())

//第二次调用iterableObj[Symbol.iterator]函数   //每一次都会创建一个新的函数 并不会使用以前的函数
const iterator1 =iterableObj[Symbol.iterator]()
 console.log(iterator1.next())
 console.log(iterator1.next())
 console.log(iterator1.next())
 console.log(iterator1.next())


 //3.对于 for of 可以便利的东西必须是一个可迭代对象      对象的格式不支持for of 迭代

 const obj = {
     name:'wangyaoda',
     age:21
 }

 for (const iterator of iterableObj) {
     console.log(iterator);
 }
```

### 原生可迭代对象


事实上我们平时创建的很多原生对象已经实现了可迭代协议，会生成一个迭代器对象的：
**String、Array、Map、Set、arguments对象、NodeList集合；**

```js
//1.数组对象
const names = ["abc", "cba", "nba"]
console.log(names[Symbol.iterator]);

const iterator = names[Symbol.iterator]()
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());

/* 
{ value: 'abc', done: false }
{ value: 'cba', done: false }
{ value: 'nba', done: false }
{ value: undefined, done: true }
{ value: undefined, done: true }
*/

for (const iterator of names) {
    console.log(iterator);  //abc cba nba
}

//Map/Set 
const set = new Set(names)  //传入一个可迭代对象
console.log(set); //Set { 'abc', 'cba', 'nba' } 

set.add(10);
set.add(100);
set.add(1000);

console.log(set[Symbol.iterator]);

for (const item of set) {
    console.log(item)
    /* 
abc
cba
nba
10
100
1000
    */
}


//函数中的arguments也是一个可以迭代的对象

function foo(x,y,z) {
    console.log(arguments[Symbol.iterator]);
    console.log('arguments',...arguments);
    for (const arg of arguments) {
        console.log(arg);
    }
}

foo(1,2,3,4,5)  

// [Function: values]
// arguments 1 2 3 4 5
// 1
// 2
// 3
// 4
// 5


//String

let W = 'wangyaoda';
console.log(W[Symbol.iterator]);
for (const str of W) {
    console.log(str);
}
// w a n g y a o d a
```

### 可迭代对象应用场景

JavaScript中语法：for ...of、展开语法（spread syntax）、yield*（后面讲）  解构赋值（Destructuring_assignment）；
创建一些对象时：new Map([Iterable])、new WeakMap([iterable])、new Set([iterable])、new WeakSet([iterable]);
一些方法的调用：Promise.all(iterable)、Promise.race(iterable)、Array.from(iterable);

```js
//1. for of


//2.展开语法（spread syntax）

//创建一个迭代器对象来访问数组
const iterableObj = {
    names:['wangyaoda','wangzhang','linyinhe','wangxiaobo'],
    //可迭代对象要求实现的函数
    [Symbol.iterator](){
        let index = 0
        return{
            //将函数写成箭头函数 指向外层的this.name 否则在调用的时候 interator.next()则指向iterator 
      //里面没有this 所以需要箭头函数
            next:()=>{
               if (index<this.names.length) {
                    return { done:false,value:this.names[index++]}
               }else{
                   return { done:true ,value:undefined}
               }
            }
        }
    }
}
const names=[1,2,3,4,5]
const newName= [...names]
const newIterator = [...iterableObj]
console.log(newName,newIterator); //[ 1, 2, 3, 4, 5 ] [ 'wangyaoda', 'wangzhang', 'linyinhe', 'wangxiaobo' ]

const obj = { name:'wangyaoda', age:22} // for of 不可以遍历  

//es9之中新增一个特性 ：用的不是迭代器 但是可以展示里面的内容
const newOBJ = {...obj}

console.log(newOBJ); //{ name: 'wangyaoda', age: 22 }


//3.结构语法 
const [name1,name2]=names
const {name,age} = obj //不一样ES9新增特性  

console.log(name1,name2,name,age);  //1 2 wangyaoda 22 


//4.创建其他对象的时候1 传入一个可以迭代的对象 

const set1 = new Set(iterableObj)
const set2 = new Set(names)

const arr1 = Array.from(iterableObj) //Array.form 将可迭代的对象转成数组

console.log(set1,set2,arr1);
//Set { 'wangyaoda', 'wangzhang', 'linyinhe', 'wangxiaobo' } Set { 1, 2, 3, 4, 5 } [ 'wangyaoda', 'wangzhang', 'linyinhe', 'wangxiaobo' ]   

// 5.Promise.all
Promise.all(iterableObj).then(res => {
    console.log(res)
  })
```

### 自定义类的可迭代性

创建一个classroom的类
教室中有自己的位置、名称、当前教室的学生；
这个教室可以进来新学生（push）；
创建的教室对象是可迭代对象；

```js
//如果自己定义一个类 是不可以使用for of进行遍历的 因为类具有不可迭代性
/* class Person {

} */

//案列 创建一个教室类，创建出来的对象都是可迭代对象

class ClassRoom {
    constructor(address, name, students) {
        this.address = address;
        this.name = name;
        this.students = students
    }
    entry(newStudent) {
        this.students.push(newStudent)
    }
    [Symbol.iterator]() {
        let index = 0;
        return {
            next: () => {
                if (index < this.students.length) {
                    return { done: false, value: this.students[index++] }
                }else{
                    return { done:true,value:undefined}
                }
            },
            //监听迭代器是否提前终止 固定的方法
            return:()=>{
                console.log('迭代器提前终止');
                return { done: true, value: undefined }
            }
        }
    }

}


const class1 = new  ClassRoom('东宁一中','高三十二班',['wangyaoda','wangzhang','lihua','liyinhe']);
class1.entry('wangxiaoxbo')

for (const stu of class1) {
    console.log(stu);

    //迭代器的中断 比如遍历的过程中通过break、continue、return、throw中断了循环操作
    //可以用自定义迭代器中的 return函数进行监听
    if (stu==='wangzhang') {
        break
    }
}
/* 
wangyaoda
wangzhang
lihua
liyinhe
wangxiaoxbo

*/

/* 如果想要写在构造函数之中   直接写在原型之中*/

function Person() {
    
}

Person.prototype[Symbol.iterator] = function () {
    
}
```

### 什么是生成器Generator

**生成器概念：生成器是ES6中新增的一种函数控制、使用的方案，它可以让我们更加灵活的控制函数什么时候继续执行、暂停执行等。**
平时我们会编写很多的函数，这些函数终止的条件通常是返回值或者发生了异常。
生成器函数也是一个函数，但是和普通的函数有一些区别：
**首先，生成器函数需要在function的后面加一个符号：***
**其次，生成器函数可以通过yield关键字来控制函数的执行流程：**
**最后，生成器函数的返回值是一个Generator（生成器）：**
生成器事实上是一种特殊的迭代器；
MDN：Instead, they return a special type of iterator, called a Generator.

```js
//生成器 后面加一个* 没有就是普通函数

function* foo() {
    console.log('函数开始执行');

    const value1 = 100
    console.log('第一段代码', value1);
    yield   //到yiled 阻塞执行 直到调用下一次next才会执行

    const value2 = 200
    console.log("第二段代码:", value2)
    yield

    const value3 = 300
    console.log("第三段代码:", value3)
    yield

    console.log("函数执行结束~")
}

// 调用生成器函数时, 会给我们返回一个生成器对象
const generator= foo()


//开始执行第一段代码
generator.next()  // console.log("第一段代码:", 100)

// 开始执行第二端代码
console.log("-------------")
generator.next()   //------------- 第二段代码: 200
generator.next()  //第三段代码: 300
console.log("----------")
generator.next()  //函数执行结束~

```

### 生成器函数的执行过程

```js
//生成器 后面加一个* 没有就是普通函数

function* foo() {
    console.log('函数开始执行');

    const value1 = 100
    console.log('第一段代码', value1);
    yield  value1  //到yiled 阻塞执行 直到调用下一次next才会执行

    const value2 = 200
    console.log("第二段代码:", value2)
    yield value2

    const value3 = 300
    console.log("第三段代码:", value3)
    yield value3

    console.log("函数执行结束~")
    return '12345'
}

// generator本质上是一个特殊的iterator

const generator = foo()
 console.log("返回值1:", generator.next())
 console.log("返回值2:", generator.next())
 console.log("返回值3:", generator.next())
  console.log("返回值4:", generator.next())


/* 
函数开始执行
第一段代码 100
返回值1: { value: 100, done: false }
第二段代码: 200
返回值2: { value: 200, done: false }
第三段代码: 300
返回值3: { value: 300, done: false }
函数执行结束~
返回值4: { value: '12345', done: true }
*/
generator.next()
generator.next()
generator.next()
generator.next()
/* 

函数开始执行
第一段代码 100
第二段代码: 200
第三段代码: 300
函数执行结束~
*/
```

### 生成器next传递参数

函数既然可以暂停来分段执行，那么函数应该是可以传递参数的，我们是否可以给每个分段来传递参数呢？
答案是可以的；
我们在调用next函数的时候，可以给它传递参数，那么这个参数会作为上一个yield语句的返回值；
注意：也就是说我们是为本次的函数代码块执行提供了一个值；

```js
function * foo(num) {
     console.log('函数开始执行~');

     const value1 = 100 + num
     console.log('第一次执行代码',value1);
     const n1= yield value1
     //上一个yield的返回值 作为第二段代码执行传入的参数

     const value2 = 200 +n1
     console.log('第二次执行代码',value2);
     const n2 = yield value2
     //上一个yield的返回值 作为下一段代码执行传入的参数

     const value3 = 300 * n2
  console.log("第三段代码:", value3)
  yield value3

  console.log("函数执行结束~")
  return "123"
}

// 生成器上的next方法可以传递参数  
//第一个yiled传递参数 需要在函数处传值
const generator = foo(123)

console.log(generator.next());
// 第一次执行代码 223
// { value: 223, done: false }

console.log(generator.next(2));
//  第二次执行代码 202
// { value: 202, done: false } 

console.log(generator.next(5));
// 第三段代码: 1500
// { value: 1500, done: false }
```

### 生成器的return终止执行

还有一个可以给生成器函数传递参数的方法是通过return函数：
return传值后这个生成器函数就会结束，之后调用next不会继续生成值了；

```js
function * foo(num) {
    console.log('函数开始执行~');

    const value1 = 100 + num
    console.log('第一次执行代码',value1);
    const n1= yield value1
    //上一个yield的返回值 作为第二段代码执行传入的参数

    //return n 相当于 加上了这个return

    const value2 = 200 +n1
    console.log('第二次执行代码',value2);
    const n2 = yield value2
    //上一个yield的返回值 作为下一段代码执行传入的参数

    const value3 = 300 * n2
 console.log("第三段代码:", value3)
 yield value3

 console.log("函数执行结束~")
 return "123"
}


const generator = foo(10)

console.log(generator.next())

// 第二段代码的执行, 使用了return
// 那么就意味着相当于在第一段代码的后面加上return, 就会提前终端生成器函数代码继续执行
console.log(generator.return(15))
console.log(generator.next())
console.log(generator.next())
console.log(generator.next())
console.log(generator.next())
console.log(generator.next())
console.log(generator.next())
/* 
{ value: 1000, done: false }
{ value: 15, done: true }
{ value: undefined, done: true }
{ value: undefined, done: true }
{ value: undefined, done: true }
{ value: undefined, done: true }
{ value: undefined, done: true }
{ value: undefined, done: true }
*/
```

### 生成器抛出异常throw

了解生成器函数内部传递参数之外，也可以给生成器函数内部抛出异常：
抛出异常后我们可以在生成器函数中捕获异常；
在catch语句中也可以继续yield新的值了，但是可以在catch语句外使用yield继续中断函数的执行；

```js
//很少使用到 这个抛出异常
function * foo() {
     console.log('代码开始执行');

     const value1 = 100
     console.log('第一段代码执行' ,value1);
       //因为下面是抛出异常 所以现在是 捕获异常后 代码可以继续执行
     try {
         
         yield value1
     } catch (error) {
         console.log('捕获到这里异常',error);
     }

     const value2 = 100
     console.log('第二段代码执行' ,value2);
     yield value2

  
}

const generator = foo()


generator.next(200)
generator.throw('这里有个异常')


/* 
代码开始执行
第一段代码执行 100
捕获到这里异常 这里有个异常
第二段代码执行 100
*/


```

### 生成器替换迭代器

废话少说直接上代码

##### （1）

```js
//全局创建一个迭代器函数
function createArrayIterator(arr) {
    let index = 0;
    //将next方法返回出去
    return{

         next :()=> {
           if (index<arr.length) {
                return { done:false, value:arr[index++]}
           }else{
               return { done:true,value:undefined}
           }
       }
    }
}

const names =['wangyaoda','wangzahng','liyinhe'];
// const namesInterator= createArrayIterator(names)

// console.log(namesInterator.next());
// console.log(namesInterator.next());
// console.log(namesInterator.next());
// console.log(namesInterator.next());
/* 
{ done: false, value: 'wangyaoda' }
{ done: false, value: 'wangzahng' }
{ done: false, value: 'liyinhe' }  
{ done: true, value: undefined }   

*/

//用生成器来替代迭代器
function* createArrayIterator(arr) {
     //第一种写法
     //  yield 'wangyaoda'    //{ value: 'wangyaoda', done: false }
     //  yield 'wangzhang'    //{ value: 'wangzhang', done: false }
     //  yield 'liyinghe'     //{ value: 'liyinghe', done: false }
                              //{ value: undefined, done: true }
    
 
    //第二种方法
    // for (const iterator of arr) {
    //    yield iterator
    // }

    //yiled * 相当于第二种方法的语法糖
    yield* arr  //后面传入一个可以迭代的对象

}

const namesInterator= createArrayIterator(names)
console.log(namesInterator.next());
console.log(namesInterator.next());
console.log(namesInterator.next());
console.log(namesInterator.next());

/* 
{ value: 'wangyaoda', done: false }
{ value: 'wangzahng', done: false }
{ value: 'liyinhe', done: false }
{ value: undefined, done: true }
*/
```

##### （2）

```js
//2.创建一个函数，这个函数可以迭代一个范围内的数字  10 ~ 20范围内的数字


//迭代器版本
//  function createRangeIterator(start,end) {
//     let index =start 
//     return{
//         next:()=>{
//             if (index<end) {
//                 return {done:false , value: index++ }
//             }else{
//                 return { done:true , value: undefined}
//             }
//         }
//     }
//  }


//  const rangeIterator = createRangeIterator(10, 20)
// console.log(rangeIterator.next())
// console.log(rangeIterator.next())
// console.log(rangeIterator.next())
// console.log(rangeIterator.next())
// console.log(rangeIterator.next())


/* 
{ done: false, value: 10 }
{ done: false, value: 11 }
{ done: false, value: 12 }
{ done: false, value: 13 }
{ done: false, value: 14 }
*/


//生成器版本
function * createRangeIterator(start,end) {
    let index = start
    while(index<end){
        yield index++
    }
}

const rangeIterator = createRangeIterator(10, 20)
console.log(rangeIterator.next())
console.log(rangeIterator.next())
console.log(rangeIterator.next())
console.log(rangeIterator.next())
console.log(rangeIterator.next())


/* 
{ done: false, value: 10 }
{ done: false, value: 11 }
{ done: false, value: 12 }
{ done: false, value: 13 }
{ done: false, value: 14 }
*/
```

##### （3）

```js
//class 案例
class ClassRoom {
    constructor(address, name, students) {
        this.address = address;
        this.name = name;
        this.students = students
    }
    entry(newStudent) {
        this.students.push(newStudent)
    }
    foo = () => {
        console.log("foo function")
      }
    
      //方法一
    //    [Symbol.iterator] = function*() {
    //      yield* this.students
    //    }

    //方法二 如果没有function关键字的时候 就将这个*写在 定义的函数前面
    *[Symbol.iterator]() {
        yield* this.students
      } 

}


const class1 = new  ClassRoom('东宁一中','高三十三班',['wangyaoda','wangzhang','lihua','liyinhe']);
class1.entry('wangxiaoxbo')
```

## 异步处理方案

```js
// request.js
function requestData(url) {
    // 异步请求的代码会被放入到executor中
    return new Promise((resolve, reject) => {
      // 模拟网络请求
      setTimeout(() => {
        // 拿到请求的结果
        resolve(url)
      }, 2000);
    })
  }
  
/* 该类需求类似于 请求员工信息 第一次请求员工共名字 第二次根据名字请求id 第三次
  根据id请求工资是多少
*/

  // 需求: 
  // 1> url: why -> res: why
  // 2> url: res + "aaa" -> res: whyaaa
  // 3> url: res + "bbb" => res: whyaaabbb

  //1.第一种方案 多次回调  但是会产生回调地狱 
  //回调函数中嵌套回调函数 多次嵌套回调函数
 
  requestData('why').then(res=>{
    requestData(res+'aaa').then(res=>{
      requestData(res+'bbb').then(res=>{
        requestData(res+'ccc').then(res=>{
          console.log(res);
        })
      })
    })
  }) 
  //whyaaabbbccc


  //2.第二种方案 Promise的then 返回值来解决这个问题 但是阅读性稍差
   
   requestData('why').then(res=>{
    //return 返回出来一个结果 作为下一次.then调用的结果
    return requestData(res+'aaa')
  }).then(res=>{
    return  requestData(res+'bbb')
  }).then(res=>{
    return requestData(res+'ccc')
  }).then(res=>{
    console.log(res);
  }) 
   

//3.第三种方法 Promise + generator 实现
  
   function* getData() {
    const res1= yield requestData('why');
    // console.log(res1);
    const res2 = yield requestData(res1+'aaa');
    // console.log(res2);
    const res3 = yield requestData(res2+'bbb');
    // console.log(res3);
    const res4 = yield requestData(res3 +'ccc');
    console.log(res4);
   }




   //(1)手动执行生成器函数   
/*    const generator =getData()


  // console.log(generator.next());  //{ value: Promise { <pending> }, done: false }

  // console.log(generator.next().value); //Promise { <pending> }

  // generator.next().value.then(res=>{
  //   console.log(res);   //why
  // })

   generator.next().value.then(res=>{
     //console.log(res);   //why
     generator.next(res).value.then(res=>{
       //console.log(res);  //whyaaa
        generator.next(res).value.then(res=>{
          //console.log(res); //whyaaabbb
          generator.next(res).value.then(res=>{
            console.log(res); //whyaaabbbccc
          })
       })
     })
   }) 
   
   */

   //(2) 自己封装了一个自动执行的函数 可以自己执行生成器函数 递归执行 自己调用自己
    function execGeneraor(genFn) {
      const  generator = genFn()
      function exec(res) {
        const result = generator.next(res)
        if (result.done) {
           return result.value
        }else{
          result.value.then(res=>{
            exec(res)
          })
        }
      }
      exec()
   } 
   //execGeneraor(getData) //whyaaabbbccc


// (3) 第三方包co自动执行  封装了递归执行迭代器
// 作者TJ: 作品： co/n(nvm)/commander(coderwhy/vue cli)/express/koa(egg)
// 递归函数
// const co = require('co')
//co(getData)  //whyaaabbbccc



 
//4.async await ES7
//就是promise和generator封装的语法糖 非常好用
//就是promise和generator封装的语法糖 非常好用
async function getDATA() {
    const res1 = await requestData('why');
    const res2 = await requestData( res1+'aaa');
    const res3 = await requestData(res2+'bbb');
    const res4 = await requestData(res3+'ccc');
    console.log(res4);
}

getDATA()

```

## async_await

async关键字用于声明一个异步函数： 

**async是asynchronous单词的缩写，异步、非同步；** 

**sync是synchronous单词的缩写，同步、同时；**

### async异步函数的写法

```js
// await/async
async function foo1() {

}

const foo2 = async () => {

}

class Foo {
  async bar() {

  }
}

```

### async异步函数的执行流程

**异步函数有返回值时，和普通函数会有区别：** 

情况一：异步函数也可以有返回值，但是异步函数的返回值会被包裹到Promise.resolve中； 

情况二：如果我们的异步函数的返回值是Promise，Promise.resolve的状态会由Promise决定； 

情况三：如果我们的异步函数的返回值是一个对象并且实现了thenable，那么会由对象的then方法来决定； 

 如果我们在async中抛出了异常，那么程序它并不会像普通函数一样报错，而是会作为Promise的reject来传递；

```js
async function foo() {
  console.log("foo function start~")

  console.log("内部的代码执行1")
  console.log("内部的代码执行2")
  console.log("内部的代码执行3")

  console.log("foo function end~")
}

//直接执行 foo函数跟不同函数没什么区别  没特殊东西的时候 直接执行就完了

console.log("script start")
foo()
console.log("script end")

```

### 和普通函数相比区别 返回值 

```js
async function foo() {
    console.log("foo function start~")

    console.log("中间代码~")

    console.log("foo function end~")

    //返回一个值 
    // return 'abc'    //abc

    //返回一个thenable 实现接口后返回 根据接口状态返回
    // return {
    //     then: function (resolve, reject) {
    //         // resolve("hahahaha")
    //         reject('hehehehe')
    //     }

    // }


  //返回一个promise 根据promise 来判断
  return new Promise((resolve,reject)=>{
    setTimeout(() => {
        resolve("hehehehe")
      }, 2000)
  })

}


//异步函数的返回值一定是一个promise
const promise = foo()
promise.then(res => {
    console.log(res);
}).catch(err=>{
    console.log(err);
})
```



### 和普通函数相比区别 异常 

```js
async function foo() {
    console.log("foo function start~")

  console.log("中间代码~")

  // 异步函数中的异常, 会被作为异步函数返回的Promise的reject值的
 
    throw new Error("error mess")


  console.log("foo function end~")
}


// 异步函数的返回值一定是一个Promise   必须用 .catch来捕获 抛出的异常
foo().catch(err => {
    console.log("1111err:",err)
  })
  
  console.log("后续还有代码~~~~~")
  
```



### await关键字

async函数另外一个特殊之处就是可以在它内部使用await关键字，而普通函数中是不可以的。 

await关键字有什么特点呢？ 

**通常使用await是后面会跟上一个表达式，这个表达式会返回一个Promise；** 

**那么await会等到Promise的状态变成fulfilled状态，之后继续执行异步函数；** 

 **如果await后面是一个普通的值，那么会直接返回这个值；** 

 **如果await后面是一个thenable的对象，那么会根据对象的then方法调用来决定后续的值；** 

 **如果await后面的表达式，返回的Promise是reject的状态，那么会将这个reject结果直接作为函数的Promise的 reject值；**

```js
// await 必须用在 async里面  await后面跟着表达式

function requestData() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
         resolve(222)
       // reject(1111)
      }, 2000);
    })
  }

  async function foo() {
      const res1 = await requestData()
      //await后面不返回值 就会堵塞代码
      console.log("后面的代码1", res1)
  console.log("后面的代码2")
  console.log("后面的代码3")

  const res2 = await requestData()
  console.log("res2后面的代码", res2)
  }

  foo()


  // 2.跟上其他的值
// async function foo() {
//   // const res1 = await 123
//   // const res1 = await {
//   //   then: function(resolve, reject) {
//   //     resolve("abc")
//   //   }
//   // }
//   const res1 = await new Promise((resolve) => {
//     resolve("why")
//   })
//   console.log("res1:", res1)
// }

// 3.reject值
// async function foo() {
//   const res1 = await requestData()
//   console.log("res1:", res1)
// }

// foo().catch(err => {
//   console.log("err:", err)
// })

```

## 事件循环

### 进程和线程

n线程和进程是操作系统中的两个概念： 

**进程（process）：计算机已经运行的程序，是操作系统管理程序的一种方式；** 

**线程（thread）：操作系统能够运行运算调度的最小单位，通常情况下它被包含在进程中；** 

 **听起来很抽象，这里还是给出我的解释：** 

**进程：我们可以认为，启动一个应用程序，就会默认启动一个进程（也可能是多个进程）；** 

**线程：每一个进程中，都会启动至少一个线程用来执行程序中的代码，这个线程被称之为主线程；** 

**所以我们也可以说进程是线程的容器；** 

 再用一个形象的例子解释： 

操作系统类似于一个大工厂

 工厂中里有很多车间，这个车间就是进程； 

每个车间可能有一个以上的工人在工厂，这个工人就是线程；

### 操作系统的工作方式

 操作系统是如何做到同时让多个进程（边听歌、边写代码、边查阅资料）同时工作呢？ 

**这是因为CPU的运算速度非常快，它可以快速的在多个进程之间迅速的切换；** 

**当我们进程中的线程获取到时间片时，就可以快速执行我们编写的代码；** 

对于用户来说是感受不到这种快速的切换的； 

**【换句话说 就是单核CPU分一些时间片 然后这个时间片 不断执行】**

你可以在Mac的活动监视器或者Windows的资源管理器中查看到很多进程：

![image-20211106185135572](./media/进程和线程.png)

### 浏览器中的JS线程

 我们经常会说JavaScript是单线程的，**但是JavaScript的线程应该有自己的容器进程：浏览器或者Node。** 

 浏览器是一个进程吗，它里面只有一个线程吗？ 

**目前多数的浏览器其实都是多进程的**，

当我们打开一个tab页面时就会开启一个新的进程，这是为了防止一个页 面卡死而造成所有页面无法响应，整个浏览器需要强制退出； 

**每个进程中又有很多的线程，其中包括执行JavaScript代码的线程；** 

 **JavaScript的代码执行是在一个单独的线程中执行的：** 

**这就意味着JavaScript的代码，在同一个时刻只能做一件事；** 

**如果这件事是非常耗时的，就意味着当前的线程就会被阻塞；** 

 所以真正耗时的操作，实际上并不是由JavaScript线程在执行的： 

**浏览器的每个进程是多线程的，那么其他线程可以来完成这个耗时的操作；** 

**比如网络请求、定时器，我们只需要在特性的时候执行应该有的回调即可；**

### 事件循环/事件队列

事件循环 形成了一个闭环

```js

console.log("script start")

// 业务代码
/* settimeout不是一个异步函数  其实是同步调用的 
   但是 settimeout 需要我们传入 一个函数 我们传入这个函数 需要在一秒之后调用 
   所以传入的函数是异步函数 【计时的操作由浏览器其他的线程来帮助我们来完成的】


   浏览器维护一个队列 队列【先进先出的数据结构】  默认情况下是空的内容，当计时器达到
   后 会将返回内容放置队列之中 当JS引擎发现有内容之后  会进行一个执行  JS引擎
   会不断地从队列里取出进行执行的
*/
setTimeout(function() {
  console.log('222');
}, 1000)

console.log("后续代码~")
console.log("script end")
```

![image-20211108155508778](./media/事件队列.png)

### 宏任务微任务

但是事件循环中并非只维护着一个队列，事实上是有两个队列：
宏任务队列（macrotaskqueue）：ajax、setTimeout、setInterval、DOM监听、UI Rendering等
微任务队列（microtask queue）：Promise的then回调、Mutation Observer API、queueMicrotask()

那么事件循环对于两个队列的优先级是怎么样的呢？
**1.main script中的代码优先执行（编写的顶层script代码）；**
**2.在执行任何一个宏任务之前（不是队列，是一个宏任务），都会先查看微任务队列中是否有任务需要执行**

**也就是宏任务执行之前，必须保证微任务队列是空的；**

**如果不为空，那么就优先执行微任务队列中的任务（回调）；**

```js
//详情解释

/* 
JS线程中 立即执行的代码 都是最顶层的代码
宏任务队列： SeTtimeout ajax网络请求  Dom回调[点击事件等等] SetTnterval  还有一些UI渲染完之后
微任务队列:  queueMicrotask promise.then  promise.catch promise.finally MUtation Observer
  
规范 在执行任何宏任务之前 都需要保证微任务队列已经被清空   如果不为空 先执行微任务队列中的任务
*/


//需要给这些代码加入到队列里面执行  不同的任务加入不同的队列
//宏任务Macrotask  微任务Microtask
setTimeout(() => {
    console.log("setTimeout")
  }, 1000)
  
  //微任务队列回调
  queueMicrotask(() => {
    console.log("queueMicrotask")
  })
  

  
Promise.resolve().then(() => {
    console.log("Promise then")
  })


 /*  
 被称之为 Main script 是在顶层之间执行的代码
 function foo() {
    console.log("foo")
  }
  
  function bar() {
    console.log("bar")
    foo()
  }
  
  bar()
  
  console.log("其他代码") */
```

### Node中的事件循环

![image-20211109155956581](./media/node中的事件循环.png)

浏览器中的EventLoop是根据HTML5定义的规范来实现的，不同的浏览器可能会有不同的实现，而Node中是由libuv实现的。
这里我们来给出一个Node的架构图：
我们会发现libuv中主要维护了一个EventLoop和worker threads（线程池）；
EventLoop负责调用系统的一些其他操作：文件的IO、Network、child-processes等
libuv是一个多平台的专注于异步IO的库，它最初是为Node开发的

### JS应用到服务端

服务器开发对于变成语言有什么样的要求：IO操作  

IO：input output  读取一些数据到程序里面 / 写入一些东西到程序外面（文件/数据库）

### Node事件循环的阶段

我们最前面就强调过，事件循环像是一个桥梁，是连接着应用程序的JavaScript和系统调用之间的通道：
无论是我们的文件IO、数据库、网络IO、定时器、子进程，在完成对应的操作后，都会将对应的结果和回调函数放到事件循环（任务队列）中；

**事件循环会不断的从任务队列中取出对应的事件（回调函数）来执行；**
**但是一次完整的事件循环Tick分成很多个阶段：**

**定时器（Timers）**：本阶段执行已经被setTimeout() 和setInterval() 的调度回调函数。
**待定回调（Pending Callback）**：对某些系统操作（如TCP错误类型）执行回调，比如TCP连接时接收到ECONNREFUSED。

**idle, prepare**：仅系统内部使用。
**轮询（Poll）**：检索新的I/O 事件；执行与I/O 相关的回调；
**检测（check）：**setImmediate() 回调函数在这里执行。
**关闭的回调函数：**一些关闭的回调函数，如：socket.on('close', ...)。



### Node中的宏任务和微任务

我们会发现从一次事件循环的Tick来说，Node的事件循环更复杂，它也分为微任务和宏任务：
**宏任务（macrotask）：setTimeout、setInterval、IO事件、setImmediate、close事件；**
**微任务（microtask）：Promise的then回调、process.nextTick、queueMicrotask；**

**但是，Node中的事件循环不只是 微任务队列和 宏任务队列：**

**微任务队列：**
**1.next tick queue：process.nextTick；**
**2、other queue：Promise的then回调、queueMicrotask；**
**宏任务队列：**

**timer queue：setTimeout、setInterval；**
**poll queue：IO事件；**
**check queue：setImmediate；**
**close queue：close事件；**

```js
//这两个比较重要
//在node里面 微任务queue nextTick比then先执行
 //宏任务 settimeout 比 setimmediate先执行
```

**node事件执行顺序**
所以，在每一次事件循环的tick中，会按照如下顺序来执行代码：
next tick microtask queue；
other microtask queue；
timer queue；
poll queue；
check queue；
close queue



node面试题

```js
async function async1() {
  console.log('async1 start')
  await async2()
  console.log('async1 end')
}

async function async2() {
  console.log('async2')
}

console.log('script start')

setTimeout(function () {
  console.log('setTimeout0')
}, 0)

setTimeout(function () {
  console.log('setTimeout2')
}, 300)

setImmediate(() => console.log('setImmediate'));

process.nextTick(() => console.log('nextTick1'));

async1();

process.nextTick(() => console.log('nextTick2'));

new Promise(function (resolve) {
  console.log('promise1')
  resolve();
  console.log('promise2')
}).then(function () {
  console.log('promise3')
})

console.log('script end')

// script start
// async1 start
// async2
// promise1
// promise2
// script end
// nexttick1
// nexttick2
// async1 end
// promise3
// settimetout0
// setImmediate
// setTimeout2
```

**执行结果分析**

![image-20211109173027875](./media/node队列循环.png)

## 错误处理方案


开发中我们会封装一些工具函数，封装之后给别人使用：
在其他人使用的过程中，可能会传递一些参数；
对于函数来说，需要对这些参数进行验证，否则可能得到的是我们不想要的结果；
很多时候我们可能验证到不是希望得到的参数时，就会直接return：
但是return存在很大的弊端：调用者不知道是因为函数内部没有正常执行，还是执行结果就是一个undefined；
事实上，正确的做法应该是如果没有通过某些验证，那么应该让外界知道函数内部报了；
如何可以让一个函数告知外界自己内部出现了错误呢？
通过throw关键字，抛出一个异常；
throw语句：
throw语句用于抛出一个用户自定义的异常；
当遇到throw语句时，当前的函数执行会被停止（throw后面的语句不会执行）；
如果我们执行代码，就会报错，拿到错误信息的时候我们可以及时的去修正代码。

### 01函数错误处理

```js
/**
 * 如果我们有一个函数, 在调用这个函数时, 如果出现了错误, 那么我们应该是去修复这个错误.
 */

function sum(num1, num2) {
  // 当传入的参数的类型不正确时, 应该告知调用者一个错误
  if (typeof num1 !== "number" || typeof num2 !== "number") {
    // return undefined
    throw "parameters is error type~"
  }

  return num1 + num2
}

// 调用者(如果没有对错误进行处理, 那么程序会直接终止)
 console.log(sum({ name: "why" }, true))
// console.log(sum(20, 30))

console.log("后续的代码会继续运行~")

```

### 02抛出异常的补充

```js
 //创建一个抛出的类
// class HYError {
//   constructor(errorCode, errorMessage) {
//     this.errorCode = errorCode
//     this.errorMessage = errorMessage
//   }
// }

function foo(type) {
  console.log("foo函数开始执行")

  if (type === 0) {
    // 1.抛出一个字符串类型(基本的数据类型)
    // throw "error"

    // 2.比较常见的是抛出一个对象类型
    // throw { errorCode: -1001, errorMessage: "type不能为0~" 

    // 3.创建类, 并且创建这个类对应的对象
    // throw new HYError(-1001, "type不能为0~")

    // 4.系统提供了一个Error 里面的信息是可以修改的  一半打印的是函数调用栈
      //从node开始的调用栈
    // const err = new Error("type不能为0")
    // err.name = "why"
    // err.stack = "aaaa"

    // 5.Error的子类
    const err = new TypeError("当前type类型是错误的~")

    throw err

    // 强调: 如果函数中已经抛出了异常, 那么后续的代码都不会继续执行了
    console.log("foo函数后续的代码")
  }

  console.log("foo函数结束执行")
}

foo(0)

console.log("后续的代码继续执行~")



```

### 03对抛出异常的捕获

我们会发现在之前的代码中，一个函数抛出了异常，调用它的时候程序会被强制终止：
这是因为如果我们在调用一个函数时，这个函数抛出了异常，但是我们并没有对这个异常进行处理，那么这个异常会继续传递到上一个函数调用中；
而如果到了最顶层（全局）的代码中依然没有对这个异常的处理代码，这个时候就会报错并且终止程序的运行；
我们先来看一下这段代码的异常传递过程：
foo函数在被执行时会抛出异常，也就是我们的bar函数会拿到这个异常；
但是bar函数并没有对这个异常进行处理，那么这个异常就会被继续传递到调用bar函数的函数，也就是test函数；
但是test函数依然没有处理，就会继续传递到我们的全局代码逻辑中；
依然没有被处理，这个时候程序会终止执行，后续代码都不会再执行了；

```js
function foo(type) {
 if(type===0){
     throw new Error('foo error message~')
 }
}

// 1.第一种是不处理, bar函数会继续将收到的异常直接抛出去
function bar() {
  foo(0)
}

function test() {
    bar()
}

function demo() {
  test()
}


// 两种处理方法:
// 1.第一种是不处理, 那么异常会进一步的抛出, 直到最顶层的调用
// 如果在最顶层也没有对这个异常进行处理, 那么我们的程序就会终止执行, 并且报错
// foo()
// 2.使用try catch来捕获异常

  demo()

console.log("后续的代码执行~")

```

是很多情况下当出现异常时，我们并不希望程序直接推出而是希望可以正确的处理异常：
这个时候我们就可以使用try catch，在ES10中，catch后面绑定的error可以省略。
当然，如果有一些必须要执行的代码，我们可以使用finally来执行：
finally表示最终一定会被执行的代码结构；
注意：如果try和finally中都有返回值，那么会使用finally当中的返回值；

```js
function foo(type) {
    if (type===0) {
        throw new Error('foo error message')
    }
}

//1.第一种是不处理,bar函数会继续将收到的函数抛出去
function bar() {
    try {
        
        foo(0)
    } catch (error) {
        // console.log("错误信息");
        // console.log(error);
    }finally{
        console.log('抛出异常');
    }
}

function test() {
    bar()
}

function demo() {
    test()
}


demo()
//两种处理方法 
/* 
1.第一种方法是不处理，那么异常会进一步抛出，知道最顶层的调用
如果在最顶层也没有对这个异常进行处理，那么我们的程序就会终止执行，并且报错
foo()

2.使用try catch来捕获异常 这样也可以 抛出错误 es10后的内容catch里面的error
可以不用写了 try catch  获取异常后不会阻塞代码
*/


console.log('后续的代码会执行么');
```

## 模块化开发

### 01什么是模块化

到底什么是模块化、模块化开发呢？ 

 **事实上模块化开发最终的目的是将程序划分成一个个小的结构；** 

 **这个结构中编写属于自己的逻辑代码，有自己的作用域，不会影响到其他的结构；** 

**这个结构可以将自己希望暴露的变量、函数、对象等导出给其结构使用；** 

**也可以通过某种方式，导入另外结构中的变量、函数、对象等；** 

上面说提到的结构，就是模块；按照这种结构划分开发程序的过程，就是模块化开发的过程；

 **无论你多么喜欢JavaScript，以及它现在发展的有多好，它都有很多的缺陷：** 

比如var定义的变量作用域问题；

 比如JavaScript的面向对象并不能像常规面向对象语言一样使用class；

 比如JavaScript没有模块化的问题； 



 Brendan Eich本人也多次承认过JavaScript设计之初的缺陷，但是随着JavaScript的发展以及标准化，存在的缺陷问题基 本都得到了完善。  无论是web、移动端、小程序端、服务器端、桌面应用都被广泛的使用；



### 02模块化的历史


在网页开发的早期，Brendan Eich开发JavaScript仅仅作为一种脚本语言，做一些简单的表单验证或动画实现等，那个时候代码还是很少的：

这个时候我们只需要讲JavaScript代码写到<script>标签中即可；
并没有必要放到多个文件中来编写；甚至流行通常来说JavaScript 程序的长度只有一行。

但是随着前端和JavaScript的快速发展，JavaScript代码变得越来越复杂了：

ajax的出现，前后端开发分离，意味着后端返回数据后，我们需要通过JavaScript进行前端页面的渲染；
SPA的出现，前端页面变得更加复杂：包括前端路由、状态管理等等一系列复杂的需求需要通过JavaScript来实现；

包括Node的实现，JavaScript编写复杂的后端程序，没有模块化是致命的硬伤；

所以，模块化已经是JavaScript一个非常迫切的需求：
但是JavaScript本身，直到ES6（2015）才推出了自己的模块化方案；
在此之前，为了让JavaScript支持模块化，涌现出了很多不同的模块化规范：AMD、CMD、CommonJS等；

### 03没有模块化带来的问题


早期没有模块化带来了很多的问题：比如命名冲突的问题
**当然，我们有办法可以解决上面的问题：立即函数调用表达式（IIFE）**
IIFE(Immediately Invoked Function Expression)
但是，我们其实带来了新的问题：
第一，我必须记得每一个模块中返回对象的命名，才能在其他模块使用过程中正确的使用；
第二，代码写起来混乱不堪，每个文件中的代码都需要包裹在一个匿名函数中来编写；
第三，在没有合适的规范情况下，每个人、每个公司都可能会任意命名、甚至出现模块名称相同的情况；
所以，我们会发现，虽然实现了模块化，但是我们的实现过于简单，并且是没有规范的。
我们需要制定一定的规范来约束每个人都按照这个规范去编写模块化的代码；
这个规范中应该包括核心功能：模块本身可以导出暴露的属性，模块又可以导入自己需要的属性；
JavaScript社区为了解决上面的问题，涌现出一系列好用的规范，接下来我们就学习具有代表性的一些规范。

### 04CommonJS


我们需要知道CommonJS是一个规范，最初提出来是在浏览器以外的地方使用，并且当时被命名为ServerJS，后来为了体现它的广泛性，修改为CommonJS，平时我们也会简称为CJS。

Node是CommonJS在服务器端一个具有代表性的实现；
Browserify是CommonJS在浏览器中的一种实现；
webpack打包工具具备对CommonJS的支持和转换；
所以，Node中对CommonJS进行了支持和实现，让我们在开发node的过程中可以方便的进行模块化开发：

**在Node中每一个js文件都是一个单独的模块；**
**这个模块中包括CommonJS规范的核心变量：exports、module.exports、require；**
我们可以使用这些变量来方便的进行模块化开发；
**前面我们提到过模块化的核心是导出和导入，Node中对其进行了实现：**

**exports和module.exports可以负责对模块中的内容进行导出；**

**require函数可以帮助我们导入其他模块（自定义模块、系统模块、第三方库模块）中的内容；**

#### (1)基本使用

main.js

```js
//使用另外一个模块导出的对象 就需要先行导入require 
const {name,age,sum} = require('./wyd.js')

console.log(name)
console.log(age)
console.log(sum(20, 30))
```

wyd.js

```js
let name = "wangyaoda";
let age = 20;

function sum(sum1,sum2) {
    return sum1+sum2
}

//1.导出方案 module.exports
module.exports ={
    name,
    age,
    sum
}
```



#### (2)内部原理

![node中commonjs的原理](./media/node中commonjs的原理.png)

就是 导出对象和导入对象两者指向一个对象  require函数 的话 其实就是 return module.exports 两者指向一个对象 可以通过修改其中一个值来验证 

导入对象

```js
//它的返回值 就是 导出对象
const wyd = require("./wyd.js")

console.log(wyd)

setTimeout(() => {
   console.log(wyd.name)
 
}, 2000)

//{ name: 'wyd', age: 18, foo: [Function: foo] }
//wz
```

导出对象

```js
const info = {
    name: "wyd",
    age: 18,
    foo: function() {
      console.log("foo函数~")
    }
  }
  
  setTimeout(() => {
     info.name = "wz"
  
  }, 1000)
  
  //指向哪一个对象 哪一个对象被导出
  module.exports = info
  
```

一秒后修改导出对象的值，两秒后输出导入对象的name  发现输出name=’wz‘ 故验证两者指向一个

#### (3)exports

**module.exports和exports有什么关系或者区别呢？**
我们追根溯源，通过维基百科中对CommonJS规范的解析：
CommonJS中是没有module.exports的概念的；
但是为了实现模块的导出，Node中使用的是Module的类，每一个模块都是Module的一个实例，也就是module；
所以在Node中真正用于导出的其实根本不是exports，而是module.exports；
因为module才是导出的真正实现者；
但是，为什么exports也可以导出呢？
这是因为module对象的exports属性是exports对象的一个引用；
也就是说module.exports = exports = main中的bar；

导出对象

```js
const name = "why"
const age = 18
function sum(num1, num2) {
  return num1 + num2
}

//exports 和 module.exports的区别  源码 
// module.exports = {}
// exports = module.exports

//第二种导出方式
 exports.name =name;
 exports.age=age
 exports.sum=sum

//这种方法 不会进行导出
// exports = {
//   name,
//   age,
//   sum
// }


// 这种代码不会进行导出  
// 因为先存入到module.exports里面之后 
//然后 module.export又指向了一个新的对象了
 
// exports.name = name
// exports.age = age
// exports.sum = sum

// module.exports = {

// }

//最终能导出的就是module.exports
```

导入对象

```js
const why = require("./wyd.js")

console.log(why.name)
console.log(why.age)
console.log(why.sum(20, 30))

```

#### (4)require函数

require是一个函数，可以帮助我们引入一个文件（模块）中导入的对象

require的查找规则

 https://nodejs.org/dist/latest-v14.x/docs/api/modules.html#modules_all_together

**情况一 ：**

require（X） X是一个核心模块，比如path，http 直接返回核心模块，并且停止查找

有限查找node中的核心模块

```js

// 情况一: 核心模块
// const path = require("path")
// const fs = require("fs")

// path.resolve()
// path.extname()

// fs.readFile()
```

**情况二：**

X是以 ./ 或 ../ 或 /（根目录）开头的

**第一步：将X当做一个文件在对应的目录下查找**； 

1.如果有后缀名，按照后缀名的格式查找对应的文件	 

2.如果没有后缀名，会按照如下顺序： 

 1> 直接查找文件X 

 2> 查找X.js文件 

 3> 查找X.json文件 

 4> 查找X.node文件 

 **第二步：没有找到对应的文件，将X作为一个目录** 

 查找目录下面的index文件 

 1> 查找X/index.js文件 

 2> 查找X/index.json文件 

3> 查找X/index.node文件 

 如果没有找到，那么报错：not found 

```js
// 情况二: 路径 ./ ../ /
const abc = require("./abc")

// console.log(abc.name)

```

**情况三**

直接是一个X（没有路径），并且X不是一个核心模块

```js

// 情况三: X不是路径也不是核心模块 使用第三方包的时候 去 node_module 
// const wyd = require('wyd') 
   const axios = require('axios')  

/* 
console.log(module.paths);
[
  'C:\\Users\\Administrator\\Desktop\\学习\\JS高级\\mypractice\\85_JS模块化\\02_CommonJS\\04_require细节\\node_modules',
  'C:\\Users\\Administrator\\Desktop\\学习\\JS高级\\mypractice\\85_JS模块化\\02_CommonJS\\node_modules',
  'C:\\Users\\Administrator\\Desktop\\学习\\JS高级\\mypractice\\85_JS模块化\\node_modules',  
  'C:\\Users\\Administrator\\Desktop\\学习\\JS高级\\mypractice\\node_modules',
  'C:\\Users\\Administrator\\Desktop\\学习\\JS高级\\node_modules',
  'C:\\Users\\Administrator\\Desktop\\学习\\node_modules',
  'C:\\Users\\Administrator\\Desktop\\node_modules',
  'C:\\Users\\Administrator\\node_modules',
  'C:\\Users\\node_modules',
  'C:\\node_modules'
]
*/
```

 如果上面的路径中都没有找到，那么报错：not found

#### (5)模块加载细节

**结论一：模块在被第一次引入时，模块中的js代码会被运行一次**
**结论二：模块被多次引入时，会缓存，最终只加载（运行）一次**
**为什么只会加载运行一次呢？**
**这是因为每个模块对象module都有一个属性：loaded。**
**为false表示还没有加载，为true表示已经加载；**
**结论三：如果有循环引入，那么加载顺序是什么**

foo.js

```js

const name = "why"
const age = 18

console.log("foo:", name)
console.log("foo中的代码被运行")

module.exports = {
  name,
  age
}

```

main.js

```js
console.log("main.js代码开始运行")
//1.模块在第一次被引入的时，模块中的js代码会被运行一次
require("./foo")
//2.模块被多次引入的时候 会缓存，最终只运行（加载）一次
require("./foo")
//3.如果有循环引入 加载顺序是什么呢
require("./foo")

console.log("main.js代码后续运行")
 /* 
  原因：
  每一个JS文件对应一个module实例 每一个module实例 里面有一个
  loaded属性 这个属性 就是 对应js是否加载问题 如果没有加载 就是fale
  如果加载过了 那么这个false属性就变成 true
 */
```



#### (6)模块加载顺序

![image-20211112162127245](./media/模块加载过程.png)

如果出现上图模块的引用关系，那么加载顺序是什么呢？ 

 这个其实是一种数据结构：图结构； 

 图结构在遍历的过程中，有深度优先搜索（DFS, depth first search）和广度优先搜索【以这个例子来说 就是 先遍历aaa 和bbb一层 然后在向下遍历】（BFS, breadth first search）； **Node采用的是深度优先算法**：main -> aaa -> ccc -> ddd -> eee ->bbb

**先从根节点main 向下查找 aaa-> ccc-> ddd-> eee 找到eee后 发现没有引用了 因为还没结束 从后面向前引用 直到回到根节点main.js  发现main.js引用bbb.js  输出bbb   bbb还引用 ccc 但是ccc已经被加载过一次了 他的 loaded=true了 所以 不会再次被加载，最后输出bbb结束 ** 

```js
//aaa.js

console.log('aaa');
require('./ccc');

//bbb.js

console.log('bbb');
require('./ccc');

//ccc.js

console.log('ccc');
require('./ddd');

//ddd.js

console.log('ddd');
require('./eee');

//eee.js

console.log('eee');

//main.js

console.log("main")
require('./aaa');
require('./bbb');

/*
输出结果
main
aaa
ccc
ddd
eee
bbb
*/
```

### 05CommonJS规范点

**CommonJS加载模块是同步的：** 

 **同步的意味着只有等到对应的模块加载完毕，当前模块中的内容才能被运行；** 

**这个在服务器不会有什么问题，因为服务器加载的js文件都是本地文件，加载速度非常快**； 

 如果将它应用于浏览器呢？ 

浏览器加载js文件需要先从服务器将文件下载下来，之后在加载运行； 

 那么采用同步的就意味着后续的js代码都无法正常运行，即使是一些简单的DOM操作； 

 **所以在浏览器中，我们通常不使用CommonJS规范：** 

 当然在webpack中使用CommonJS是另外一回事；  因为它会将我们的代码转成浏览器可以直接执行的代码； 在早期为了可以在浏览器中使用模块化，通常会采用AMD或CMD： 

 但是目前一方面现代的浏览器已经支持ES Modules，另一方面借助于webpack等工具可以实现对CommonJS或者 ES Module代码的转换； 

 AMD和CMD已经使用非常少了，所以这里我们进行简单的演练；

### 06AMD规范

AMD是Asynchronous Module Definition（异步模块定义）的缩写； 

 **它采用的是异步加载模块**； 

事实上AMD的规范还要早于CommonJS，但是CommonJS目前依然在被使用，而AMD使用的较少了； 

 我们提到过，规范只是定义代码的应该如何去编写，只有有了具体的实现才能被应用： 

 **AMD实现的比较常用的库是require.js和curl.js；**

**第一步：下载requireJs**

https://github.com/requirejs/requirejs

**第二步：定义HTML的script标签引入require.js和定义入口文件：** 

 data-main属性的作用是在加载完src的文件后会加载执行该文件

**index.html**

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <!--data-main属性的作用是在加载完src的文件后会加载执行该文件-->
  <script src="./lib/require.js" data-main="./index.js"></script>
</body>
</html>
```

**bar.js**

```js
//定义模块
define(function() {
  const name = "coderwhy";
  const age = 18;
  const sayHello = function(name) {
    console.log("你好" + name);
  }
//返回出去
  return {
    name,
    age,
    sayHello
  }
});
```

**foo.js**

```js
//引入bar里面的函数值 依赖模块放入数组
define(['bar'], function(bar) {
  console.log(bar.name);
  console.log(bar.age);
  bar.sayHello("kobe");
});
```

**index.js**

```js
//匿名函数 自执行函数
(function() {
    //注册你要引入的模块,配置你引入的路径
  require.config({
    baseUrl: '', //默认值 
    paths: {
        //不能加后缀名
      "bar": "./modules/bar",
      "foo": "./modules/foo"
    }
  })
    //加载使用的模块
  require(['foo','bar'], function(foo) {
  });
})();
```

### 07CMD规范

CMD规范也是应用于浏览器的一种模块化规范： 

CMD 是Common Module Definition（通用模块定义）的缩写； 

它也采用了异步加载模块，但是它将CommonJS的优点吸收了过来； 

 但是目前CMD使用也非常少了； 

 CMD也有自己比较优秀的实现方案： **SeaJS**

**第一步：下载sea.js**

https://github.com/seajs/seajs

**第二步：引入sea.js和使用主入口文件**

sea.js是指定主入口文件的

**index.html**

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <script src="./lib/sea.js"></script>
  <script>
      //作为主入口
    seajs.use('./index.js');
  </script>
</body>
</html>
```

**index.js**

```js
define(function(require, exports, module) {
    //引入
  const foo = require('./modules/foo');

  console.log(foo.name);
  console.log(foo.age);
  foo.sayHello("王小波");
})
```

**modules/foo.js**

```js
define(function(require, exports, module) {
  const name = "李银河";
  const age = 20;
  const sayHello = function(name) {
    console.log("你好" + name);
  }
  module.exports = {
    name: name,
    age: age,
    sayHello: sayHello
  }
});
```

### 08认识ES Module

JavaScript没有模块化一直是它的痛点，所以才会产生我们前面学习的社区规范CommonJS、AMD、CMD等，
所以ES推出自己的模块化系统

**ES Module和CommonJS的模块化有一些不同之处：**
**一方面它使用了import和export关键字；另一方面它采用编译期的静态分析，并且也加入了动态引用的方式；ES Module模块采用export和import关键字来实现模块化：export负责将模块内的内容导出；import负责从其他模块导入内容；**

**了解：采用ES Module将自动采用严格模式：use strict**

如果你不熟悉严格模式可以简单看一下MDN上的解析；
https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Strict_mode

#### (1)基本使用

foo.js

```js
export const name = 'wyd';
export const age = 20;
```

main.js

```js
import {name,age} from './foo.js'
console.log(name,age);
```

index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
  <!-- 
    script标签引入JS文件的话 默认会变成一个普通的JS解析 加载完直接【从上向下】执行
    如果出现import 引入其他模块的话 会报错 所以加上 type='module'是必要的
    如果  本地打开浏览器的话  file协议是不允许加载一个模块的 这样会报错
    启动一个服务  相当于本地服务器 去发送一个http请求 这样就不会出错
  -->
  <script src="./main.js" type="module"></script>
</body>
</html>
//输出 why 18
```

你需要注意本地测试—如果你通过本地加载Html 文件(比如一个file:// 路径的文件), 你将会遇到CORS 错误，因为Javascript 模块安全性需要

#### (2)其他用法

foo.js

```js
//1.export 导出 声明语句
// export const name = 'wangyaoda';
// export const  age = '19';

// export function foo() {
//     console.log('11111');
// }

// export class Person{

// }

//2.export 导出 和 声明语句分开
const name = "why"
const age = 18
function foo() {
  console.log("foo function")
}

//这是一个固定的语法 不是 对象 里面不能写键值对  
export {
  name,
  age,
  foo
}

//3. 在第二种导出方式的时候 起一个别名

// export {
//   name as fName,
//   age as fAge,
//   foo as fFoo
// }

```

main.js

```js
//导入的方式 普通的导入 
// import { name, age, foo } from "./foo.js"
// import { fName, fAge, fFoo } from './foo.js'


//2.导入的时候 起一个别名 

// import { name as fName, age as fAge, foo as fFoo } from './foo.js'

// 3.导入方式三: 将导出的所有内容放到一个标识符中 将里面所有东西 放入 foo之中了 
import * as foo from './foo.js'
console.log(foo.name)
console.log(foo.age)
foo.foo()


```

index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <script src="./main.js" type="module"></script>
</body>
</html> 
```



#### (3)结合使用

utils文件夹

```js
//format.js
function timeFormat() {
    return "2222-12-12"
  }
  
  function priceFormat() {
    return "222.22"
  }
  
  export {
    timeFormat,
    priceFormat
  }
 //math.js
  function add(num1, num2) {
    return num1 + num2
  }
  
  function sub(num1, num2) {
    return num1 - num2
  }
  
  export {
    add,
    sub
  }
  //index.js 将math和 format里面的东西导出
  /* 
工具库的封装 作为一个统一的出口
*/

// 1.导出方式一:
// import { add, sub } from './math.js'
// import { timeFormat, priceFormat } from './format.js'
// export {
//   add,
//   sub,
//   timeFormat,
//   priceFormat
// }


// 2.导出方式二:
 export { add, sub } from './math.js'
 export { timeFormat, priceFormat } from './format.js'



// 3.导出方式三:
// export * from './math.js'
// export * from './format.js'

```

main.js

```js
import {add,sub,timeFormat,priceFormat} from './utils/index.js'

console.log(add(10, 20))
console.log(sub(10, 20))
console.log(priceFormat())
console.log(timeFormat())

```

index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <script src="./main.js" type="module"></script>
</body>
</html>
```

#### (4)Default

foo.js

```js
const name = "why"
const age = 18

const foo = "foo value"

// 1.默认导出的方式一:
export {
  // named export 命名式导出
  name,
  // age as default,  //写上 as default 就成为了默认导出了 
  // foo as default
}

// 2.默认导出的方式二: 常见  也可以导出多数内容
export default {
    foo,
    name,
    age
}

// 注意: 默认导出只能有一个


```

main.js

```js
// import { name, age } from './foo.js'

// import * as foo from './foo.js'

// 导入语句: 导入的默认的导出  切记只能是默认的  导入的名字可以自己起
import why from './foo.js'

console.log(why)
```

index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <script src="./main.js" type="module"></script>
</body>
</html>
```

#### (5)import函数

**正常来说 导入的时候 会 阻塞后面的代码执行 因为不确定你是否后面的代码** 
**使用了 我前面要导入的变量  所以 才会阻塞执行 但是如果我们不想让他阻塞执行呢** 
**import()函数 调用就不会阻塞后面代码的执行了 并且 返回的是一个promise调用** 
**如果调用成功 返回一个promise.resolve 从.then里面获取你想要的 变量or函数**

foo.js

```js
const name = "why"
const age = 18

const foo = "foo value"

export {
  name,
  age,
  foo
}
```

main.js

```js
// import {name,age,foo} from './foo.js'
// console.log(name);

import ("./foo.js").then(res=>{
    console.log('res',res);
})

console.log("后续的代码都是不会运行的~")

/* 
正常来说 导入的时候 会 阻塞后面的代码执行 因为不确定你是否后面的代码 
使用了 我前面要导入的变量  所以 才会阻塞执行 但是如果我们不想让他阻塞执行呢 
import()函数 调用就不会阻塞后面代码的执行了 并且 返回的是一个promise调用 
如果调用成功 返回一个promise.resolve 从.then里面获取你想要的 变量or函数
*/



// ES11新增的特性 就是看你导出的东西路径的 
// meta属性本身也是一个对象: { url: "当前模块所在的路径" }
console.log(import.meta)
```

index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <script src="./main.js" type="module"></script>
</body>
</html>
```



![image-20211114145152759](./media/import函数导出.png)

#### (6)内部原理

**【具体有点不好理解 个人没有看全（以后回来二刷的时候 可以看下）】**

ES Module是如何被浏览器解析并且让模块之间可以相互引用的呢？ 

zui详细的文档解析： https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/ 

 ES Module的解析过程可以划分为三个阶段： 

 阶段一：构建（Construction），根据地址查找js文件，并且下载，将其解析成模块记录（Module Record）； 

 阶段二：实例化（Instantiation）对模块记录进行实例化，并且分配内存空间，解析模块的导入和导出语句把模块指向 对应的内存地址。 

 阶段三：运行（Evaluation），运行代码，计算值，并且将值填充到内存地址中；

**在webpack中 ESModule 和 CommonJS都支持的两者之间的相互引用  node里面如果要支持他们两个 就必须不同的版本不同的支持     浏览器没办法支持两者之间的相互引用**







## 包管理工具

**npm init -y 自动化生成 package.json 但是要求我们生成路径里面不能有中文  否则只能手动生成 package.json**

### 包管理工具npm

Node Package Manager，也就是Node包管理器

但是目前已经不仅仅是Node包管理器了，在前端项目中我们也在使用它来管理依赖的包；
比如vue、vue-router、vuex、express、koa、react、react-dom、axios、babel、webpack等等；

如何下载npm工具呢？

npm属于node的一个管理工具，所以我们需要先安装Node；

node管理工具：https://nodejs.org/en/
npm管理的包可以在哪里查看、搜索呢？
https://www.npmjs.org/
这是我们安装相关的npm包的官网；

npm管理的包存放在哪里呢？
我们发布自己的包其实是发布到registry上面的；
当我们安装一个包时其实是从registry上面下载的包；

### npm的配置文件package.json

那么对于一个项目来说，我们如何使用npm来管理这么多包呢？
事实上，我们每一个项目都会有一个对应的配置文件，无论是前端项目（Vue、React）还是后端项目（Node）；
这个配置文件会记录着你项目的名称、版本号、项目描述等；
也会记录着你项目所依赖的其他库的信息和依赖库的版本号；这个配置文件就是package.json

那么这个配置文件如何得到呢？
方式一：手动从零创建项目，npm init –y 但是如果有中文路径就只能手动生成
方式二：通过脚手架创建项目，脚手架会帮助我们生成package.json，并且里面有相关的配置

### package.json常见的属性

**必须填写的属性：name、version**
**name是项目的名称；**
**version是当前项目的版本号；**
**description是描述信息，很多时候是作为项目的基本描述；**
**author是作者相关信息（发布时用到）；**
**license是开源协议（发布时用到）；**

**private属性：**
private属性记录当前的项目是否是私有的；
当值为true时，npm是不能发布它的，这是防止私有项目或模块发布出去的方式；比如公司的项目就是不能发布出去的 这个是私有财产

**main属性：**
设置程序的入口。
webpack不是会自动找到程序的入口吗？
这个入口和webpack打包的入口并不冲突；它是在你**发布一个模块的时候会用到的**；
比如我们使用axios模块const axios = require('axios');实际上是找到对应的main属性查找文件的；

**scripts属性**
scripts属性用于配置一些脚本命令，以键值对的形式存在；配置后我们可以通过npm run 命令的key来执行这个命令；

**npm start和npm run start的区别是什么？**

它们是等价的；
对于常用的start、test、stop、restart可以省略掉run 直接通过npm start等方式运行； 【只有这几个是可以省掉的 其他都不行】

**dependencies属性**
dependencies属性是指定无论开发环境还是生成环境都需要依赖的包；
通常是我们项目实际开发用到的一些库模块vue、vuex、vue-router、react、react-dom、axios等等；

**devDependencies属性**
一些包在生成环境是不需要的，比如webpack、babel等；
这个时候我们会通过npm install webpack --save-dev，将它安装到devDependencies属性中；

**peerDependencies属性**
还有一种项目依赖关系是对等依赖，也就是你依赖的一个包，它必须是以另外一个宿主包为前提的；
比如element-plus是依赖于vue3的，ant design是依赖于react、react-dom；

【通过这个属性来确定你所安装的是否有我们对应的包，比如说element-plus对应的vue3，没有vue3这个包 他跑不起来这种 就需要】

 **engines属性** 

engines属性用于指定Node和NPM的版本号； 

在安装的过程中，会先检查对应的引擎版本，如果不符合就会报错； 

事实上也可以指定所在的操作系统 "os" : [ "darwin", "linux" ]，只是很少用到； 

 **browserslist属性**

用于配置打包后的JavaScript浏览器的兼容情况，参考； 

否则我们需要手动的添加polyfills来让支持某些语法； 也就是说它是为webpack等打包工具服务的一个属性（这里不是详细讲解webpack等工具的工作原理，所以不 再给出详情）；

```json
{
  "name": "npmdemo",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
    //私有属性 确保你能不能发布出去
  "private": true,
  "scripts": {
      //里面的指令是可以自定义的
    "wyd": "node ./scripts/wyd.js",
    "test": "",
    "start": ""
  },
  "author": "",
    //协议
  "license": "ISC",
    //依赖
  "dependencies": {
    "axios": "^0.24.0",
    "element-plus": "^1.2.0-beta.1",
    "vue": "^2.6.14"
  },
    //开发环境下的 实际就使用一次 生成环境不需要
  "devDependencies": {
    "webpack": "^5.61.0",
    "webpack-cli": "^4.9.1"
  },
    //指定node和npm的版本号
  "engines": {},
    //用于配置打包后的JS浏览器兼容情况 适配浏览器 可以单独抽出来做一个文件
  "browserslist": {}
}

```

### 依赖的版本管理

**我们会发现安装的依赖版本出现：^2.0.3或~2.0.3，这是什么意思呢？**
npm的包通常需要遵从semver版本规范：
semver：https://semver.org/lang/zh-CN/
npm semver：https://docs.npmjs.com/misc/semver

**semver版本规范是X.Y.Z：**
X主版本号（major）：当你做了不兼容的API 修改（可能不兼容之前的版本）；
Y次版本号（minor）：当你做了向下兼容的功能性新增（新功能增加，但是兼容之前的版本）；
Z修订号（patch）：当你做了向下兼容的问题修正（没有新功能，修复了之前版本的bug）；

```JS
//X 版本修改 就是 很多API进行修改了 就比如说vue2 换到vue3 有可能不会兼容之前的版本 

//Y版本就是指的是 向下功能新增 就是增加一些东西 增加一些小功能 就比如说 react16. 7 到react16.8 是增加了reacthooks

//Z改版 就是 将问题修正 以前的代码可能会有些小问题 就需要修正
```

**我们这里解释一下^和~的区别：**
^x.y.z：表示x是保持不变的，y和z永远安装最新的版本；
~x.y.z：表示x和y保持不变的，z永远安装最新的版本；

### package.json 和 package-lock.json

```js
//package.json里面记录的是大概的版本 而 package-lock.json里面记录的是最真实的版本 

//如果将node_modules删除  会根据package-lock.json的版本 去匹配 package.json 里面 的版本  如果二者匹配就去重新下载一下
```

### npm install 命令

安装npm包分两种情况： 

**全局安装（global install）： npm install webpack -g;** 

**项目（局部）安装（local install）： npm install webpack** 

全局安装 全局安装是直接将某个包安装到全局： 比如yarn的全局安装： 但是很多人对全局安装有一些误会： 通常使用npm全局安装的包都是一些工具包：yarn、webpack等； 

并不是类似于 axios、express、koa、element-plus等库文件； 

所以全局安装了之后并不能让我们在所有的项目中使用 axios等库；

项目安装会在当前目录下生产一个 node_modules 文件夹，我们之前讲解require查找顺序时有讲解过这个包在什 么情况下被查找； 

 局部安装分为开发时依赖和生产时依赖：

```
# 安装开发和生产依赖
npm install axios
npm i axios
# 开发依赖
npm install webpack --save-dev
npm install webpack -D
npm i webpack –D
# 根据package.json中的依赖包
npm install
```

### npm install 内部原理

![image-20211115101043254](./media/npminstall原理.png)

早期的npm是不支持缓存的 现在的npm支持缓存了 （从npm5开始）因为来自yarn的压力

```js
/* 
npm install会检测是有package-lock.json文件：
没有lock文件
 分析依赖关系，这是因为我们可能包会依赖其他的包，并且多个包之间会产生相同依赖的情况；
 从registry仓库中下载压缩包（如果我们设置了镜像，那么会从镜像服务器下载压缩包）；
 获取到压缩包后会对压缩包进行缓存（从npm5开始有的）；
 将压缩包解压到项目的node_modules文件夹中（前面我们讲过，require的查找顺序会在该包下面查找）

有lock文件
 检测lock中包的版本是否和package.json中一致（会按照semver版本规范检测）；
不一致，那么会重新构建依赖关系，直接会走顶层的流程；
 一致的情况下，会去优先查找缓存
没有找到，会从registry仓库下载，直接走顶层流程；
 查找到，会获取缓存中的压缩文件，并且将压缩文件解压到node_modules文件夹中；
  */
```

package-lock.json里面表示了你安装的依赖最正确的版本 

先是npm install 检查是否有package-lock.json文件 如果没有文件 就先去构建依赖关系怕【分析依赖关系，这是因为我们可能包会依赖其他的包，并且多个包之间会产生相同依赖的情况；】  先去看你下载的东西 比如说下载axios 就去看看axios 的依赖关系 是否还依赖别的文件 然后去 registry仓库下载 一个压缩包【压缩包是带索引指示的 能够展示出你所下载的版本关系，根据索引去找content内容】然后解压到node_modules里面，里面对应的就是源代码 ；这样虽然完成安装了 ，但是里面没有package- lock.json文件，没法真正的确定版本 ，所以 它会自动帮助我们生成package-lock.json,

如果npm install 有package-lock.json 文件 就去检查package.json 和package-lock.json里面的版本是否一致【如果不一致的话 就会重新去构建依赖，重新生成package-lock.json文件 eg:下载axios 你下载的package-lock.json里面是0.23.2版本，而package.json是0.26.0版本这样的话就会重新去构建依赖】，一致的话 去npm缓存里面查找【查找缓存】，

查找到去缓存文件 如果没找到则去 registry仓库重新下载

```js
//先去npm get cache 获取缓存的文件夹

//输出

//C:\Users\Administrator\AppData\Roaming\npm-cache
```

![image-20211115114201343](./media/npm缓存路径.png)

npm缓存索引  根据下面这个index-v5索引去找content-v2里面的内容

![image-20211115114407575](./media/npm缓存索引.png)

### package-lock.json文件

package-lock.json文件解析： 

 **name：项目的名称；** 

 **version：项目的版本；** 

 **lockfileVersion：lock文件的版本；** 

 r**equires：使用requires来跟踪模块的依赖关系；** 

 **dependencies：项目的依赖  当前项目依赖axios，但是axios依赖follow-redireacts；  axios中的属性如下：**

  **version表示实际安装的axios的版本；** 

 **resolved用来记录下载的地址，registry仓库中的位置；** 

 **requires记录当前模块的依赖；** 

**integrity用来从缓存中获取索引，再通过索引去获取压缩包文件；**

```json
{
  "name": "npmdemo",
  "version": "1.0.0",
    // lockfileVersion：lock文件的版本； 
  "lockfileVersion": 1,
    //requires：使用requires来跟踪模块的依赖关系
  "requires": true,
    //dependencies 项目的依赖  
  "dependencies": {
    "axios": {
        //  version表示实际安装的axios的版本； 
      "version": "0.24.0",
        // resolved用来记录下载的地址，registry仓库中的位置； 
      "resolved": "https://registry.npmjs.org/axios/-/axios-0.24.0.tgz",
        //integrity用来从npm缓存中获取索引，再通过索引去获取压缩包文件
      "integrity": "sha512-Q6cWsys88HoPgAaFAVUb0WpPk0O8iTeisR9IMqy9G8AbO4NlpVknrnQS03zzF9PGAWgO3cgletO3VjV/P7VztA==",
        // requires记录当前模块的依赖； 
      "requires": {
        "follow-redirects": "^1.14.4"
      }
    },
    "follow-redirects": {
      "version": "1.14.5",
      "resolved": "https://registry.npmjs.org/follow-redirects/-/follow-redirects-1.14.5.tgz",
      "integrity": "sha512-wtphSXy7d4/OR+MvIFbCVBDzZ5520qV8XfPklSN5QtxuMUJZ+b0Wnst1e1lCDocfzuCkHqj8k0FpZqO+UIaKNA=="
    }
  }
}

```

### npm其他命令

```js
 我们这里再介绍几个比较常用的：
 
 卸载某个依赖包：
npm uninstall package
npm uninstall package --save-dev
npm uninstall package -D

强制重新build  //将原来的依赖全部删除重新安装一遍
npm rebuild

清除缓存
npm cache clean
```

 npm的命令其实是非常多的：
 https://docs.npmjs.com/cli-documentation/cli
 更多的命令，可以根据需要查阅官方文档

### yarn工具

 yarn是由Facebook、Google、Exponent 和 Tilde 联合推出了一个新的 JS 包管理工具； 

 yarn 是为了弥补 npm 的一些缺陷而出现的； 

 早期的npm存在很多的缺陷，比如安装依赖速度很慢、版本依赖混乱等等一系列的问题； 

 虽然从npm5版本开始，进行了很多的升级和改进，但是依然很多人喜欢使用yarn；

yarn里面不是package-lock.json了 而是yarn.lock 锁定 

### cnpm工具

由于一些特殊的原因，某些情况下我们没办法很好的从 https://registry.npmjs.org下载下来一些需要的包 

什么是镜像：就是国内在整一个服务器 将国外的东西 备份一份到我国内的服务器上面，一般十分钟更新一次的 十分钟看一下区别 然后重新更新一下

**查看npm镜像：** 

```js
npm config get registry 
```

 **我们可以直接设置npm的镜像：** 

```node
npm config set registry https://registry.npm.taobao.org
```

 但是对于大多数人来说（比如我），并不希望将npm镜像修改了： 

 第一，不太希望随意修改npm原本从官方下来包的渠道； 

 第二，担心某天淘宝的镜像挂了或者不维护了，又要改来改去；

 这个时候，我们可以使用cnpm，并且将cnpm设置为淘宝的镜像：

```
npm install -g cnpm --registry=https://registry.npm.taobao.org //安装时设置

cnpm config set registry https://registry.npm.taobao.org   //安装完设置

cnpm config get registry # https://r.npm.taobao.org/
```

### npx命令

 npx是npm5.2之后自带的一个命令。 

**npx的作用非常多，但是比较常见的是使用它来调用项目中的某个模块的指令** 

 我们以webpack为例： 

全局安装的是webpack5.1.3 项目安装的是webpack3.6.0 

 如果我在终端执行 webpack --version使用的是哪一个命令呢？ 

显示结果会是 webpack 5.1.3，事实上使用的是全局的，为什么呢？ 

原因非常简单，在当前目录下找不到webpack时，就会去全局找，并且执行命令；

既然找到的是全局的webpack 然后我又想用局部的webpack的话 应该怎么办呢

**如何使用项目（局部）的webpack？**

 方式一：在终端中使用如下命令（在项目根目录下）

```
./node_modules/.bin/webpack --version
```

 方式二：修改package.json中的scripts

```
"scripts": {
"webpack": "webpack --version"
}
```

方式三：使用npx

```
npx webpack --version
```

npx的原理非常简单，它会到当前目录的node_modules/.bin目录下查找对应的命令

### 发布和使用自己的包

百度搜一下就可以了 具体就不详细写了

